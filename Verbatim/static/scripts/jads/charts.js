$(document).ready(function() {

    // AJAX GET
    $('#loadchart').click(function(){
        $('#chartresults').html("");
        $('#loadstatus').html("<font color=\"#cccccc\">Loading chart data...</font>");
        $.ajax({
            type: "GET",
            url: "/jads/ajax/loadchart/",
            dataType: "json",
            data: { "basecurrency": $("#basecurrency").val(),
                    "quotecurrency": $("#quotecurrency").val(),
                    "datasource": $("#datasource").val(),
                    "starttime": $("#starttime").val(),
                    "endtime": $("#endtime").val(),
                    "interval": $("#interval").val()},
            success: function(response) {
                $('#loadstatus').html("<font color=\"#4499ff\"><b>Success!</b></font>");
                var data = response;
                var table = $('<table></table>').addClass('charts');
                $('<thead>').append(
                    $('<td>').text("Period (UNIX)"),
                    $('<td>').text("Period"),
                    $('<td>').text("Open"),
                    $('<td>').text("High"),
                    $('<td>').text("Low"),
                    $('<td>').text("Close")
                    ).appendTo(table);
                $.each(data, function(interval, properties) {
                    var $tr = $('<tr>').append(
                        $('<td>').text(properties.period),
                        $('<td>').text(timeConverter(properties.period)),
                        $('<td>').text(properties.open),
                        $('<td>').text(properties.high),
                        $('<td>').text(properties.low),
                        $('<td>').text(properties.close),
                        ).appendTo(table);
                })
                $('#chartresults').html(table).trigger("create");
                //$('#chartresults').html(sequence);
            },
            error: function(response) {
                var error = response.responseJSON;
                $('#loadstatus').html("<font color=\"#aa0000\"><b>Error!</b></font>");
                $('#chartresults').html("Error loading chart data: "+error.errordescription);
            }
        });
    });

    // AJAX POST
    // N/A

    // CSRF code

    // This function gets cookie with a given name
    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }
    var csrftoken = getCookie('csrftoken');

    /*
    The functions below will create a header with csrftoken
    */

    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }

    function sameOrigin(url) {
        // test that a given url is a same-origin URL
        // url could be relative or scheme relative or absolute
        var host = document.location.host; // host + port
        var protocol = document.location.protocol;
        var sr_origin = '//' + host;
        var origin = protocol + sr_origin;
        // Allow absolute or scheme relative URLs to same origin
        return (url == origin || url.slice(0, origin.length + 1) == origin + '/') ||
            (url == sr_origin || url.slice(0, sr_origin.length + 1) == sr_origin + '/') ||
            // or any other URL that isn't scheme relative or absolute i.e relative.
            !(/^(\/\/|http:|https:).*/.test(url));
    }

    $.ajaxSetup({
        crossDomain: false, // obviates need for sameOrigin test
        beforeSend: function(xhr, settings) {
            if (!csrfSafeMethod(settings.type) && sameOrigin(settings.url)) {
                // Send the token to same-origin, relative URLs only.
                // Send the token only if the method warrants CSRF protection
                // Using the CSRFToken value acquired earlier
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });
});

function timeConverter(UNIX_timestamp){
  var a = new Date(UNIX_timestamp * 1000);
  
  var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
  var year = a.getFullYear();
  var month = months[a.getMonth()];
  var date = a.getDate();
  var hour = a.getHours();
  var min = "0" + a.getMinutes();
  var sec = "0" + a.getSeconds();
  
  var time = date + ' ' + month + ' ' + year + ' ' + hour + ':' + min + ':' + sec;
  return time;
}