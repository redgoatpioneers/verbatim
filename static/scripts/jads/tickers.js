$(document).ready(function() {

    $('.tickerchoices').hide();
    $('#datasource').get(0).selectedIndex = 0;
    $('#datasource').change(function () {
        $('.tickerchoices').hide();
        $('#'+$(this).val()).show();
        $('.pairbox').attr('checked', false);
        $('#tickers').html("");
        $('#loadstatus').html("");
    })

    // AJAX GET
    $('#generate').click(function(){
        $('#tickers').html("");
        $('#loadstatus').html("<font color=\"#cccccc\">Loading chart data...</font>");
        
        var datasource = $("#datasource").val();
        
        var pairs = [];
        $(".pairbox:checked").each(function(){
            pairs.push(this.id)
        })
        //alert("Current pairs selected: " + pairs)

        $.ajax({
            type: "GET",
            url: "/jads/gettickers/",
            dataType: "json",
            data: { "datasource": datasource,
                    "pairs": pairs},
            success: function(response) {
                $('#loadstatus').html("<font color=\"#4499ff\"><b>Success!</b></font>");
                
                var data = response;
                var table = $('<table></table>').addClass('tickers');
                $('<thead>').append(
                    $('<td>').text("Pair"),
                    $('<td>').text("Current Price"),
                    $('<td>').text("Last Trade Size"),
                    $('<td>').text("Latest Bid"),
                    $('<td>').text("Latest Ask"),
                    $('<td>').text("24h Volume")
                    ).appendTo(table);
                $.each(data, function(interval, properties) {
                    var $tr = $('<tr>').append(
                        $('<td>').text(properties.pair),
                        $('<td>').text(properties.price),
                        $('<td>').text(properties.size),
                        $('<td>').text(properties.bid),
                        $('<td>').text(properties.ask),
                        $('<td>').text(properties.volume),
                        ).appendTo(table);
                })
                $('#tickers').html(table).trigger("create");
            },
            error: function(response) {
                var error = response.responseJSON;
                $('#loadstatus').html("<font color=\"#aa0000\"><b>Error!</b></font>");
                $('#tickers').html("Error retrieving tickers: "+error.errordescription);
            }
        });
    });

//    <div id="gdaxtickers">
//    <h1>GDAX Tickers</h1>

//    <table class="tickers">
//        <thead>
//            <th>Pair</th>
//            <th>Current Price</th>
//            <th>Last Trade Size</th>
//            <th>Latest Bid</th>
//            <th>Latest Ask</th>
//            <th>24h Volume</th>
//        </thead>
//    % for t in gdaxtickers %
//        <tr>
//            <td>{{t.pair}}</td>
//            <td>{{t.price}}</td>
//            <td>{{t.size}}</td>
//            <td>{{t.bid}}</td>
//            <td>{{t.ask}}</td>
//            <td>{{t.volume}}</td>
//        </tr>
//    % endfor %
//    </table>
//    </div>
//
//    <p />
//    <h1>Bitfinex Tickers</h1>
//
//    <table class="tickers">
//        <thead>
//            <th>Pair</th>
//            <th>Current Price</th>
//            <th>Latest Bid</th>
//            <th>Latest Ask</th>
//            <th>24h Volume</th>
//            <th>Low</th>
//            <th>High</th>
//        </thead>
//    % for t in bfxtickers %
//        <tr>
//            <td>{{t.pair}}</td>
//            <td>{{t.last_price}}</td>
//            <td>{{t.bid}}</td>
//            <td>{{t.ask}}</td>
//            <td>{{t.volume}}</td>
//            <td>{{t.low}}</td>
//            <td>{{t.high}}</td>
//        </tr>
//    % endfor %
//    </table>

    // AJAX POST
    // N/A

    // CSRF code

    // This function gets cookie with a given name
    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }
    var csrftoken = getCookie('csrftoken');

    /*
    The functions below will create a header with csrftoken
    */

    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }

    function sameOrigin(url) {
        // test that a given url is a same-origin URL
        // url could be relative or scheme relative or absolute
        var host = document.location.host; // host + port
        var protocol = document.location.protocol;
        var sr_origin = '//' + host;
        var origin = protocol + sr_origin;
        // Allow absolute or scheme relative URLs to same origin
        return (url == origin || url.slice(0, origin.length + 1) == origin + '/') ||
            (url == sr_origin || url.slice(0, sr_origin.length + 1) == sr_origin + '/') ||
            // or any other URL that isn't scheme relative or absolute i.e relative.
            !(/^(\/\/|http:|https:).*/.test(url));
    }

    $.ajaxSetup({
        crossDomain: false, // obviates need for sameOrigin test
        beforeSend: function(xhr, settings) {
            if (!csrfSafeMethod(settings.type) && sameOrigin(settings.url)) {
                // Send the token to same-origin, relative URLs only.
                // Send the token only if the method warrants CSRF protection
                // Using the CSRFToken value acquired earlier
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });
});