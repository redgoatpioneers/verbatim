$(document).ready(function() {

    // AJAX GET
    $('#loadchart').click(function(){
        $('#chartresults').html("");
        $('#loadstatus').html("<font color=\"#cccccc\">Loading chart data...</font>");
        $.ajax({
            type: "GET",
            url: "/jads/runstrategy/",
            dataType: "json",
            data: { // Chart selections
                    "basecurrency": $("#basecurrency").val(),
                    "quotecurrency": $("#quotecurrency").val(),
                    "datasource": $("#datasource").val(),
                    "starttime": $("#starttime").val(),
                    "endtime": $("#endtime").val(),
                    "interval": $("#interval").val(),
                    // Indicator selections
                    "tsi_short": $("#tsi_short").val(),
                    "tsi_long": $("#tsi_long").val(),
                    "rsi_length": $("#rsi_length").val(),
                    "dilen": $("#dilen").val(),
                    "adxlen": $("#adxlen").val(),
                    "wvf_length": $("#wvf_length").val(),
                    // Backtesting Selections
                    "backtest": $("#backtest").is(':checked'),
                    "deposit": $("#deposit").val(),
                    "investment": $("#investment").val(),
                    "trade_percentage": $("#trade_percentage").val(),
                    // Threshold Strategy Selections
                    "threshold": $("#threshold").is(':checked'),
                    "threshold_lower": $("#threshold_lower").val(),
                    "threshold_upper": $("#threshold_upper").val(),
                    // Buy-and-Hold Strategy Selections
                    "buyandhold": $("#buyandhold").is(':checked'),
                    "buyandhold_lower": $("#buyandhold_lower").val()},
            success: function(response) {
                $('#loadstatus').html("<font color=\"#4499ff\"><b>Success!</b></font>");
                var data = response;
                var strategies = data.strategies;
                var sequence = data.sequence;

                var numstrategies = strategies.length;
                var strategy_list = {}

                // Create table headers
                var table = $('<table></table>').addClass('charts');
                var header_row = $('<thead>').append(
                    $('<td>').text("Period"),
                    $('<td>').text("Open"),
                    $('<td>').text("High"),
                    $('<td>').text("Low"),
                    $('<td>').text("Close"),
                    $('<td>').text("Volume"),
                    $('<td>').text("JADS")
                ).appendTo(table);

                // Create table headers for each strategy
                for (var i = 0; i < numstrategies; i++) {
                    var strat = strategies[i];
                    strategy_list[i] = strat.strategy; // Maintain the order so you can add rows later
                    $('<td>').text(strat.strategy + " Action").appendTo(header_row);
                    $('<td>').text(strat.strategy + " Confidence").appendTo(header_row);
                    if ($("#backtest").is(':checked')) {
                        $('<td>').text($("#quotecurrency").val()).appendTo(header_row);
                        $('<td>').text($("#basecurrency").val()).appendTo(header_row);
                        $('<td>').text("% Gain").appendTo(header_row);
                    }
                }

                // Create a row for each interval
                $.each(sequence, function(interval, properties) {
                    var data_row = $('<tr id="strategy_table_rows">').append(
                        $('<td>').text(timeConverter(properties.period)),
                        $('<td>').text(properties.open),
                        $('<td>').text(properties.high),
                        $('<td>').text(properties.low),
                        $('<td>').text(properties.close),
                        $('<td>').text(properties.volume),
                        $('<td>').text(properties.jads)
                    ).appendTo(table);

                    // Create columns for each strategy in the same order as headers
                    for (var i = 0; i < Object.keys(strategy_list).length; i++) {
                        for (var j = 0; j < properties.strategies.length; j++) {
                            if (properties.strategies[j]["strategy"] === strategy_list[i]) {
                                $('<td>')
                                    .css(actionFormat(properties.strategies[j]["action"]))
                                    .text(properties.strategies[j]["action"]).appendTo(data_row);
                                $('<td>')
                                    .text(properties.strategies[j]["confidence"]).appendTo(data_row);
                                if ($("#backtest").is(':checked')) {
                                    $('<td>').text(properties.strategies[j]["balance"]).appendTo(data_row);
                                    $('<td>').text(properties.strategies[j]["holdings"]).appendTo(data_row);
                                    $('<td>').text(properties.strategies[j]["gain"]).appendTo(data_row);
                                }
                            }
                        }
                    }
                });

                google.charts.setOnLoadCallback(drawIndicatorsLineChart(sequence.reverse()));
                
                $('#chartresults').html(table).trigger("create");
            },
            error: function(response, textStatus, thrownError) {
                $('#loadstatus').html("<font color=\"#aa0000\"><b>Error!</b></font>");
                $('#chartresults').html(JSON.stringify(response));
            }
        });
    });

    // AJAX POST
    // N/A

    // CSRF code

    // This function gets cookie with a given name
    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }
    var csrftoken = getCookie('csrftoken');

    /*
    The functions below will create a header with csrftoken
    */

    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }

    function sameOrigin(url) {
        // test that a given url is a same-origin URL
        // url could be relative or scheme relative or absolute
        var host = document.location.host; // host + port
        var protocol = document.location.protocol;
        var sr_origin = '//' + host;
        var origin = protocol + sr_origin;
        // Allow absolute or scheme relative URLs to same origin
        return (url == origin || url.slice(0, origin.length + 1) == origin + '/') ||
            (url == sr_origin || url.slice(0, sr_origin.length + 1) == sr_origin + '/') ||
            // or any other URL that isn't scheme relative or absolute i.e relative.
            !(/^(\/\/|http:|https:).*/.test(url));
    }

    $.ajaxSetup({
        crossDomain: false, // obviates need for sameOrigin test
        beforeSend: function(xhr, settings) {
            if (!csrfSafeMethod(settings.type) && sameOrigin(settings.url)) {
                // Send the token to same-origin, relative URLs only.
                // Send the token only if the method warrants CSRF protection
                // Using the CSRFToken value acquired earlier
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });
});

function timeConverter(UNIX_timestamp){
  var a = new Date(UNIX_timestamp * 1000);
  
  var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
  var year = a.getFullYear();
  var month = months[a.getMonth()];
  var date = a.getDate();
  var hour = ("0" + a.getHours()).slice(-2);
  var min = ("0" + a.getMinutes()).slice(-2);
  var sec = ("0" + a.getSeconds()).slice(-2);
  
  var time = date + ' ' + month + ' ' + year + ' ' + hour + ':' + min + ':' + sec;
  return time;
}

function chartTimeConverter(UNIX_timestamp){
  var a = new Date(UNIX_timestamp * 1000);
  
  var months = ['01','02','03','04','05','06','07','08','09','10','11','12'];
  var year = a.getFullYear();
  var month = months[a.getMonth()];
  var date = a.getDate();
  var hour = ("0" + a.getHours()).slice(-2);
  var min = ("0" + a.getMinutes()).slice(-2);
  
  var time = year + '-' + month + '-' + date + ' ' + hour + ':' + min;
  return time;
}

function drawIndicatorsLineChart(jsonResponse) {
    var chart = new google.visualization.LineChart(document.getElementById('indicators_line_chart'));
    var jsonData = getIndicatorsJSONData(jsonResponse);
    var dataTable = new google.visualization.DataTable(jsonData);
    var options = {
        vAxis: {title: 'Indicator Value / % Gain'}
    };

    chart.draw(dataTable, options);
};

function getIndicatorsJSONData(jsonResponse) {
    var jsonData = {}

    var strategies = jsonResponse[0].strategies;
    var num_strategies = strategies.length;
    var strategy_list = {}

    jsonData['cols'] = [{'id': 'period', 'label': 'Period', 'type': 'string'}]
    
    indicators = [{'name': 'jads', 'label': 'JADS'}]
    for (i = 0; i < num_strategies; i++) {
        var strategy = strategies[i];
        strategy_list[i] = strategy.strategy;
        indicators.push({'name': strategy.strategy + '_gain', 'label': strategy.strategy + ' % Gain'})
    };
    indicators.forEach(function(indicator){
        jsonData['cols'].push({'id': indicator.name, 'label': indicator.label, 'type': 'number'})
    });

    jsonData['rows'] = []
    
    jsonResponse.forEach(function(period){
        var new_row = {'c':[{'v': chartTimeConverter(period.period)},
                            {'v': 'jads' in period ? period.jads : null}]}

        for (var i = 0; i < Object.keys(strategy_list).length; i++) {
            for (var j = 0; j < period.strategies.length; j++) {
                if (period.strategies[j]["strategy"] === strategy_list[i]) {
                    new_row["c"].push({'v': 'gain' in period.strategies[j] ? period.strategies[j]["gain"] : null})
                }
            }
        }

        jsonData['rows'].push(new_row)
    });

    return jsonData;
};

function actionFormat(action){
    if (action.toLowerCase() === "buy") {
        return {"color": "green", "font-weight": "bold"}
    } else if (action.toLowerCase() === "sell") {
        return {"color": "red", "font-weight": "bold"}
    } else {
        return {"color": "#3388cc", "font-weight": "bold"}
    }
}
