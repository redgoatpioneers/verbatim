from jads.models import Currency, Exchange, Trade, Candle

from rest_framework import serializers

class CurrencySerializer(serializers.ModelSerializer):
    class Meta:
        model = Currency
        fields = (
            'symbol',
            'symbol3',
            'name',
            )

class ExchangeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Exchange
        fields = (
            'name',
            'api_url',
            )

class TradeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Trade
        fields = (
            'symbol1',
            'symbol2',
            'price',
            'amount',
            'tradedate',
            'source',
            )

class CandleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Candle
        fields = (
            'symbol1',
            'symbol2',
            'startdate', 
            'interval',
            'open',
            'high',
            'low',
            'close',
            'volume',
            'source',
            )