from celery.task.schedules import crontab
from celery.decorators import task
from celery.utils.log import get_task_logger

import time
import datetime

from django.db.models import Max

from jads.datasources.candlemaker import sequencer, combinecandles
from jads.models import Exchange, Currency, Candle

logger = get_task_logger(__name__)

def prettytime(unixtimestamp):
    return (datetime.datetime
            .fromtimestamp(int(unixtimestamp))
            .strftime('%Y-%m-%d %I:%M %p'))

now = time.mktime(datetime.datetime.now().timetuple())

@task(name="print-test")
def celery_test():
    logger.info("Ran print test")
    print("It's %s and I am celery!" % datetime.datetime.now())

# =============================================================================
# TASKS
# =============================================================================

# Update 5 minute candles from GDAX
# -----------------------------------------------------------------------------

@task(bind=True)
def gdax_btcusd_fullupdate(request):
    auto_sequencer("Bitcoin", "United States Dollar", "GDAX", "300")

@task(bind=True)
def gdax_ethusd_fullupdate(request):
    auto_sequencer("Ethereum", "United States Dollar", "GDAX", "300")

@task(bind=True)
def gdax_ltcusd_fullupdate(request):
    auto_sequencer("Litecoin", "United States Dollar", "GDAX", "300")

@task(bind=True)
def gdax_bchusd_fullupdate(request):
    auto_sequencer("Bitcoin Cash", "United States Dollar", "GDAX", "300")

@task(bind=True)
def gdax_ethbtc_fullupdate(request):
    auto_sequencer("Ethereum", "Bitcoin", "GDAX", "300")

@task(bind=True)
def gdax_ltcbtc_fullupdate(request):
    auto_sequencer("Litecoin", "Bitcoin", "GDAX", "300")

@task(bind=True)
def gdax_bchbtc_fullupdate(request):
    auto_sequencer("Bitcoin Cash", "Bitcoin", "GDAX", "300")

# Update 5 minute candles from Binance
# -----------------------------------------------------------------------------

@task(bind=True)
def binance_btcusdt_fullupdate(request):
    auto_sequencer("Bitcoin", "United States Dollar", "Binance", "300")

@task(bind=True)
def binance_ethbtc_fullupdate(request):
    auto_sequencer("Ethereum", "Bitcoin", "Binance", "300")

@task(bind=True)
def binance_adabtc_fullupdate(request):
    auto_sequencer("Cardano", "Bitcoin", "Binance", "300")

@task(bind=True)
def binance_ltcbtc_fullupdate(request):
    auto_sequencer("Litecoin", "Bitcoin", "Binance", "300")

@task(bind=True)
def binance_eosbtc_fullupdate(request):
    auto_sequencer("EOS", "Bitcoin", "Binance", "300")

@task(bind=True)
def binance_neobtc_fullupdate(request):
    auto_sequencer("NEO", "Bitcoin", "Binance", "300")

@task(bind=True)
def binance_iotabtc_fullupdate(request):
    auto_sequencer("Iota", "Bitcoin", "Binance", "300")

@task(bind=True)
def binance_dashbtc_fullupdate(request):
    auto_sequencer("Dash", "Bitcoin", "Binance", "300")

@task(bind=True)
def binance_xmrbtc_fullupdate(request):
    auto_sequencer("Monero", "Bitcoin", "Binance", "300")

@task(bind=True)
def binance_trxbtc_fullupdate(request):
    auto_sequencer("Tronix", "Bitcoin", "Binance", "300")

@task(bind=True)
def binance_btgbtc_fullupdate(request):
    auto_sequencer("Bitcoin Gold", "Bitcoin", "Binance", "300")

@task(bind=True)
def binance_venbtc_fullupdate(request):
    auto_sequencer("VeChain", "Bitcoin", "Binance", "300")

@task(bind=True)
def binance_qtumbtc_fullupdate(request):
    auto_sequencer("Qtum", "Bitcoin", "Binance", "300")

@task(bind=True)
def binance_etcbtc_fullupdate(request):
    auto_sequencer("Ethereum Classic", "Bitcoin", "Binance", "300")

@task(bind=True)
def binance_lskbtc_fullupdate(request):
    auto_sequencer("Lisk", "Bitcoin", "Binance", "300")

@task(bind=True)
def binance_pptbtc_fullupdate(request):
    auto_sequencer("Populous", "Bitcoin", "Binance", "300")

@task(bind=True)
def binance_bnbbtc_fullupdate(request):
    auto_sequencer("ZCash", "Bitcoin", "Binance", "300")

@task(bind=True)
def binance_bnbbtc_fullupdate(request):
    auto_sequencer("Icon", "Bitcoin", "Binance", "300")

@task(bind=True)
def binance_bnbbtc_fullupdate(request):
    auto_sequencer("Binance Coin", "Bitcoin", "Binance", "300")

# Combine candles for GDAX
# -----------------------------------------------------------------------------

@task(bind=True)
def gdax_btcusd_combinecandles(request):
    auto_combine("GDAX", "Bitcoin", "United States Dollar")
    
@task(bind=True)
def gdax_ethusd_combinecandles(request):
    auto_combine("GDAX", "Ethereum", "United States Dollar")
    
# @task(bind=True)
# def gdax_ltcusd_combinecandles(request):
#     auto_combine("GDAX", "Litecoin", "United States Dollar")
    
# @task(bind=True)
# def gdax_bchusd_combinecandles(request):
#     auto_combine("GDAX", "Bitcoin Cash", "United States Dollar")

# @task(bind=True)
# def gdax_ethbtc_combinecandles(request):
#     auto_combine("GDAX", "Ethereum", "Bitcoin")

# @task(bind=True)
# def gdax_ltcbtc_combinecandles(request):
#     auto_combine("GDAX", "Litecoin", "Bitcoin")

# @task(bind=True)
# def gdax_bchbtc_combinecandles(request):
#     auto_combine("GDAX", "Bitcoin Cash", "Bitcoin")

# Combine candles for Binance
# -----------------------------------------------------------------------------

@task(bind=True)
def binance_btcusdt_combinecandles(request):
    auto_combine("Binance", "Bitcoin", "United States Dollar")

# @task(bind=True)
# def binance_ethbtc_combinecandles(request):
#     auto_combine("Binance", "Ethereum", "Bitcoin")

@task(bind=True)
def binance_adabtc_combinecandles(request):
    auto_combine("Binance", "Cardano", "Bitcoin")

# @task(bind=True)
# def binance_ltcbtc_combinecandles(request):
#     auto_combine("Binance", "Litecoin", "Bitcoin")

# @task(bind=True)
# def binance_eosbtc_combinecandles(request):
#     auto_combine("Binance", "EOS", "Bitcoin")

# @task(bind=True)
# def binance_neobtc_combinecandles(request):
#     auto_combine("Binance", "NEO", "Bitcoin")

# @task(bind=True)
# def binance_iotabtc_combinecandles(request):
#     auto_combine("Binance", "Iota", "Bitcoin")

# @task(bind=True)
# def binance_dashbtc_combinecandles(request):
#     auto_combine("Binance", "Dash", "Bitcoin")

# @task(bind=True)
# def binance_xmrbtc_combinecandles(request):
#     auto_combine("Binance", "Monero", "Bitcoin")

# @task(bind=True)
# def binance_trxbtc_combinecandles(request):
#     auto_combine("Binance", "Tronix", "Bitcoin")

# @task(bind=True)
# def binance_btgbtc_combinecandles(request):
#     auto_combine("Binance", "Bitcoin Gold", "Bitcoin")

# @task(bind=True)
# def binance_venbtc_combinecandles(request):
#     auto_combine("Binance", "VeChain", "Bitcoin")

# @task(bind=True)
# def binance_qtumbtc_combinecandles(request):
#     auto_combine("Binance", "Qtum", "Bitcoin")

# @task(bind=True)
# def binance_etcbtc_combinecandles(request):
#     auto_combine("Binance", "Ethereum Classic", "Bitcoin")

# @task(bind=True)
# def binance_lskbtc_combinecandles(request):
#     auto_combine("Binance", "Lisk", "Bitcoin")

# @task(bind=True)
# def binance_pptbtc_combinecandles(request):
#     auto_combine("Binance", "Populous", "Bitcoin")

# @task(bind=True)
# def binance_bnbbtc_combinecandles(request):
#     auto_combine("Binance", "ZCash", "Bitcoin")

# @task(bind=True)
# def binance_bnbbtc_combinecandles(request):
#     auto_combine("Binance", "Icon", "Bitcoin")

# @task(bind=True)
# def binance_bnbbtc_combinecandles(request):
#     auto_combine("Binance", "Binance Coin", "Bitcoin")

# =============================================================================
# TASK METHODS
# =============================================================================

# Automatically pull a set of the latest 5 minute candles
# -----------------------------------------------------------------------------
def auto_sequencer(base, quote, datasource, interval):
    print("INFO >>> Autosequence || Base: %s | Quote: %s " \
          "| Data Source: %s | Interval (s): %s" % \
          (base, quote, datasource, interval))
    try:
        # Start from the last saved candle.
        latest_time = (Candle.objects.filter(
            source=Exchange.objects.get(name=datasource),
            symbol1=Currency.objects.get(name=base),
            symbol2=Currency.objects.get(name=quote),
            interval=interval)
            .aggregate(Max('startdate'))['startdate__max'])
        startdate = prettytime(latest_time)
        enddate = prettytime(int(latest_time) + (int(interval)*100))
        print("INFO >>> Success pulling candles. Startdate: %s | Enddate: %s" % 
              (startdate, enddate))
    except Exception as e:
        print("ERROR >>> %s" % e)
        logger.info("Error pulling trades from the database: %s" % e)
        return

    try:
        print("INFO >>> Beginning sequencer...")
        sequencer(base, quote, datasource, interval, "false", "true",
            startdate, enddate)
        print("INFO >>> Successfully created candles.")
        logger.info("Successfully created %ss %s/%s candles for %s" % 
                    (interval, base, quote, datasource))
    except Exception as e:
        print("ERROR >>> %s" % e)
        logger.info("Error creating candles: %s" % e)

# Automatically combine smaller candles into bigger ones
# -----------------------------------------------------------------------------
def auto_combine(datasource, base, quote):
    print("INFO >>> Autocombine || Source: %s | Base: %s | Quote: %s " %
          (datasource, base, quote))
    try:
        # Look at the 15-min candles to find the startdate for combining
        candles = Candle.objects.filter(
            source=Exchange.objects.get(name=datasource),
            symbol1=Currency.objects.get(name=base),
            symbol2=Currency.objects.get(name=quote),
            interval=900)
        
        latest_time = candles.aggregate(Max('startdate'))['startdate__max']
        a_week_ago = time.time() - 604800
        enddate = prettytime(int(time.time() - (time.time() % 300)))

        if latest_time > a_week_ago:
            startdate = prettytime(a_week_ago)
        else:
            startdate = prettytime(latest_time)

        print("INFO >>> Success pulling candles. " \
              "Startdate: %s | Enddate: %s" % (startdate, enddate))
    except Exception as e:
        print("ERROR >>> %s" % e)
        logger.info("Error pulling trades from the database: %s" % e)
        return

    try:
        print("INFO >>> Beginning combiner...")
        combinecandles(datasource, base, quote,
            startdate, enddate, update=False)
        print("INFO >>> Successfully created combined candles.")
        logger.info("Successfully combined %s/%s candles for %s from %s to %s."
            % (base, quote, datasource, startdate, enddate))
    except Exception as e:
        print("ERROR >>> %s" % e)
        logger.info("Error creating candles: %s" % e)