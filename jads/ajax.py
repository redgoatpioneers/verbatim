import datetime
import json
import operator
import sys

from time import sleep
from math import floor

from django.http import Http404, HttpResponse, HttpResponseBadRequest
from django.db.models import Min, Max

from jads.models import Exchange, Trade, Currency

from jads.common.utilities import prettytime

from jads.datasources.gdax import getgdaxticker
from jads.datasources.bitfinex import getbfxticker
from jads.datasources.binance import getbinance24hr
from jads.datasources.loadcsvhistory import importhistory
from jads.datasources.candlemaker import sequencer, deletecandles, combinecandles
from jads.datasources.sequencer2 import sequence_ohlc

from jads.analysis.indicators import (
    apply_sma,
    apply_ema,
    apply_tsi,
    apply_rsi,
    apply_adx,
    apply_wvf,
    apply_jads,
    )

from jads.analysis.indicators2 import (
    sma,
    ema,
    tsi,
    rsi,
    adx,
    wvf,
    jads,)

from jads.analysis.strategy import (
    convert_sequence_strategy,
    threshold_strategy,
    buyandhold_strategy,
    )

from jads.analysis.analyses import (
    getverbatimdata,
    saveindicators,
    )

from jads.analysis.verbatim import (
    store_verbatim,
    retrieve_verbatim,
    delete_verbatim,
    )

from jads.tasks import auto_sequencer, auto_combine

'''
AJAX.PY
------------------------------
Created by Jacob Ashton
Red Goat Pioneers (c) October 2017

'''

def loadverbatim(request):
    print('Attempting to load verbatim indexes...')
    if request.is_ajax() and request.method != 'GET':
        print('This is not a valid GET request!');
        return HttpResponse(json.dumps({'message': 'Unauthorized attempt!'}), \
                            content_type='application/json', status=400)
    elif request.is_ajax() and request.method == 'GET':
        verbatimdata = getverbatimdata(request.GET.get('datasource'))

        if 'error' in verbatimdata:
            return HttpResponse(json.dumps(verbatimdata),
                content_type='application/json', status=400)

        print('Verbatim index data received!')
        return HttpResponse(json.dumps(verbatimdata),
            content_type='application/json', status=200)
    else:
        print('Error retrieving chart data...');
        return Http404

def loadfilescript(request):
    print('Attempting to load file %s...' % request.POST.get('filepath'))
    if request.is_ajax() and request.method != 'POST':
        print('This is not a valid POST request!');
        return HttpResponse(json.dumps({'message': 'Unauthorized attempt!'}), \
                            content_type='application/json', status=400)
    elif request.is_ajax() and request.method == 'POST':
        response = importhistory(request.POST.get('filepath'),      \
                                 request.POST.get('datasource'),    \
                                 request.POST.get('basecurrency'),  \
                                 request.POST.get('quotecurrency'))

        if response['results'] == 'success':
            return HttpResponse(json.dumps(response),
                                content_type='application/json', status=200)
        else:
            return HttpResponse(json.dumps(response),
                                content_type='application/json', status=400)
    else:
        print('Error loading file...');
        raise Http404

def gettickers(request):
    print('Attempting to retrieve tickers...')
    if request.is_ajax() and request.method != 'GET':
        print('This is not a valid GET request!');
        return HttpResponse(json.dumps({'message': 'Unauthorized attempt!'}), \
                            content_type='application/json', status=400)
    elif request.is_ajax() and request.method == 'GET':
        datasource = request.GET.get('datasource')
        pairs = request.GET.getlist('pairs[]')
        tickers = []

        #debug
        #print('Datasource: %s | Pairs: %s' % (datasource, pairs))

        if datasource == 'Bitfinex':
            for pair in pairs:
                try:
                    tickerdata = getbfxticker(pair[0:3].lower(), pair[4:7].lower())
                    ticker = {'pair':   tickerdata['pair'],
                              'price':  float(tickerdata['last_price']),
                              'size':   'N/A',
                              'bid':    float(tickerdata['bid']),
                              'ask':    float(tickerdata['ask']),
                              'volume': float(tickerdata['volume'])}
                    tickers.append(ticker)
                    sleep(1)
                except:
                    pass

        elif datasource == 'GDAX':
            for pair in pairs:
                try:
                    tickerdata = getgdaxticker(pair)
                    ticker = {'pair':   tickerdata['pair'],
                              'price':  float(tickerdata['price']),
                              'size':   float(tickerdata['size']),
                              'bid':    float(tickerdata['bid']),
                              'ask':    float(tickerdata['ask']),
                              'volume': float(tickerdata['volume'])}
                    tickers.append(ticker)
                    sleep(1)
                except:
                    pass

        elif datasource == 'Binance':
            for pair in pairs:
                try:
                    tickerdata = getbinance24hr(pair)
                    ticker = {'pair':   tickerdata['symbol'],
                              'price':  float(tickerdata['lastPrice']),
                              'size':   float(tickerdata['lastQty']),
                              'bid':    float(tickerdata['bidPrice']),
                              'ask':    float(tickerdata['askPrice']),
                              'volume': float(tickerdata['volume'])}
                    tickers.append(ticker)
                    sleep(1)
                except:
                    pass

        else:
            tickers = None

        print('Success!')
        return HttpResponse(json.dumps(tickers), content_type='application/json')
    else:
        print('Error retrieving ticker data...');
        raise Http404    

def loadexchangestatuses(request):
    print('Attempting to load exchange statuses...')
    if request.is_ajax() and request.method != 'GET':
        print('This is not a valid GET request!');
        return HttpResponse(json.dumps({'message': 'Unauthorized attempt!'}), \
                            content_type='application/json', status=400)
    elif request.is_ajax() and request.method == 'GET':
        exchanges = Exchange.objects.all()
        statuses = []

        for exchange in exchanges:
            exchangetrades = Trade.objects.filter(source=exchange)
            if exchangetrades.count() > 0:    
                tradeaggregates = exchangetrades.aggregate(Max('tradedate'),
                                                           Min('tradedate'))
                statuses.append(
                    {'exchange': exchange.name,
                    'numtrades': exchangetrades.count(),
                    'mintradedate': prettytime(tradeaggregates['tradedate__min']),
                    'maxtradedate': prettytime(tradeaggregates['tradedate__max'])},)
        return HttpResponse(json.dumps(statuses), content_type='application/json')
    else:
        print('Error loading exchange data...');
        raise Http404

def getsequence(request):
    print('Attempting to load chart data...')
    if request.is_ajax() and request.method != 'GET':
        print('This is not a valid GET request!');
        return HttpResponse(json.dumps({'message': 'Unauthorized attempt!'}), \
                            content_type='application/json', status=400)
    elif request.is_ajax() and request.method == 'GET':
        sequence = sequence_ohlc(request.GET.get('basecurrency'),
            request.GET.get('quotecurrency'),
            request.GET.get('datasource'),
            request.GET.get('starttime'),
            request.GET.get('endtime'),
            request.GET.get('interval'))

        json_sequence = sequence.to_dict(orient='records')

        print('%s periods received!' % len(sequence))
        return HttpResponse(json.dumps(json_sequence),
                            content_type='application/json', status=200)
    else:
        print('Error retrieving chart data...');
        return Http404

def getchart(request):
    print('Attempting to load chart data...')
    if request.is_ajax() and request.method != 'GET':
        print('This is not a valid GET request!');
        return HttpResponse(json.dumps({'message': 'Unauthorized attempt!'}),
                            content_type='application/json', status=400)
    elif request.is_ajax() and request.method == 'GET':
        sequence = sequence_ohlc(
            request.GET.get('basecurrency'),
            request.GET.get('quotecurrency'),
            request.GET.get('datasource'),
            request.GET.get('starttime'),
            request.GET.get('endtime'),
            request.GET.get('interval'))

        if sequence is None:
            errormessage = 'No periods exist for this time period!'
            print(errormessage)
            return HttpResponse(json.dumps({'error': errormessage}),
                                content_type='application/json', status=400)

        if 'error' in sequence:
            errormessage = sequence['error']
            print(errormessage)
            return HttpResponse(json.dumps({'error': errormessage}),
                                content_type='application/json', status=400)

        print('%s periods received!' % len(sequence))

        json_sequence = sequence.to_dict(orient='records')
        final_sequence = [{'period': p['period'], \
                           'open': '%.6f' % p['open'], \
                           'high': '%.6f' % p['high'], \
                           'low': '%.6f' % p['low'], \
                           'close': '%.6f' % p['close'], \
                           'volume': '%.2f' % p['volume']
                          } for p in json_sequence]

        print('Success! Returning sequence...')
        return HttpResponse(json.dumps(final_sequence),
                            content_type='application/json', status=200)
    else:
        print('Error retrieving chart data...');
        return Http404

def get_indicator_sequence(source, base, quote, starttime, endtime, interval,
    tsi_short, tsi_long, rsi_length, dilen, adxlen, wvf_length,
    use_existing='true'):
    
    print('INFO >>> Retrieving %ss %s-%s %s candles between %s and %s...' % 
        (interval, base, quote, source, starttime, endtime))
    sequence = []
    
    if use_existing == "true":
        print('INFO >>> Retrieving indicator values...')
        sequence = sequence_ohlc(base, quote, source,
            starttime, endtime, interval, True)

    elif use_existing == "false":
        print('INFO >>> Calculating indicator values...')
        try:
            sequence = sequence_ohlc(base, quote, source,
                starttime, endtime, interval, False)
        except Exception as e:
            print("ERROR >>> Unable to retrieve candles: %s" % str(e))
            return {'error': "Unable to retrieve candles!"}
        
        if sequence is None or len(sequence) == 0:
            print("INFO >>> No candles during this time period.")
            return {'error': "No candles during this time period."}

        print('INFO >>> Success! %s periods received.' % len(sequence))
        print('INFO >>> Calculating indicator values...')
        
        # Standard Moving Average (SMA)
        try:
            sequence['sma20'] = sma(sequence, 'ohlc4', 20)
            sequence['sma50'] = sma(sequence, 'ohlc4', 50)
            sequence['sma100'] = sma(sequence, 'ohlc4', 100)
        except:
            return {'error': "Unable to process SMA!"}
            
        # True Strength Indicator (TSI)
        try:
            tsi_sequence = tsi(sequence, 'close', tsi_short, tsi_long)
            sequence['tsi'] = tsi_sequence['tsi']
            sequence['tsi_signal'] = tsi_sequence['tsi_signal']
        except:
            return {'error': "Unable to process TSI!"}
        
        # Relative Strength Index (RSI)    
        try:
            sequence['rsi'] = rsi(sequence, 'close', rsi_length)
        except:
            return {'error': "Unable to process RSI!"}
            
        # Average Directional Index (ADX)
        try:
            sequence['adx'] = adx(sequence, dilen, adxlen)['adx']
        except:
            return {'error': "Unable to process ADX!"}
            
        # Williams VIX Fix (WVF)
        try:
            sequence['wvf'] = wvf(sequence, wvf_length)
        except:
            return {'error': "Unable to process WVF!"}
            
        # Verbatim Index (VBI)
        try:
            sequence['vbi'] = 0 # Not yet implemented
        except:
            return {'error': "Unable to process VBI!"}
    
    else:
        print("ERROR >>> Invalid input for use_existing: %s" % use_existing)

    json_sequence = sequence.to_dict(orient='records')
    final_sequence = [{
        'period': p['period'],
        'open': '%.6f' % p['open'],
        'high': '%.6f' % p['high'],
        'low': '%.6f' % p['low'],
        'close': '%.6f' % p['close'],
        'volume': '%.2f' % p['volume'],
        'sma20': '%.2f' % p['sma20'],
        'sma50': '%.2f' % p['sma50'],
        'sma100': '%.2f' % p['sma100'],
        'tsi': '%.2f' % p['tsi'],
        'tsi_signal': '%.2f' % p['tsi_signal'],
        'rsi': '%.2f' % p['rsi'],
        'adx': '%.2f' % p['adx'],
        'wvf': '%.2f' % p['wvf'],
        'vbi': '%d' % p['vbi'],
        } for p in json_sequence]

    return final_sequence

def getindicators(request):
    if request.is_ajax() and request.method != 'GET':
        print('ERROR >>> This is not a valid GET request!');
        return HttpResponse(json.dumps({'message': 'Unauthorized attempt!'}),
            content_type='application/json', status=400)
    elif request.is_ajax() and request.method == 'GET':
        sequence = get_indicator_sequence(
            request.GET.get('datasource'),
            request.GET.get('basecurrency'),
            request.GET.get('quotecurrency'),
            request.GET.get('starttime'),
            request.GET.get('endtime'),
            request.GET.get('interval'),
            int(request.GET.get('tsi_short')),
            int(request.GET.get('tsi_long')),
            int(request.GET.get('rsi_length')),
            int(request.GET.get('dilen')),
            int(request.GET.get('adxlen')),
            int(request.GET.get('wvf_length')),
            request.GET.get('use_existing')
            )

        if 'error' in sequence:
            return HttpResponse(json.dumps(sequence),
                content_type='application/json', status=400)

        print('INFO >>> Success! Returning sequence with indicators...')
        return HttpResponse(json.dumps(sequence),
            content_type='application/json', status=200)
    else:
        print('ERROR >>> Error retrieving indicator data...');
        return Http404

def runanalysis(request):
    if request.is_ajax() and request.method != 'GET':
        print('ERROR >>> This is not a valid GET request!');
        return HttpResponse(json.dumps({'message': 'Unauthorized attempt!'}),
            content_type='application/json', status=400)
    
    elif request.is_ajax() and request.method == 'GET':
        sequence = get_indicator_sequence(
            request.GET.get('datasource'),
            request.GET.get('basecurrency'),
            request.GET.get('quotecurrency'),
            request.GET.get('starttime'),
            request.GET.get('endtime'),
            request.GET.get('interval'),
            int(request.GET.get('tsi_short')),
            int(request.GET.get('tsi_long')),
            int(request.GET.get('rsi_length')),
            int(request.GET.get('dilen')),
            int(request.GET.get('adxlen')),
            int(request.GET.get('wvf_length')),
            'false'
            )

        if 'error' in sequence:
            return HttpResponse(json.dumps(sequence),
                content_type='application/json', status=400)

        print('INFO >>> Success! Returning sequence with indicators...')
        return HttpResponse(json.dumps(sequence),
            content_type='application/json', status=200)
    
    else:
        print('ERROR >>> Error retrieving analysis data...');
        return Http404

def saveanalysis(request):
    if request.is_ajax() and request.method != 'POST':
        print('This is not a valid POST request!');
        return HttpResponse(json.dumps({'message': 'Unauthorized attempt!'}),
            content_type='application/json', status=400)
    
    elif request.is_ajax() and request.method == 'POST':
        sequence = get_indicator_sequence(
            request.POST.get('datasource'),
            request.POST.get('basecurrency'),
            request.POST.get('quotecurrency'),
            request.POST.get('starttime'),
            request.POST.get('endtime'),
            request.POST.get('interval'),
            int(request.POST.get('tsi_short')),
            int(request.POST.get('tsi_long')),
            int(request.POST.get('rsi_length')),
            int(request.POST.get('dilen')),
            int(request.POST.get('adxlen')),
            int(request.POST.get('wvf_length')),
            'false'
            )

        try:
            print("INFO >>> Saving indicator values to the candles...")
            saveindicators(
                sequence,
                request.POST.get('datasource'),
                request.POST.get('basecurrency'),
                request.POST.get('quotecurrency'),
                request.POST.get('starttime'),
                request.POST.get('endtime'),
                request.POST.get('interval'),
                )
        except Exception as e:
            print("ERROR >>> Unable to save indicator values: %s" % str(e))
            return HttpResponse(json.dumps({'message': str(e)}),
                content_type='application/json', status=400)

        print('INFO >>> Success! Returning analyzed data...')
        return HttpResponse(json.dumps(sequence),
          content_type='application/json', status=200)
    
    else:
        print('ERROR >>> Error saving analysis data...');
        return HttpResponse(json.dumps({'message': 'Unauthorized attempt!'}),
            content_type='application/json', status=400)

def runstrategy(request):
    if request.is_ajax() and request.method != 'GET':
        print('ERROR >>> This is not a valid GET request!');
        return HttpResponse(json.dumps({'message': 'Unauthorized attempt!'}),
            content_type='application/json', status=400)
    
    elif request.is_ajax() and request.method == 'GET':
        sequence = get_indicator_sequence(
            request.GET.get('datasource'),
            request.GET.get('basecurrency'),
            request.GET.get('quotecurrency'),
            request.GET.get('starttime'),
            request.GET.get('endtime'),
            request.GET.get('interval'),
            int(request.GET.get('tsi_short')),
            int(request.GET.get('tsi_long')),
            int(request.GET.get('rsi_length')),
            int(request.GET.get('dilen')),
            int(request.GET.get('adxlen')),
            int(request.GET.get('wvf_length'))
            )

        # Threshold Strategy
        if request.GET.get('threshold') == 'true':
            strategy_sequence = threshold_strategy(sequence,
                request.GET.get('threshold_lower'),
                request.GET.get('threshold_upper'),
                request.GET.get('backtest'),
                request.GET.get('deposit'),
                request.GET.get('investment'),
                request.GET.get('trade_percentage'))

        # Buy-and-Hold Strategy
        if request.GET.get('buyandhold') == 'true':
            strategy_sequence = buyandhold_strategy(sequence,
                request.GET.get('buyandhold_lower'),
                request.GET.get('backtest'),
                request.GET.get('deposit'),
                request.GET.get('investment'),
                request.GET.get('trade_percentage'))

        for p in strategy_sequence['sequence']:
            p['open'] = '%.6f' % p['open']
            p['high'] = '%.6f' % p['high']
            p['low'] = '%.6f' % p['low']
            p['close'] = '%.6f' % p['close']
            p['volume'] = '%.2f' % p['volume']
            p['wvf'] = '%.2f' % p['wvf']
            
        print('Success! Returning sequence with strategy results...')
        return HttpResponse(json.dumps(strategy_sequence),
            content_type='application/json', status=200)
    
    else:
        print('Error retrieving strategy data...');
        return Http404

def createcandles(request):
    if request.is_ajax() and request.method != 'POST':
        print('ERROR >>> This is not a valid POST request!');
        return HttpResponse(json.dumps({'message': 'Unauthorized attempt!'}),
                            content_type='application/json', status=400)
    
    elif request.is_ajax() and request.method == 'POST':
        print(('INFO >>> Creating {0} second {1}/{2} candles for {3} ' + 
            'from {4} to {5}')
            .format(
                request.POST.get('interval'),
                request.POST.get('basecurrency'),
                request.POST.get('quotecurrency'),
                request.POST.get('datasource'),
                request.POST.get('starttime'),
                request.POST.get('endtime')
                ))

        response = sequencer(
            request.POST.get('basecurrency'),
            request.POST.get('quotecurrency'),
            request.POST.get('datasource'),
            request.POST.get('interval'),
            request.POST.get('rewrite'),
            request.POST.get('fromsource'),
            request.POST.get('starttime'),
            request.POST.get('endtime')
        )

        print('INFO >>> Successfully created candles!')
        return HttpResponse(json.dumps(response),
          content_type='application/json', status=200)
    
    else:
        print('ERROR >>> Error creating candles...');
        return HttpResponse(json.dumps({'message': 'Unauthorized attempt!'}),
                            content_type='application/json', status=400)

def removecandles(request):
    if request.is_ajax() and request.method != 'POST':
        print('ERROR >>> This is not a valid POST request!');
        return HttpResponse(json.dumps({'message': 'Unauthorized attempt!'}),
                            content_type='application/json', status=400)
    
    elif request.is_ajax() and request.method == 'POST':
        print(('INFO >>> Deleting all {0} second {1}/{2} candles ' + 
            'for {3} from {4} to {5}').format(
                request.POST.get('interval'),
                request.POST.get('basecurrency'),
                request.POST.get('quotecurrency'),
                request.POST.get('datasource'),
                request.POST.get('starttime'),
                request.POST.get('endtime')))

        response = deletecandles(
              request.POST.get('datasource'),
              request.POST.get('basecurrency'),
              request.POST.get('quotecurrency'),
              request.POST.get('starttime'),
              request.POST.get('endtime'),
              request.POST.get('interval'))

        print('INFO >>> Successfully deleted candles!')
        return HttpResponse(json.dumps(response),
          content_type='application/json', status=200)
    
    else:
        print('ERROR >>> Unable to remove candles...');
        return HttpResponse(json.dumps({'message': 'Unauthorized attempt!'}),
                            content_type='application/json', status=400)

def autocandlemaker(request):
    if request.is_ajax() and request.method != 'POST':
        print('ERROR >>> This is not a valid POST request!');
        return HttpResponse(json.dumps({'message': 'Unauthorized attempt!'}),
                            content_type='application/json', status=400)
    
    elif request.is_ajax() and request.method == 'POST':
        base = request.POST.get('basecurrency')
        quote = request.POST.get('quotecurrency')
        source = request.POST.get('datasource')

        print('Starting automatic candle maker.')
        print('Base Currency: %s | Quote Currency: %s | Data Source: %s' %
          (base, quote, source))
        auto_sequencer(base, quote, source, '300')

        response = 'Success'
        print('Success!')
        return HttpResponse(json.dumps(response),
          content_type='application/json', status=200)
    
    else:
        print('Error automatically creating candles...');
        return HttpResponse(json.dumps({'message': 'Unauthorized attempt!'}),
                            content_type='application/json', status=400)

def candlecombiner(request):
    if request.is_ajax() and request.method != 'POST':
        print('This is not a valid POST request!');
        return HttpResponse(json.dumps({'message': 'Unauthorized attempt!'}),
                            content_type='application/json', status=400)
    
    elif request.is_ajax() and request.method == 'POST':
        source = request.POST.get('datasource')
        symbol1 = request.POST.get('basecurrency')
        symbol2 = request.POST.get('quotecurrency')
        starttime = request.POST.get('starttime')
        endtime = request.POST.get('endtime')
        update = request.POST.get('update')

        print(('Combining 5m candles into larger candles for %s/%s ' + 
          'on %s between %s and %s.') %
            (symbol1, symbol2, source, starttime, endtime))

        combinecandles(source, symbol1, symbol2, starttime, endtime)

        response = 'Success'
        print('Success!')
        return HttpResponse(json.dumps(response),
          content_type='application/json', status=200)
    
    else:
        print('Error combining candles...')
        return HttpResponse(json.dumps({'message': 'Unauthorized attempt!'}),
                            content_type='application/json', status=400)

def autocombine(request):
    if request.is_ajax() and request.method != 'POST':
        print('ERROR >>> This is not a valid POST request!');
        return HttpResponse(json.dumps({'message': 'Unauthorized attempt!'}),
          content_type='application/json', status=400)
    
    elif request.is_ajax() and request.method == 'POST':
        source = request.POST.get('datasource')
        base = request.POST.get('basecurrency')
        quote = request.POST.get('quotecurrency')

        print('INFO >>> Auto-combining %s/%s candles on %s.' %
            (base, quote, source))

        auto_combine(source, base, quote)

        response = 'Success'
        print('INFO >>> Successfully auto-combined candles!')
        return HttpResponse(json.dumps(response),
          content_type='application/json', status=200)
    
    else:
        print('ERROR >>> Error combining candles...')
        return HttpResponse(json.dumps({'message': 'Unauthorized attempt!'}),
          content_type='application/json', status=400)

def reviewverbatim(request):
    if request.is_ajax() and request.method != 'GET':
        print('ERROR >>> This is not a valid GET request!');
        return HttpResponse(json.dumps({'message': 'Unauthorized attempt!'}),
          content_type='application/json', status=400)

    elif request.is_ajax() and request.method == 'GET':
        source = request.GET.get('datasource')
        base = request.GET.get('basecurrency')
        quote = request.GET.get('quotecurrency')
        starttime = request.GET.get('starttime')
        endtime = request.GET.get('endtime')

        print(('INFO >>> Pulling Verbatim indices ' +
            'for {0} {1}/{2} candles between {3} and {4})')
            .format(source, base, quote, starttime, endtime))

        indices = retrieve_verbatim(source, base, quote, starttime, endtime)

        response = [{
            'period': '{0:d}'.format(vindex['period']),
            'strength': '{0:.2f}'.format(vindex['strength']),
            'trend': '{0:.2f}'.format(vindex['trend']),
            'momentum': '{0:.2f}'.format(vindex['momentum']),
            'volatility': '{0:.2f}'.format(vindex['volatility']),
            'volume': '{0:.2f}'.format(vindex['volume']),
            'verbatim': '{0:d}'.format(vindex['verbatim']),
        } for vindex in indices]

        print('INFO >>> Successfully created Verbatim indices!')
        return HttpResponse(json.dumps(response),
          content_type='application/json', status=200)
    
    else:
        print('ERROR >>> Error creating Verbatim indices...')
        return HttpResponse(json.dumps({'message': 'Unauthorized attempt!'}),
          content_type='application/json', status=400)

def createverbatim(request):
    if request.is_ajax() and request.method != 'POST':
        print('ERROR >>> This is not a valid POST request!');
        return HttpResponse(json.dumps({'message': 'Unauthorized attempt!'}),
          content_type='application/json', status=400)
    
    elif request.is_ajax() and request.method == 'POST':
        source = request.POST.get('datasource')
        base = request.POST.get('basecurrency')
        quote = request.POST.get('quotecurrency')
        starttime = request.POST.get('starttime')
        endtime = request.POST.get('endtime')
        interval = int(request.POST.get('interval'))

        print(('INFO >>> Calculating Verbatim indices over {0}s intervals ' +
            'for {1} {2}/{3} candles between {4} and {5})').format(
                interval, source, base, quote, starttime, endtime
            ))

        indices = store_verbatim(source, base, quote,
            starttime, endtime, interval)

        response = [{
            'period': '{0:d}'.format(vindex['period']),
            'strength': '{0:.2f}'.format(vindex['strength']),
            'trend': '{0:.2f}'.format(vindex['trend']),
            'momentum': '{0:.2f}'.format(vindex['momentum']),
            'volatility': '{0:.2f}'.format(vindex['volatility']),
            'volume': '{0:.2f}'.format(vindex['volume']),
            'verbatim': '{0:d}'.format(vindex['verbatim']),
        } for vindex in indices]

        print('INFO >>> Successfully created Verbatim indices!')
        return HttpResponse(json.dumps(response),
          content_type='application/json', status=200)
    
    else:
        print('ERROR >>> Error creating Verbatim indices...')
        return HttpResponse(json.dumps({'message': 'Unauthorized attempt!'}),
          content_type='application/json', status=400)

def deleteverbatim(request):
    if request.is_ajax() and request.method != 'POST':
        print('ERROR >>> This is not a valid POST request!');
        return HttpResponse(json.dumps({'message': 'Unauthorized attempt!'}),
          content_type='application/json', status=400)

    elif request.is_ajax() and request.method == 'POST':
        source = request.POST.get('datasource')
        base = request.POST.get('basecurrency')
        quote = request.POST.get('quotecurrency')
        starttime = request.POST.get('starttime')
        endtime = request.POST.get('endtime')

        print(('INFO >>> Deleting Verbatim indices ' +
            'for {0} {1}/{2} candles between {3} and {4})')
            .format(source, base, quote, starttime, endtime))

        response = delete_verbatim(source, base, quote, starttime, endtime)

        print('INFO >>> Successfully created Verbatim indices!')
        return HttpResponse(json.dumps(response),
          content_type='application/json', status=200)
    
    else:
        print('ERROR >>> Error deleting Verbatim indices...')
        return HttpResponse(json.dumps({'message': 'Unauthorized attempt!'}),
          content_type='application/json', status=400)