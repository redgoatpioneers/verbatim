from django.contrib import admin

from jads.models import Trade
from jads.models import Currency
from jads.models import Exchange
from jads.models import Candle

# Register your models here.
admin.site.register(Trade)
admin.site.register(Currency)
admin.site.register(Exchange)
admin.site.register(Candle)