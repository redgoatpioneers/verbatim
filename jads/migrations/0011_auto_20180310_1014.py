# Generated by Django 2.0.2 on 2018-03-10 16:14

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('jads', '0010_auto_20180308_2041'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='indicator',
            name='candle',
        ),
        migrations.AddField(
            model_name='candle',
            name='adx',
            field=models.DecimalField(decimal_places=10, default=0, max_digits=20),
        ),
        migrations.AddField(
            model_name='candle',
            name='atr',
            field=models.DecimalField(decimal_places=10, default=0, max_digits=20),
        ),
        migrations.AddField(
            model_name='candle',
            name='rsi',
            field=models.DecimalField(decimal_places=10, default=0, max_digits=20),
        ),
        migrations.AddField(
            model_name='candle',
            name='sma100',
            field=models.DecimalField(decimal_places=10, default=0, max_digits=20),
        ),
        migrations.AddField(
            model_name='candle',
            name='sma20',
            field=models.DecimalField(decimal_places=10, default=0, max_digits=20),
        ),
        migrations.AddField(
            model_name='candle',
            name='sma50',
            field=models.DecimalField(decimal_places=10, default=0, max_digits=20),
        ),
        migrations.AddField(
            model_name='candle',
            name='tsi',
            field=models.DecimalField(decimal_places=10, default=0, max_digits=20),
        ),
        migrations.AddField(
            model_name='candle',
            name='tsi_signal',
            field=models.DecimalField(decimal_places=10, default=0, max_digits=20),
        ),
        migrations.AddField(
            model_name='candle',
            name='vbi',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='candle',
            name='wvf',
            field=models.DecimalField(decimal_places=10, default=0, max_digits=20),
        ),
        migrations.DeleteModel(
            name='Indicator',
        ),
    ]
