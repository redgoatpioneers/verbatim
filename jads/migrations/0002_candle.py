# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-10-01 22:51
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('jads', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Candle',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('startdate', models.IntegerField()),
                ('interval', models.IntegerField()),
                ('open', models.DecimalField(decimal_places=10, max_digits=20)),
                ('high', models.DecimalField(decimal_places=10, max_digits=20)),
                ('low', models.DecimalField(decimal_places=10, max_digits=20)),
                ('close', models.DecimalField(decimal_places=10, max_digits=20)),
                ('source', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='jads.Exchange')),
                ('symbol1', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='symbol1_candleset', to='jads.Currency')),
                ('symbol2', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='symbol2_candleset', to='jads.Currency')),
            ],
        ),
    ]
