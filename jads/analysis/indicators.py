import datetime
import time
import operator

'''
INDICATORS.PY
------------------------------
Created by Jacob Ashton
Red Goat Pioneers (c) October 2017

'''

# --------------------------------------------------------
# Miscellaneous functions
# --------------------------------------------------------

def prettytime(unixtimestamp):
    return datetime.datetime.fromtimestamp(int(unixtimestamp)).strftime('%Y-%m-%d %H:%M:%S')

# --------------------------------------------------------
# Math functions
# --------------------------------------------------------

def nz(value, default=0):
    return value if float('-inf') < float(value) < float('inf') else default

# --------------------------------------------------------
# Price conversion functions
# --------------------------------------------------------

def ohlc4(ohlc_period):
    return (float(ohlc_period["open"]) + float(ohlc_period["high"]) + \
            float(ohlc_period["low"]) + float(ohlc_period["close"]))/4

def tr(ohlc_sequence):
    sequence = sorted(ohlc_sequence, key=operator.itemgetter("period"))

    sequence[0]["tr"] = float(sequence[0]["high"]) - float(sequence[0]["low"])
    for i,p in enumerate(sequence[1:]):
        if not sequence[i]:
            p["tr"] = float(sequence[0]["high"]) - float(sequence[0]["low"])
        else:
            p["tr"] =  max(float(p["high"]) - float(p["low"]), \
                           abs(float(p["high"]) - float(sequence[i]["close"])), \
                           abs(float(p["low"]) - float(sequence[i]["close"])))

    #debug
    #for i,p in enumerate(sequence[::10]):
    #    print("DEBUG TR || Period: %s | TR: %.2f" % \
    #          (i+1, p["tr"]))


    return [{"period": p["period"], "tr": p["tr"]} for p in sequence]

def rma(price, previous_rma, length):
    return (float(previous_rma) * (length-1) + float(price)) / length

# --------------------------------------------------------
# Indicator functions
# --------------------------------------------------------

def sma(price_sequence, length):
    sequence = sorted(price_sequence, key=operator.itemgetter("period"))

    for i, period in enumerate(sequence):
        sum_price = 0
        #For the first few, the lookback needs to be everything up to the length
        #Afterward, only use the last x periods, where x is the length
        lookback = i-length if i >= length else None
        for previous in sequence[lookback:i+1]:
            sum_price += period["price"]
        period["sma"] = sum_price / (i-lookback+1 if lookback else i+1)

    return [{"period": p["period"], "sma": p["sma"]} for p in sequence]

def ema(price_sequence, length):
    sequence = sorted(price_sequence, key=operator.itemgetter("period"))

    #Calculate initial SMAs to use as the first EMAs
    sum_price = 0
    for i, period in enumerate(sequence[:length]):
        sum_price += period["price"]
        period["ema"] = sum_price / (i+1)

    #Calculate the weighting multiplier
    weight = (2.0 / (length + 1.0))

    #Calculate the exponential moving average for each period
    for i, period in enumerate(sequence[length:]):
        period["ema"] = (float(period["price"]) - float(sequence[length+i-1]["ema"])) * weight \
                         + float(sequence[length+i-1]["ema"])

    return [{"period": p["period"], "ema": p["ema"]} for p in sequence]

def price_change(price_sequence):
    for i, period in enumerate(price_sequence):
        period["change"] = (0 if i == 0 else (float(period["price"]) - float(price_sequence[i-1]["price"])))
    return price_sequence

def abs_price(price_sequence):
    return [{"period": p["period"], "abs": abs(p["price"])} for p in price_sequence]

def double_smooth(price_sequence, length_short, length_long):
    double_smooth = ema([{"period": p["period"], "price": p["ema"]} \
                         for p in ema(price_sequence, length_long)], length_short)

    return [{"period": p["period"], "double_smooth": p["ema"]} for p in double_smooth]

def tsi(price_sequence, length_short, length_long):
    sequence = [{"period": p["period"], "price": p["change"]} \
                for p in (price_change(sorted(price_sequence, key=operator.itemgetter("period"))))]
    abs_sequence = [{"period": p["period"], "price": p["abs"]} \
                    for p in (abs_price(sorted(sequence, key=operator.itemgetter("period"))))]

    ds_sequence = double_smooth(sequence, length_short, length_long)
    abs_ds_sequence = double_smooth(abs_sequence, length_short, length_long)

    for period in sequence:
        period["ds"] = next((p["double_smooth"] for p in ds_sequence \
                            if p["period"] == period["period"]), period["price"])

        period["abs_ds"] = next((p["double_smooth"] for p in abs_ds_sequence \
                            if p["period"] == period["period"]), period["price"])

        try:
            period["tsi"] = 100 * (float(period["ds"]) / float(period["abs_ds"]))
        except ZeroDivisionError:
            period["tsi"] = 50

    return [{"period": p["period"], "tsi": p["tsi"]} for p in sequence]

def rsi(price_sequence, length):
    sequence = [{"period": p["period"], "change": p["change"]} \
                for p in (price_change(sorted(price_sequence, key=operator.itemgetter("period"))))]

    for period in sequence[:length]:
        period["rsi"] = 50

    average_gain = sum(float(p["change"]) for p in sequence[:length] if float(p["change"]) > 0) / length
    average_loss = sum(abs(float(p["change"])) for p in sequence[:length] if float(p["change"]) < 0) / length
    
    for p in sequence[length:]:
        gain = float(p["change"]) > 0
        loss = float(p["change"]) < 0
        average_gain = ((float(p["change"]) if gain else 0) + (average_gain * (length - 1))) \
                        / length
        average_loss = ((abs(float(p["change"])) if loss else 0) + (average_loss * (length - 1))) \
                        / length
        try:
            p["rsi"] = 100 - (100 / (1 + (average_gain / average_loss)))
        except ZeroDivisionError:
            p["rsi"] = 100

    return [{"period": p["period"], "rsi": p["rsi"]} for p in sequence]

def rma_sequence(price_sequence, length):
    sequence = [{"period": p["period"], "price": p["price"]} \
                for p in (sorted(price_sequence, key=operator.itemgetter("period")))]

    sequence[0]["rma"] = 0

    for i,p in enumerate(sequence[1:]):
        p["rma"] = rma(float(p["price"]), float(sequence[i]["rma"]), length)

    #debug
    #for i,p in enumerate(sequence[::10]):
    #    print("DEBUG RMA_SEQUENCE || Period: %s | RMA: %.2f" % \
    #          (i+1, p["rma"]))

    return [{"period": p["period"], "rma": p["rma"]} for p in sequence]

def directional_movement(ohlc_sequence, high_sequence, low_sequence, length=14):
    sequence = sorted(ohlc_sequence, key=operator.itemgetter("period"))

    up = [{"period": p["period"], "change": p["change"]} \
                for p in (price_change(sorted(high_sequence, key=operator.itemgetter("period"))))]

    down = [{"period": p["period"], "change": -float(p["change"])} \
                for p in (price_change(sorted(low_sequence, key=operator.itemgetter("period"))))]

    tr_sequence = tr(sequence)
    
    truerange = rma_sequence([{"period": p["period"], "price": p["tr"]} \
                for p in tr_sequence], length)

    for p in sequence:
        p["up"] = next((pd["change"] for pd in up if pd["period"] == p["period"]), 0)
        p["down"] = next((pd["change"] for pd in down if pd["period"] == p["period"]), 0)
        p["tr"] = next((pd["tr"] for pd in tr_sequence if pd["period"] == p["period"]), 0)
        p["truerange"] = next((pd["rma"] for pd in truerange if pd["period"] == p["period"]), 0)
        p["plus_rma"] = float(p["up"]) if (p["up"] > p["down"] and p["up"] > 0) else 0
        p["minus_rma"] = float(p["down"]) if (p["down"] > p["up"] and p["down"] > 0) else 0

    plus_rma_sequence = rma_sequence([{"period": p["period"], "price": p["plus_rma"]} \
                                     for p in sequence], length)
    minus_rma_sequence = rma_sequence([{"period": p["period"], "price": p["minus_rma"]} \
                                     for p in sequence], length)

    for p in sequence:
        if float(p["truerange"]) == 0:
            p["plus"] = 0
            p["minus"] = 0
        else:
            p["plus"] = next((100 * float(pd["rma"]) / float(p["truerange"])) \
                             for pd in plus_rma_sequence if pd["period"] == p["period"])
            p["minus"] = next((100 * float(pd["rma"]) / float(p["truerange"])) \
                             for pd in minus_rma_sequence if pd["period"] == p["period"])

    #debug
    #for i,p in enumerate(sequence[780:800]):
    #    print("DEBUG DIRMOV || Period: %s | UP: %.2f | DN: %.2f | TR: %.2f | True: %.2f | P: %.2f | M %.2f" % \
    #          (prettytime(p["period"]), p["up"], p["down"], p["tr"], p["truerange"], p["plus"], p["minus"]))

    return [{"period": p["period"], "plus": p["plus"], "minus": p["minus"]} for p in sequence]

def adx(ohlc_sequence, adxlen, dilen):
    sequence = sorted(ohlc_sequence, key=operator.itemgetter("period"))
    high_sequence = [{"period": p["period"], "price": p["high"]} for p in sequence]
    low_sequence = [{"period": p["period"], "price": p["low"]} for p in sequence]

    dirmov = directional_movement(sequence, high_sequence, low_sequence, dilen)
    dirmov[0]["adx_rma_value"] = abs(float(dirmov[0]["plus"]) - float(dirmov[0]["minus"])) / \
                                 (1 if (float(dirmov[0]["plus"]) + float(dirmov[0]["minus"])) == 0 \
                                    else (float(dirmov[0]["plus"]) + float(dirmov[0]["minus"])))
    dirmov[0]["adx"] = 100 * rma(dirmov[0]["adx_rma_value"], dirmov[0]["adx_rma_value"], adxlen)

    for i,p in enumerate(dirmov[1:]):
        plus = float(p["plus"])
        minus = float(p["minus"])
        adx_sum = plus + minus
        p["adx_rma_value"] = abs(plus - minus) / (1 if adx_sum == 0 else adx_sum)
        p["adx"] = 100 * rma(p["adx_rma_value"], dirmov[i]["adx_rma_value"], adxlen)

    #debug
    #for i,p in enumerate(dirmov[::10]):
    #    print("DEBUG ADX || Period: %s | ADX_RMA: %s | ADX %s" % (i+1, p["adx_rma_value"], p["adx"]))

    return [{"period": p["period"], "adx": p["adx"]} for p in dirmov]

def wvf(price_sequence, length):
    sequence = sorted(price_sequence, key=operator.itemgetter("period"))

    for i,p in enumerate(sequence):
        lookback = (i-length) if (i-length) >= 0 else 0
        closes = [p["price"] for p in sequence[lookback:i+1]]
        if len(closes) == 0:
            p["wvf"] = 0
            continue
        p["wvf"] = ( (max(closes) - p["price"]) / max(closes) ) * 10
        
        #debug
        #testmax = max(closes) if len(closes) > 0 else "N/A"
        #print("Period #%s | Start: %s | End %s | Max Close %s | VixFix: %s" % \
        #      (i+1, lookback+1, i+1, testmax, p["wvf"]))

    return [{"period": p["period"], "wvf": p["wvf"]} for p in sequence]

def jads(tsi_rsi_adx_wvf_signal):
    sequence = sorted(tsi_rsi_adx_wvf_signal, key=operator.itemgetter("period"))
    max_volume = max(p["volume"] for p in sequence)

    for i,p in enumerate(sequence):
        jads_base = (float(p["tsi"]) + (float(p["rsi"] - 50))) * float(p["adx"])
        p["jads"] = jads_base

    return [{"period": p["period"], "jads": p["jads"]} for p in sequence]

# --------------------------------------------------------------------
# Functions to add indicators as a key to an existing ohlc signal
# --------------------------------------------------------------------

def apply_sma(ohlc_sequence, length=10):
    print("Calculating SMA for %s periods" % len(ohlc_sequence))
    if len(ohlc_sequence) == 0:
        print("No periods to analyze!")
        return None
    
    sma_sequence = sma([{"period": p["period"], "price": ohlc4(p)} \
                            for p in sorted(ohlc_sequence, key=operator.itemgetter("period"))], length)

    for period in ohlc_sequence:
        period["sma"] = next((p["sma"] for p in sma_sequence if p["period"] == period["period"]), ohlc4(period))

    return sorted(ohlc_sequence, key=operator.itemgetter("period"))

def apply_ema(ohlc_sequence, length=10):
    print("Calculating EMA for %s periods" % len(ohlc_sequence))
    if len(ohlc_sequence) == 0:
        print("No periods to analyze!")
        return None
    
    ema_sequence = ema([{"period": p["period"], "price": ohlc4(p)} \
                            for p in sorted(ohlc_sequence, key=operator.itemgetter("period"))], length)
    
    for period in ohlc_sequence:
        period["ema"] = next((p["ema"] for p in ema_sequence if p["period"] == period["period"]), ohlc4(period))

    return sorted(ohlc_sequence, key=operator.itemgetter("period"))

def apply_tsi(ohlc_sequence, length_short=14, length_long=25):
    print("Calculating TSI for %s periods" % len(ohlc_sequence))
    if len(ohlc_sequence) == 0:
        print("No periods to analyze!")
        return None
    
    tsi_sequence = tsi([{"period": p["period"], "price": p["close"]} \
                            for p in sorted(ohlc_sequence, key=operator.itemgetter("period"))], \
                       length_short, length_long)
    
    for period in ohlc_sequence:
        period["tsi"] = next((p["tsi"] for p in tsi_sequence if p["period"] == period["period"]), 0)

    return sorted(ohlc_sequence, key=operator.itemgetter("period"))

def apply_rsi(ohlc_sequence, length=14):
    print("Calculating RSI for %s periods" % len(ohlc_sequence))
    if len(ohlc_sequence) == 0:
        print("No periods to analyze!")
        return None
    
    rsi_sequence = rsi([{"period": p["period"], "price": ohlc4(p)} \
                            for p in sorted(ohlc_sequence, key=operator.itemgetter("period"))], length)
    
    for period in ohlc_sequence:
        period["rsi"] = next((p["rsi"] for p in rsi_sequence if p["period"] == period["period"]), 50)

    return sorted(ohlc_sequence, key=operator.itemgetter("period"))

def apply_adx(ohlc_sequence, adxlen=14, dilen=14):
    print("Calculating ADX for %s periods" % len(ohlc_sequence))
    if len(ohlc_sequence) == 0:
        print("No periods to analyze!")
        return None
    
    adx_sequence = adx(sorted(ohlc_sequence, key=operator.itemgetter("period")), adxlen, dilen)
    
    for i,period in enumerate(ohlc_sequence):
        period["adx"] = next((p["adx"] for p in adx_sequence if p["period"] == period["period"]), 0)

    return sorted(ohlc_sequence, key=operator.itemgetter("period"))

def apply_wvf(ohlc_sequence, length=14):
    print("Calculating CM Wiliams Vix Fix for %s periods" % len(ohlc_sequence))
    if len(ohlc_sequence) == 0:
        print("No periods to analyze!")
        return None
    
    wvf_sequence = wvf([{"period": p["period"], "price": p["close"]} \
                            for p in sorted(ohlc_sequence, key=operator.itemgetter("period"))], length)
    
    for i,period in enumerate(ohlc_sequence):
        period["wvf"] = next((p["wvf"] for p in wvf_sequence if p["period"] == period["period"]), 0)

    return sorted(ohlc_sequence, key=operator.itemgetter("period"))

def apply_jads(tsi_rsi_adx_wvf_signal):
    print("Calculating JADS Power Oscillator for %s periods" % len(tsi_rsi_adx_wvf_signal))
    if len(tsi_rsi_adx_wvf_signal) == 0:
        print("No periods to analyze!")
        return None
    
    wvf_sequence = jads([{"period": p["period"], \
                          "tsi": p["tsi"], \
                          "rsi": p["rsi"], \
                          "adx": p["adx"], \
                          "wvf": p["wvf"], \
                          "volume": p["volume"]}
                         for p in sorted(tsi_rsi_adx_wvf_signal, key=operator.itemgetter("period"))])
    
    for i,period in enumerate(tsi_rsi_adx_wvf_signal):
        period["jads"] = next((p["jads"] for p in wvf_sequence if p["period"] == period["period"]), 0)

    return sorted(tsi_rsi_adx_wvf_signal, key=operator.itemgetter("period"))