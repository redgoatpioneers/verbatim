from __future__ import division

import time
import datetime
import operator

from django.db.models import Min, Max
from django.db import transaction

from jads.common.utilities import unixtime, prettytime
from jads.models import Exchange, Currency, Candle

'''
SEQEUNCER.PY
------------------------------
Created by Jacob Ashton
Red Goat Pioneers (c) January 2018

'''

def saveindicators(indicatordata, datasource, basecurrency, quotecurrency,
    starttime, endtime, interval):
    
    indicators = [indicator for indicator in indicatordata]

    print("INFO >>> Pulling candles for given timeframe to set up match...")
    candles = Candle.objects.filter(
        symbol1 = Currency.objects.get(name=basecurrency),
        symbol2 = Currency.objects.get(name=quotecurrency),
        startdate__gte = unixtime(starttime),
        startdate__lt = unixtime(endtime),
        interval = interval,
        source = Exchange.objects.get(name=datasource)
        )

    @transaction.atomic
    def bulk_save():
        candles_updated = 0
        with transaction.atomic():
            for candle in candles:
                for indicator in indicators:
                    if candle.startdate == indicator['period']:
                        candle.sma20  = indicator['sma20']
                        candle.sma50  = indicator['sma50']
                        candle.sma100  = indicator['sma100']
                        candle.tsi  = indicator['tsi']
                        candle.tsi_signal  = indicator['tsi_signal']
                        candle.rsi  = indicator['rsi']
                        candle.adx  = indicator['adx']
                        candle.wvf  = indicator['wvf']
                        candle.save()
                        candles_updated += 1
                        indicators.remove(indicator)
        return candles_updated

    print("INFO >>> Finding {0} candles and saving/updating analyses..."
        .format(candles.count()))

    try:
        candles_updated = bulk_save()
    except Exception as e:
        print("ERROR >>> Error updating candles: {0}".format(e))
        return
    
    print("INFO >>> Success! Saved indicators for {0}/{1} candles."
        .format(candles_updated, len(candles)))

def getverbatimdata(datasource):
    print("INFO >>> This functionality is being re-written! Come back later!")
    return {'error': 'This functionality is being re-written! Come back later!'}

    top10 = {
         '1': {'pair': ['BTC',  'USD'], 'intervals': {}},
         '2': {'pair': ['ETH',  'BTC'], 'intervals': {}},
         '3': {'pair': ['ADA',  'BTC'], 'intervals': {}},
         '4': {'pair': ['DASH', 'BTC'], 'intervals': {}},
         '5': {'pair': ['NEO',  'BTC'], 'intervals': {}},
         '6': {'pair': ['LTC',  'BTC'], 'intervals': {}},
         '7': {'pair': ['XMR',  'BTC'], 'intervals': {}},
         '8': {'pair': ['EOS',  'BTC'], 'intervals': {}},
         '9': {'pair': ['TRX',  'BTC'], 'intervals': {}},
        '10': {'pair': ['VEN',  'BTC'], 'intervals': {}},
    }
    intervals = ['300', '900', '3600', '7200', '14400', '86400', '604800']
    interval_map = {
        '300':    '5m',
        '900':   '15m',
        '3600':   '1h',
        '7200':   '2h',
        '14400':  '4h',
        '86400':  '1d',
        '604800': '1w',
    }

    for index, pair in top10.items():
        for interval in intervals:
            latestcandle = None
            latestanalysis = None

            # Get the most recent candle
            try:
                basecurrency = Currency.objects.get(symbol=pair['pair'][0])
                quotecurrency = Currency.objects.get(symbol=pair['pair'][1])
                candles = Candle.objects.filter(
                    symbol1 = basecurrency,
                    symbol2 = quotecurrency,
                    interval = interval,
                    source = Exchange.objects.get(name=datasource)
                    )
                lateststartdate = candles.aggregate(
                    Max('startdate'))['startdate__max']
                latestcandle = candles.filter(startdate=lateststartdate)
            except Exception as e:
                #print("Error retrieving last candle: %s" % str(e))
                pass

            # Get the analysis for the most recent candle, if it exists
            try:
                latestanalysis = Indicator.objects.get(candle=latestcandle,
                    analysis='JADS')
            except Exception as e:
                print("ERROR >>> Unable to indicator for latest candle: %s"
                    % str(e))
                pass

            if latestanalysis is not None:
                pair['intervals'][interval] = {
                    'interval': interval_map[interval],
                    'period': '%.2f' % latestanalysis.candle.startdate,
                    'tsi': '%.2f' % latestanalysis.tsi,
                    'rsi': '%.2f' % latestanalysis.rsi,
                    'adx': '%.2f' % latestanalysis.rsi,
                    'wvf': '%.2f' % latestanalysis.rsi,
                    'jads': '%.2f' % latestanalysis.rsi,
                    'volume': '%.2f' % latestanalysis.candle.volume,
                }
            else:
                pair['intervals'][interval] = {
                    'interval': interval_map[interval],
                    'period': 'N/A',
                    'tsi':    'N/A',
                    'rsi':    'N/A',
                    'adx':    'N/A',
                    'wvf':    'N/A',
                    'jads':   'N/A',
                    'volume': 'N/A',
                }

    return top10