from datetime import datetime

import pandas
import numpy

from jads.common.utilities import prettytime

'''
INDICATORS.PY
-------------------------------------------------------------------------------
Created by Jacob Ashton
Red Goat Pioneers (c) February 2018

    INSTRUCTIONS
    ===========================================================================
    Any function requiring ohlc_dataframe as an input must be called with a
    pandas dataframe in the following format:

   ----------------------------------------------------------------------------
   | period | interval | open  | high  | low   | close | ohlc4 | volume | ... |
   ----------------------------------------------------------------------------
   | Int    | Int      | Float | Float | Float | Float | Float | Float  | ... |
   ----------------------------------------------------------------------------

   Period: Start time of the candle in Unixtime, no milliseconds
   Interval: Total length of the candle, in seconds (e.g. 300 = 5 m candles)
   Open/High/Low/Close: Decimal values for the OHLC prices. Must be positive.
   OHLC4: Decimal value representing (open+high+low+close)/4. Must be positive.
   Volume: Decimal value for the volume over this interval. Must be positive.
   *** Additional columns representing indicator values, etc. may be required.

'''

# -----------------------------------------------------------------------------
# Indicator functions
# -----------------------------------------------------------------------------

''' ___________________________________________________________________________
    Rate of Change
    ---------------------------------------------------------------------------
    PURPOSE:
        Adds Rate of Change (ROC) to an OHLC series.
    INPUTS:
        ohlc_dataframe: A pandas DataFrame containing ohlc sequence data.
        source: Either 'open', 'high', 'low', 'close', or 'ohlc4'.
        length: The number of periods to look back to determine ROC.
    OUTPUT:
        Returns the SMA values in a new pandas DataFrame.
    NOTES:
        Because the average requires a certain number of periods, the first
        few periods will be assigned the first SMA value calcuated.
        For example, if using length 5, then the first four periods will have
        an SMA value equal to the average of the first 5 periods.
'''


def roc(ohlc_dataframe, source, lookback):
    sequence = ohlc_dataframe.sort_values(by='period')

    sequence['roc'] = 0
    for i in range(lookback, len(sequence)):
        sequence.ix[i, 'roc'] = (
            100
            * (sequence.ix[i, source] - sequence.ix[i-lookback, source])
            / (sequence.loc[i-lookback, source]))

    return sequence['roc']


''' ___________________________________________________________________________
    Standard Moving Average
    ---------------------------------------------------------------------------
    PURPOSE:
        Adds Standard Moving Average (SMA) to an OHLC series.
    INPUTS:
        ohlc_dataframe: A pandas DataFrame containing ohlc sequence data.
        source: Either 'open', 'high', 'low', 'close', or 'ohlc4'.
        length: The number of periods to include in the average.
    OUTPUT:
        Returns the SMA values in a new pandas DataFrame.
    NOTES:
        Because the average requires a certain number of periods, the first
        few periods will be assigned the first SMA value calcuated.
        For example, if using length 5, then the first four periods will have
        an SMA value equal to the average of the first 5 periods.
'''


def sma(ohlc_dataframe, source='close', length=9):
    sequence = ohlc_dataframe.sort_values(by='period')
    smas = sequence[source].rolling(window=length).mean()

    return smas.fillna(method='bfill')


''' ___________________________________________________________________________
    Exponential Moving Average
    ---------------------------------------------------------------------------
    PURPOSE:
        Adds Exponential Moving Average (EMA) to an OHLC series.
    INPUTS:
        ohlc_dataframe: A pandas DataFrame containing ohlc sequence data.
        source: Either 'open', 'high', 'low', 'close', or 'ohlc4'.
        length: The number of periods to include in the average.
    OUTPUT:
        Returns the EMA values in a new pandas DataFrame.
    NOTES:
        Because the average requires a certain number of periods, the first
        few periods will be assigned the first EMA value calcuated. For
        example, if using length 5, then the first four periods will have
        an EMA value equal to the average of the first 5 periods.
'''


def ema(ohlc_dataframe, source, length):
    sequence = ohlc_dataframe.sort_values(by='period')
    emas = sequence[source].ewm(span=length).mean()

    return emas.fillna(method='bfill')


''' ___________________________________________________________________________
    Price Change
    ---------------------------------------------------------------------------
    PURPOSE:
        Adds the price change from the previous candle to an OHLC series.
    INPUTS:
        ohlc_dataframe: A pandas DataFrame containing ohlc sequence data.
        source: Either 'open', 'high', 'low', 'close', or 'ohlc4'.
    OUTPUT:
        Returns the price change values in a new pandas DataFrame.
    NOTES:
        The first candle will have a value of 0, since there was no previous
        candle.
'''


def price_change(ohlc_dataframe, source):
    sequence = ohlc_dataframe.sort_values(by='period')
    price_changes = sequence[source].diff()

    return price_changes.fillna(0)


''' ___________________________________________________________________________
    Price Change
    ---------------------------------------------------------------------------
    PURPOSE:
        Adds the absolute price value to an OHLC series.
    INPUTS:
        ohlc_dataframe: A pandas DataFrame containing ohlc sequence data.
        source: Either 'open', 'high', 'low', 'close', or 'ohlc4'.
    OUTPUT:
        Returns the absolute value of the prices in a new pandas DataFrame.
    NOTES:
        None.
'''


def abs_price(ohlc_dataframe, source):
    sequence = ohlc_dataframe.sort_values(by='period')
    absolute_prices = sequence.apply(lambda row: abs(row[source]), axis=1)

    return absolute_prices.fillna(method='bfill')


''' ___________________________________________________________________________
    Double Smooth
    ---------------------------------------------------------------------------
    PURPOSE:
        Adds the double-smoothed EMA to an OHLC series.
    INPUTS:
        ohlc_dataframe: A pandas dataframe containing ohlc sequence data.
        source: Either 'open', 'high', 'low', 'close', or 'ohlc4'.
                For TSI, another key may be specified with corresponding data.
        length_short: The EMA period length for the second smooth.
        length_long: The EMA period length for the first smooth. 
    OUTPUT:
        Returns the double-smoothed EMA values in a new pandas DataFrame.
    NOTES:
        Because the average requires a certain number of periods, the first
        few periods will be assigned the first EMA values calcuated.
        For example, if using length 5, then the first four periods will have
        an EMA value equal to the average of the first 5 periods.
'''


def double_smooth(ohlc_dataframe, source, length_short, length_long):
    sequence = ohlc_dataframe.sort_values(by='period')

    first_smooth = (sequence[source]
                    .ewm(span=length_long)
                    .mean()
                    .fillna(method='bfill'))

    second_smooth = (first_smooth
                     .ewm(span=length_short)
                     .mean()
                     .fillna(method='bfill'))

    return second_smooth


''' ___________________________________________________________________________
    True Strength Indicator
    ---------------------------------------------------------------------------
    PURPOSE:
        Adds the TSI to an OHLC series.
    INPUTS:
        ohlc_dataframe: A pandas dataframe containing ohlc sequence data.
        source: Either 'open', 'high', 'low', 'close', or 'ohlc4'.
        length_short: The EMA period length for the second smooth.
        length_long: The EMA period length for the first smooth. 
    OUTPUT:
        Returns the original dataframe but with a new 'tsi' column.
    NOTES:
        Because EMA averages requires a certain number of periods, the first
        few periods will be assigned the first EMA values calcuated.
        For example, if using length 5, then the first four periods will have
        an EMA value equal to the average of the first 5 periods.

        Any NaN values (e.g. 100/0) will be assigned a default value of 50.
    SOURCE:
        http://stockcharts.com/school/
            doku.php?id=chart_school:technical_indicators:true_strength_index
'''


def tsi(ohlc_dataframe, source='close',
        length_short=13, length_long=25, length_signal=13):
    sequence = ohlc_dataframe.sort_values(by='period')

    # Double-smoothed PC
    sequence['pc'] = price_change(sequence, source)
    sequence['ds_pc'] = \
        double_smooth(sequence, 'pc', length_short, length_long)

    # Double-smoothed Absolute PC
    sequence['abs_pc'] = price_change(sequence, source).abs()
    sequence['ds_abs_pc'] = \
        double_smooth(sequence, 'abs_pc', length_short, length_long)

    sequence['tsi'] = (100 * sequence['ds_pc']) / sequence['ds_abs_pc']
    sequence['tsi_signal'] = ema(sequence, ['tsi'], length_signal)

    return sequence[['tsi', 'tsi_signal']].fillna(50)


''' ___________________________________________________________________________
    Rolling Moving Average (developed for RSI)
    ---------------------------------------------------------------------------
    PURPOSE:
        Adds a Rolling Moving Average (RMA) to an OHLC series.
    INPUTS:
        ohlc_dataframe: A pandas DataFrame containing ohlc sequence data.
        source: Either 'open', 'high', 'low', 'close', or 'ohlc4'.
                For TSI, another key may be specified with corresponding data.
        length: The number of periods to include in the average.
    OUTPUT:
        Returns the RMA values in a new pandas DataFrame.
    NOTES:
        Because the average requires a certain number of periods, the first
        few periods will be assigned the first RMA value calcuated.
        For example, if using length 5, then the first four periods will have
        an RMA value equal to the average of the first 5 periods.
    SOURCE:
        https://www.tradingview.com/study-script-reference/#fun_rma
'''


def rma(ohlc_dataframe, source, length):
    sequence = ohlc_dataframe.sort_values(by='period')

    sequence.ix[0, 'rma'] = sequence.ix[0, source]
    for i in range(1, len(sequence)):
        sequence.ix[i, 'rma'] = (
            (sequence.ix[i-1, 'rma'] * (length - 1) + sequence.ix[i, source])
            / length)
    rmas = sequence['rma']

    return rmas.fillna(method='bfill')


''' ___________________________________________________________________________
    Relative Strength Index
    ---------------------------------------------------------------------------
    PURPOSE:
        Adds the Relative Strength Index (RSI) to an OHLC series.
    INPUTS:
        ohlc_dataframe: A pandas dataframe containing ohlc sequence data.
        source: Either 'open', 'high', 'low', 'close', or 'ohlc4'.
        length_short: The EMA period length for the second smooth.
        length_long: The EMA period length for the first smooth. 
    OUTPUT:
        Returns the TSI values in a new pandas DataFrame.
    NOTES:
        Because EMA averages requires a certain number of periods, the first
        few periods will be assigned the first EMA values calcuated.
        For example, if using length 5, then the first four periods will have
        an EMA value equal to the average of the first 5 periods.

        Any NaN values (e.g. 100/0) will be assigned a default value of 50.
'''


def rsi(ohlc_dataframe, source='close', length=14):
    sequence = ohlc_dataframe.sort_values(by='period')
    
    sequence['up'] = (sequence[source] - sequence[source].shift()).fillna(0)
    sequence['up'][sequence['up'] < 0] = 0

    sequence['down'] = (sequence[source].shift() - sequence[source]).fillna(0)
    sequence['down'][sequence['down'] < 0] = 0

    rs = rma(sequence, 'up', length) / rma(sequence, 'down', length)
    rsis = 100 - 100 / (1 + rs)

    return rsis.fillna(method='bfill')


''' ___________________________________________________________________________
    True Range
    ---------------------------------------------------------------------------
    PURPOSE:
        Adds the True Range to an OHLC series.
    INPUTS:
        ohlc_dataframe: A pandas DataFrame containing ohlc sequence data.
    OUTPUT:
        Returns the True Range values in a new pandas DataFrame.
    NOTES:
        Because the average requires a certain number of periods, the first
        few periods will be assigned the first SMA value calcuated.
        For example, if using length 5, then the first four periods will have
        an SMA value equal to the average of the first 5 periods.
'''


def tr(ohlc_dataframe):
    sequence = ohlc_dataframe.sort_values(by='period')

    sequence['atr1'] = abs(sequence['high'] - sequence['low'])
    sequence['atr2'] = abs(sequence['high'] - sequence['close'].shift())
    sequence['atr3'] = abs(sequence['low'] - sequence['close'].shift())

    trs = sequence[['atr1', 'atr2', 'atr3']].max(axis=1)

    return trs.fillna(method='bfill')


''' ___________________________________________________________________________
    Average Directional Index
    ---------------------------------------------------------------------------
    PURPOSE:
        Adds the Average Directional Index (ADI) to an OHLC series.
    INPUTS:
        ohlc_dataframe: A pandas DataFrame containing ohlc sequence data.
        length: The period length used in the RMA calculation.
    OUTPUT:
        Returns the positive and negative DM values in a new pandas DataFrame.
    NOTES:
        None.
    SOURCE: https://www.tradingview.com/wiki/Directional_Movement_(DMI)
'''


def adx(ohlc_dataframe, dilen=14, adxlen=14):
    sequence = ohlc_dataframe.sort_values(by='period')
    
    # 1) Find the up and down movements for each periods
    sequence['um'] = (sequence['high'] - sequence['high'].shift()).fillna(0)
    sequence['dm'] = (sequence['low'] - sequence['low'].shift()).fillna(0)
    
    # 2) Determine the direction of the movement comparing up vs down
    sequence['pdm'] = 0
    sequence.loc[(sequence['um'] > sequence['dm'])
                 & (sequence['um'] > 0), 'pdm'] = sequence['um']
    sequence['mdm'] = 0
    sequence.loc[(sequence['dm'] > sequence['um'])
                 & (sequence['dm'] > 0), 'mdm'] = sequence['dm']
    
    # 3) Calculate the average true range for each period
    sequence['tr'] = tr(sequence)
    sequence['atr'] = rma(sequence, 'tr', dilen)
    
    # 4) Calculate the positive and negative directional indices (+DM/-DM)
    sequence['prma'] = sequence['pdm'] / sequence['atr']
    sequence['mrma'] = sequence['mdm'] / sequence['atr']
    sequence['pdi'] = ((100 * rma(sequence, 'prma', dilen))
                       .fillna(method='bfill'))
    sequence['mdi'] = ((100 * rma(sequence, 'mrma', dilen))
                       .fillna(method='bfill'))
    
    # 5) Calculate the ADX
    sequence['arma'] = (
            (sequence['pdi'] - sequence['mdi'])
            / (sequence['pdi'] + sequence['mdi'])).abs().fillna(method='bfill')
    sequence['adx'] = 100 * rma(sequence, 'arma', adxlen)
    
    return sequence[['pdi', 'mdi', 'adx']]


''' ___________________________________________________________________________
    Larry Williams VIX Fix
    ---------------------------------------------------------------------------
    PURPOSE:
        Adds Larry Williams's "fix" for the volatility index (VIX). The method
        is referred to as the Williams VIX Fix, or WVF.
    INPUTS:
        ohlc_dataframe: A pandas DataFrame containing ohlc sequence data.
        source: Either 'open', 'high', 'low', 'close', or 'ohlc4'.
        length: The period length used in the VIX calculation.
    OUTPUT:
        Returns the WVF values in a new pandas DataFrame.
    NOTES:
        (Highest(Close,22) – Low) / (Highest(Close,22)) * 100
    SOURCE:
        https://www.tradingview.com/script/
            og7JPrRA-CM-Williams-Vix-Fix-Finds-Market-Bottoms/
        https://www.ireallytrade.com/newsletters/VIXFix.pdf
        http://www.technicalanalyst.co.uk/2015/03/the-vix-fix/
'''


def wvf(ohlc_dataframe, length=22):
    sequence = ohlc_dataframe.sort_values(by='period')

    sequence['wvf'] = 0
    for i in range(length, len(sequence)):
        sequence.ix[i, 'wvf'] = (
            100
            * (sequence.loc[i-length:i, 'close'].max() - sequence.ix[i, 'low'])
            / (sequence.loc[i-length:i, 'close'].max()))

    return sequence['wvf']


''' ___________________________________________________________________________
    JADS Power Oscillator
    ---------------------------------------------------------------------------
    PURPOSE:
        Indicator aggregator combining the effects of RSI, TSI, and ADX.
    INPUTS:
        ohlc_dataframe: A pandas DataFrame containing ohlc sequence data.
    OUTPUT:
        Returns the WVF values in a new pandas DataFrame.
    NOTES:
        ((tsi + JADS_rsi) * adx(dilen, adxlen)) * volume
    SOURCE:
        Developed by Jacob Ashton (JA) and Drew Sowersby (DS).
'''


def jads(ohlc_dataframe):
    sequence = ohlc_dataframe.sort_values(by='period')

    sequence['tsi'] = tsi(sequence, 'close', 14, 25)
    sequence['rsi'] = rsi(sequence, 'close', 14)
    sequence['adx'] = adx(sequence, 14, 14)['adx']

    # jads = (sequence['tsi'] + (sequence['rsi'] - 50)) * sequence['adx']
    sequence['jads'] = 0

    return sequence['jads']


pandas.options.mode.chained_assignment = None
