from __future__ import division

import time
import datetime
import operator

from jads.models import Exchange, Currency, Candle

'''
SEQEUNCER.PY
------------------------------
Created by Jacob Ashton
Red Goat Pioneers (c) October 2017

'''

def prettytime(unixtimestamp):
    return datetime.datetime.fromtimestamp(int(unixtimestamp)).strftime('%Y-%m-%d %H:%M:%S')

def convert_sequence_strategy(sequence):
    return {"sequence": sequence, "strategies": []}

def ohlc_value(period): 
    return float((float(period["open"]) + \
    			  float(period["high"]) + \
    			  float(period["low"]) +  \
    			  float(period["close"])) \
    			 /4)

def buyandhold_strategy(strategy_sequence, buy_threshold,
                        backtest, deposit, investment, trade_percentage):
    strategy = "Buy and Hold"
    buy_threshold = int(buy_threshold)

    trade_percentage = float(trade_percentage)/100
    holdings = float(deposit)
    balance = float(investment)

    # TODO: Use these to prevent overflow errors and/or obnoxious amounts of profit
    max_holdings = 10000000
    max_balance = 10000000
    
    print("Running buy-and-hold strategy for %s periods" % len(strategy_sequence["sequence"]))
    print("Backtesting enabled: %s" % backtest)

    if len(strategy_sequence["sequence"]) == 0:
        print("No periods to analyze!")
        return None

    strategy_sequence["sequence"] = \
        sorted(strategy_sequence["sequence"], key=operator.itemgetter("period"), reverse=False)

    strategy_sequence["strategies"].append({"strategy": strategy,
                                            "buy_threshold": buy_threshold})

    for i,period in enumerate(strategy_sequence["sequence"]):
        if "strategies" not in period:
            period["strategies"] = []

        if period["jads"] <= buy_threshold:
            action = "Buy"
            confidence = abs(period["jads"])
        else:
            action = "Hold"
            confidence = 100

        if backtest == "true":
            base_value = (float(deposit) * float(ohlc_value(period))) + float(investment)
            balance = float(balance)
            holdings = float(holdings)

            if action == "Buy":
                #print("Buying! Current Balance: %s | Current Holdings: %s" % (balance, holdings))
                if balance > 0:
                    purchase = balance * trade_percentage
                    #print("Buying %s with a balance of %s." % (purchase, balance))
                    balance = balance - purchase
                    holdings = holdings + (purchase / ohlc_value(period))
                else:
                    pass
            else:
                #print("Hold! Current Balance: %s | Current Holdings: %s" % (balance, holdings))
                pass

            current_value = (holdings * ohlc_value(period)) + balance
            balance = "%.8f" % float(balance)
            holdings = "%.8f" % float(holdings)
            gain = "%.2f" % ((100 * current_value / base_value) - 100)
        else:
        	gain = "N/A"

        period["strategies"].append({"strategy": strategy,
                                     "action": action,
                                     "confidence": "%.2f" % confidence,
                                     "balance": balance,
                                     "holdings": holdings,
                                     "gain": gain})

    

    strategy_sequence["sequence"] = \
        sorted(strategy_sequence["sequence"], key=operator.itemgetter("period"), reverse=True)

    return strategy_sequence

def threshold_strategy(strategy_sequence, lower_threshold, upper_threshold,
                       backtest, deposit, investment, trade_percentage):
    strategy = "Threshold"
    lower_threshold = int(lower_threshold)
    upper_threshold = int(upper_threshold)

    trade_percentage = float(trade_percentage)/100
    holdings = float(deposit)
    balance = float(investment)

    # TODO: Use these to prevent overflow errors and/or obnoxious amounts of profit
    max_holdings = 10000000
    max_balance = 10000000
    
    print("Running threshold strategy for %s periods" % len(strategy_sequence["sequence"]))
    print("Backtesting enabled: %s" % backtest)

    if len(strategy_sequence["sequence"]) == 0:
        print("No periods to analyze!")
        return None

    strategy_sequence["sequence"] = \
        sorted(strategy_sequence["sequence"], key=operator.itemgetter("period"), reverse=False)

    strategy_sequence["strategies"].append({"strategy": strategy,
                                            "lower_threshold": lower_threshold,
                                            "upper_threshold": upper_threshold})

    for i,period in enumerate(strategy_sequence["sequence"]):
        if "strategies" not in period:
            period["strategies"] = []

        if period["jads"] >= upper_threshold:
            action = "Sell"
            confidence = period["jads"]
        elif period["jads"] <= lower_threshold:
            action = "Buy"
            confidence = abs(period["jads"])
        else:
            action = "Hold"
            if period["jads"] >= 0:
                confidence = 100 * (upper_threshold - period["jads"]) / upper_threshold
            else:
                confidence = 100 * (lower_threshold - period["jads"]) / lower_threshold

        if backtest == "true":
            base_value = (float(deposit) * float(ohlc_value(period))) + float(investment)
            balance = float(balance)
            holdings = float(holdings)

            if action == "Buy":
                #print("Buying! Current Balance: %s | Current Holdings: %s" % (balance, holdings))
                if balance > 0:
                    purchase = balance * trade_percentage
                    #print("Buying %s with a balance of %s." % (purchase, balance))
                    balance = balance - purchase
                    holdings = holdings + (purchase / ohlc_value(period))
                else:
                    pass
            elif action == "Sell":
                #print("Selling! Current Balance: %s | Current Holdings: %s" % (balance, holdings))
                if holdings > 0:
                    sale = holdings * trade_percentage
                    #print("Selling %s with %s holdings." % (sale, holdings))
                    holdings = holdings - sale
                    balance = balance + (sale * ohlc_value(period))
                else:
                    pass
            else:
                #print("Hold! Current Balance: %s | Current Holdings: %s" % (balance, holdings))
                pass

            current_value = (holdings * ohlc_value(period)) + balance
            balance = "%.8f" % float(balance)
            holdings = "%.8f" % float(holdings)
            gain = "%.2f" % ((100 * current_value / base_value) - 100)
        else:
        	gain = "N/A"

        period["strategies"].append({"strategy": strategy,
                                     "action": action,
                                     "confidence": "%.2f" % confidence,
                                     "balance": balance,
                                     "holdings": holdings,
                                     "gain": gain})

    

    strategy_sequence["sequence"] = \
        sorted(strategy_sequence["sequence"], key=operator.itemgetter("period"), reverse=True)

    return strategy_sequence