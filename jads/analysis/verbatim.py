import sys

import time
import datetime
from operator import itemgetter

from django.core.exceptions import ObjectDoesNotExist

from jads.common.utilities import prettytime, unixtime
from jads.api.adx import getadx
from jads.api.tsi import gettsi

from jads.models import Exchange, Currency, VerbatimIndex

'''
VERBATIM.PY
------------------------------
Created by Jacob Ashton
Red Goat Pioneers (c) March 2018

'''

def validate_time(original_time, interval=300):
    if not isinstance(original_time, int):
        try:
            original_time = unixtime(original_time)
        except:
            print("ERROR >>> Time value provided is invalid: {0}"
                .format(original_time))
            return None

    if original_time < 1262304000 or original_time > 2145916800:
        print("ERROR >>> That time is out of range! Please " + 
              "try a time greater than 1262304000 and less than 2145916800.")
        return None

    clean_time = original_time + (interval - (original_time % interval))

    return clean_time

def create_index(source, base, quote, properties, rewrite=False):
    base = Currency.objects.get(name=base)
    quote = Currency.objects.get(name=quote)

    try:
        vindex = VerbatimIndex.objects.get(
            source = Exchange.objects.get(name=source),
            symbol1 = base,
            symbol2 = quote,
            startdate = properties["period"],
            )

        if rewrite == True:
            VerbatimIndex.objects.filter(
                source = Exchange.objects.get(name=source),
                symbol1 = base,
                symbol2 = quote,
                startdate = properties["period"],
            ).update(
                strength = properties["strength"],
                trend = properties["trend"],
                momentum = properties["momentum"],
                volatility = properties["volatility"],
                volume = properties["volume"],
                verbatim = properties["verbatim"],
            )

            print(("INFO >>> Verbatim index @{0} updated.")
                .format(properties['period']))

        else:
            print(("INFO >>> Verbatim index @{0} skipped.")
                .format(properties['period']))

    except ObjectDoesNotExist:
        vindex = VerbatimIndex(
            source = Exchange.objects.get(name=source),
            symbol1 = base,
            symbol2 = quote,
            startdate = properties["period"],
            strength = properties["strength"],
            trend = properties["trend"],
            momentum = properties["momentum"],
            volatility = properties["volatility"],
            volume = properties["volume"],
            verbatim = properties["verbatim"],
            )

        print(("INFO >>> Verbatim index @{0} created (VBI: {1}).")
            .format(properties['period'], properties['verbatim']))

        return vindex
    
    except Exception as e:
        print("ERROR >>> Unexpected error: {0}".format(e))
        raise e

def get_index(source, base, quote, starttime, interval=300):
    
    # Initialize and validate parameters 
    # --------------------------------------------------------------------------
    starttime = validate_time(starttime, interval)

    if starttime is None:
        print("ERROR >>> Unable to convert time(s) to UNIX timestamps.")
        return

    # Use the indicator APIs to calculate the Verbatim score for this period 
    # --------------------------------------------------------------------------
    print("INFO >>> Calculating Verbatim index for this period...")

    period = {'period': starttime}

    period_tsi = gettsi(source, base, quote, starttime)
    period_adx = getadx(source, base, quote, starttime)

    if 'error' in period_tsi or 'error' in period_adx:
        print("ERROR >>> Unable to process Verbatim for this period.")
        return None

    period['strength'] = period_tsi['jads_tsiscore']
    period['trend'] = period_adx['jads_adxscore']
    period['momentum'] = 0
    period['volatility'] = 0
    period['volume'] = 0
    period['verbatim'] = period['strength'] * period['trend']

    print("DEBUG >>> {1}".format(period['period'], period))
    
    return period

def store_verbatim(source, base, quote, starttime, endtime, interval):
    
    def save_new_indices(indices_batch):
        print("INFO >>> Saving {0} new indices to the database..."
            .format(len(indices_batch)))
        try:
            VerbatimIndex.objects.bulk_create(new_indices, batch_size=50)
        except Exception as e:
            print("ERROR >>> Error creating indices: " + str(e))

    # Clean start and end datetimes 
    # --------------------------------------------------------------------------
    starttime = validate_time(starttime, interval)
    endtime = validate_time(endtime, interval)

    if starttime is None or endtime is None:
        print("ERROR >>> Unable to convert time(s) to UNIX timestamps.")
        return

    if starttime == endtime:
        print("INFO >>> End time is equal to start time, " + 
            "so no candles will be made.")
        return

    # Break the given timeframe up into periods based on the interval provided 
    # --------------------------------------------------------------------------
    periods = []
    current_time = validate_time(starttime, interval)
    while current_time < endtime:
        periods.append(current_time)
        current_time += interval
    periods.sort(reverse=True)

    # Create the VerbatimIndex objects and save/update in the database 
    # --------------------------------------------------------------------------
    save_batch_size = 1
    new_indices = []
    sequence = []
    for period in periods:
        if len(new_indices) >= save_batch_size:
            save_new_indices(new_indices)
            new_indices = []
        period = get_index(source, base, quote, period, interval)
        if period is not None:
            seqence.append(period)
            new_index = create_index(source, base, quote, period)
            if new_index is not None:
                new_indices.append(new_index)
        

    return sequence

def retrieve_verbatim(source, base, quote, starttime, endtime):
    indices = VerbatimIndex.objects.filter(
        source = Exchange.objects.get(name=source),
        symbol1 = Currency.objects.get(name=base),
        symbol2 = Currency.objects.get(name=quote),
        startdate__gte = validate_time(starttime),
        startdate__lte = validate_time(endtime),
        )

    sequence = [{
        'period': vindex.startdate,
        'strength': vindex.strength,
        'trend': vindex.trend,
        'momentum': vindex.momentum,
        'volatility': vindex.volatility,
        'volume': vindex.volume,
        'verbatim': vindex.verbatim,
    } for vindex in indices]

    return sorted(sequence, key=itemgetter('period'))

def delete_verbatim(source, base, quote, starttime, endtime):
    indices = VerbatimIndex.objects.filter(
        source = Exchange.objects.get(name=source),
        symbol1 = Currency.objects.get(name=base),
        symbol2 = Currency.objects.get(name=quote),
        startdate__gte = validate_time(starttime),
        startdate__lte = validate_time(endtime),
        )

    for vindex in indices:
        try:
            print(("INFO >>> Deleting Verbatim index for {0} {1}/{2} candles" + 
                "at {3}.").format(
                    source, vindex.symbol1.symbol, vindex.symbol2.symbol,
                    starttime, endtime
                ))
            vindex.delete()
        except Exception as e:
            print("ERROR >>> Unable to delete Verbatim index at {0}: {1}"
                .format(vindex.startdate, e))

    return