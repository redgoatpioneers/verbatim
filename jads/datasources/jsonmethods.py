import sys
import json

from urllib.request import urlopen
import urllib.error

def getjson(html):
    try:
        with urlopen(html) as url:
            headers = url.info()
            data = url.read().decode(headers.get_content_charset(failobj="utf-8"))
        content = json.loads(data)
        result = {'headers': headers.items(), 'body': content}
        return result
    except urllib.error.HTTPError as e:
        return {'error': 'HTTPError', 'body': 'HTTP Error: Please check the URL and try again. ' + \
                                              'Error Message: ' + str(e)}