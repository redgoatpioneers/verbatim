import csv
from django.db.models import Min, Max

from jads.models import Trade, Currency, Exchange

#Assumes you provide a CSV file with three comma-separated columns: Trade date (UNIX time), price, amount

def importhistory(filepath, datasource, basecurrency, quotecurrency):
	results = {}
	try:
		print("~~~ BEGINNING FILE IMPORT ~~~")
		with open(filepath, 'r') as csvfile:
			currency1 = Currency.objects.get(name=basecurrency)
			currency2 = Currency.objects.get(name=quotecurrency)
			exchange = Exchange.objects.get(name=datasource)
			tradeStream = csv.DictReader(csvfile, fieldnames=("date", "price", "amount"), delimiter = ',')

			trades = 0
			tradesSkipped = 0
			maxExistingTradeDate = Trade.objects.aggregate(Max('tradedate'))["tradedate__max"]
			minExistingTradeDate = Trade.objects.aggregate(Min('tradedate'))["tradedate__min"]

			newTrades = []

			for trade in tradeStream:
				if (minExistingTradeDate is not None and \
				   (int(trade["date"]) < minExistingTradeDate or int(trade["date"]) > maxExistingTradeDate)) \
				   or minExistingTradeDate is None:
					newTrades.append(Trade(symbol1=currency1, symbol2=currency2, price=trade["price"], \
										 amount=trade["amount"], tradedate=trade["date"], source=exchange))
					print("(Trade %s) New %s trade created: %s %s traded for %s at %s @ %s" % \
						  (trades, datasource, trade["amount"], \
						  	basecurrency, quotecurrency, trade["price"], trade["date"]))
					trades += 1
				else:
					trades += 1
					tradesSkipped += 1
				if trades % 100000 == 0:
					print("~~~ %s trades reviewed so far, %s added, %s skipped ~~~" % \
						  (trades, trades-tradesSkipped, tradesSkipped))
					Trade.objects.bulk_create(newTrades)
					newTrades = []
		Trade.objects.bulk_create(newTrades)
		print("~~~ FILE IMPORT COMPLETE ~~~")
		print("--- %s rows imported, %s rows skipped" % ((trades-tradesSkipped), tradesSkipped))
		results["trades"] = trades
		results["results"] = "success"
	except Exception as e:
		print("Error reading file: %s" % e)
		results["trades"] = 0
		results["results"] = str(e)
	return results

'''
class Trade(models.Model):
	tradeid = models.IntegerField(primary_key=True)
	symbol1 = models.ForeignKey(Currency, related_name='symbol1_set', on_delete=models.PROTECT)
	symbol2 = models.ForeignKey(Currency, related_name='symbol2_set', on_delete=models.PROTECT)
	price = models.DecimalField(max_digits=20, decimal_places=10)
	amount = models.DecimalField(max_digits=20, decimal_places=10)
	tradedate = models.IntegerField()
	source = models.ForeignKey(Exchange, on_delete=models.PROTECT)

'''