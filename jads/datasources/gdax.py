import datetime

from jads.datasources.jsonmethods import getjson
from jads.models import Exchange

'''
GDAX.PY
------------------------------
Created by Jacob Ashton
Red Goat Pioneers (c) October 2017

'''

if Exchange.objects.filter(name='GDAX').count() > 0:
    apisource = Exchange.objects.get(name='GDAX').api_url
else:
    apisource = 'https://api.gdax.com/'


def iso8601time(unixtimestamp):
    return datetime.datetime.fromtimestamp(int(unixtimestamp)).strftime('%Y-%m-%dT%H:%M:%S')


def getgdaxcurrencies():
    currencies = getjson(apisource + 'currencies')['body']
    return currencies


def getgdaxpairs():
    products = getjson(apisource + 'products')['body']
    pairs = []

    for product in products:
        pairs.append(product['id'])

    pairs.sort()
    return pairs

# [
# 	{
# 		"id": "BTC-USD",
# 		"base_currency": "BTC",
# 		"quote_currency": "USD",
# 		"base_min_size": "0.01",
# 		"base_max_size": "10000.00",
# 		"quote_increment": "0.01"
# 	}
# ]


def getgdaxticker(pair):
    ticker = getjson(apisource + 'products/%s/ticker' % pair)['body']
    ticker['pair'] = pair
    return ticker

# {
#   "trade_id": 4729088,
#   "price": "333.99",
#   "size": "0.193",
#   "bid": "333.98",
#   "ask": "333.99",
#   "volume": "5957.11914015",
#   "time": "2015-11-14T20:46:03.511254Z"
# }


def getgdaxcurrent(pair):
    current = getjson(apisource + 'hist/' + pair.upper())['body']
    return current

# [
# 	[ time, low, high, open, close, volume ],
# 	[ 1415398768, 0.32, 4.2, 0.35, 4.2, 12.3 ],
# 	...
# ]


def getgdaxhistorical(pair, start, end, seconds):
    sendstring = apisource + 'products/%s/candles?' % pair.upper() + \
                'start=' + str(iso8601time(start)) + \
                '&end=' + str(iso8601time(end)) + \
                '&granularity=' + str(seconds)
    print("Sending %s to GDAX..." % sendstring)
    historical = getjson(sendstring)['body']
    return historical

# [
# 	[ time, low, high, open, close, volume ],
# 	[ 1415398768, 0.32, 4.2, 0.35, 4.2, 12.3 ],
# 	...
# ]
