import sys

import time
import datetime
import math

from django.db.models import Min, Max
from django.core.exceptions import ObjectDoesNotExist

from jads.common.utilities import (
    prettytime, unixtime, get_period_name, fix_starttime)

from jads.datasources.gdax import getgdaxhistorical
from jads.datasources.binance import getbinancehistorical
from jads.datasources.sequencer2 import sequence_ohlc_dict

from jads.models import Exchange, Currency, Trade, Candle

'''
SEQEUNCER.PY
------------------------------
Created by Jacob Ashton
Red Goat Pioneers (c) October 2017

'''

ACTIVE_INTERVALS = ['900',     # 15m
                    '1800',    # 30m
                    '3600',    # 1hr
                    '7200',    # 2hr
                    '14400',   # 4hr
                    '28800',   # 8hr
                    '86400',   # 1dy
                    '604800']  # 1wk


def minute(unixtimestamp):
    return int(prettytime(unixtimestamp)[14:16])


def hour(unixtimestamp):
    return int(prettytime(unixtimestamp)[11:13])


def day(unixtimestamp):
    return datetime.datetime.fromtimestamp(unixtimestamp).strftime("%A")


def sequencer(symbol1, symbol2, source, interval, rewrite, fromsource,
              starttime="2000-01-01 12:00 AM", endtime="2020-12-31 12:59 PM"):
    
    # Initialize and validate parameters 
    # --------------------------------------------------------------------------

    try:
        starttime = unixtime(starttime)
        endtime = unixtime(endtime)
        interval = int(interval)
        rewrite = True if rewrite == "true" else False
        fromsource = True if fromsource == "true" else False
    except:
        print("ERROR >>> Bad input provided... exiting prematurely.")
        return

    if starttime < 1262304000 or endtime > 2145916800:
        print("ERROR >>> That time is out of range! Please " + 
              "try a time greater than 1262304000 and less than 2145916800.")
        return

    if starttime == endtime:
        print("INFO >>> End time is equal to start time, so no candles will be made.")
        return

    if not isinstance(rewrite, bool):
        print("ERROR >>> Rewrite must be a boolean value!")
        return

    if not isinstance(fromsource, bool):
        print("ERROR >>> Pull from source must be a boolean value!")
        return

    # Some APIs get confused when you send future dates, so make sure
    # the time is present or past
    now = datetime.datetime.now()
    now = time.mktime(now.timetuple())
    if endtime > now:
        endtime = now

    # Make sure the time period starts and ends at multiples of the period
    # interval for consistent candles
    starttime = fix_starttime(int(starttime), int(interval))
    endtime = fix_starttime(int(endtime), int(interval))

    # Retrieve data from database or exchange 
    # --------------------------------------------------------------------------

    if not fromsource:
        print("INFO >>> Retrieving trade data from database...")
        trades = Trade.objects.filter(
            source=Exchange.objects.get(name=source),
            symbol1=Currency.objects.get(name=symbol1),
            symbol2=Currency.objects.get(name=symbol2),
            tradedate__gte=starttime,
            tradedate__lt=endtime)

        trades = [{"time": t.tradedate, "price": float(t.price)} for t in trades]
    elif fromsource:
        if source == "GDAX":
            print("INFO >>> Retrieving trade data from GDAX...")
            creategdaxcandles(symbol1, symbol2,
                              starttime, endtime, interval, rewrite)
            return
        elif source == "Binance":
            print("INFO >>> Retrieving trade data from Binance...")
            createbinancecandles(symbol1, symbol2,
                                 starttime, endtime, interval, rewrite)
            return
        else:
            print("INFO >>> Historic data retrieval is not available for {0}."
                  .format(source))
            return
    else:
        print("ERROR >>> Must indicate whether to pull from data source!")
        return

    if len(trades) == 0:
        print("INFO >>> There are no trades for the given time period.")
    else:
        print("INFO >>> {0} trades serialized successfully!"
              .format(len(trades)))

    # Build out the periods 
    # -------------------------------------------------------------------------

    periods = []
    currenttime = starttime
    while currenttime <= endtime:
        periods.append(currenttime)
        currenttime += int(interval)
    
    print(("INFO >>> Creating trade sequence for {0} periods " +
           "based on requested interval...").format(len(periods)))
    
    sequence = [{"period": period, "interval": interval} for period in periods]
    for i, period in enumerate(sequence):
        # Show a message every 1/10 of the periods to keep track of progress
        # for big data sets
        if i == 0 or (i+1) % (math.floor(len(periods)/10)
                              if math.floor(len(periods)/10) > 0
                              else 1) == 0:
            print(">>> Building periods {0} - {1}..."
                  .format((str(i+1), str(i+math.floor(len(periods)/10)))))
        # Get the trades that are between the current period start time
        # and next period start time
        period["trades"] = [t for t in trades
                            if period["period"]
                            <= t["time"]
                            < (period["period"]+interval)]
        # Clear the old trades out so it goes faster each round
        trades = trades[len(period["trades"]):]

    print("INFO >>> Successfully created trade sequence!")
    print("INFO >>> Building OHLC candles...")

    # Create/update the candles in the database 
    # -------------------------------------------------------------------------

    candles_touched = 0

    for period in sequence:
        candles = []
        if len(period["trades"]) == 0:
            continue

        times = [t["time"] for t in period["trades"]]
        prices = [t["price"] for t in period["trades"]]
        open_time = min(times)
        close_time = max(times)

        open_price = next((t["price"]
                           for t in period["trades"]
                           if t["time"] == open_time))
        high_price = max(prices)
        low_price = min(prices)
        close_price = next((t["price"] for t in period["trades"]
                            if t["time"] == close_time))

        ohlc_period = {"period": period["period"],
                       "interval": period["interval"],
                       "open": open_price,
                       "high": high_price,
                       "low": low_price,
                       "close": close_price,
                       "volume": 0}
        
        new_candle = createcandle(ohlc_period, source,
                                  symbol1, symbol2, rewrite)
        if new_candle:
            candles.append()
        candles_touched += 1

        try:
            Candle.objects.bulk_create(candles)
        except Exception as e:
            print("ERROR >>> Error creating candles: %s" % str(e))
            return
    print("INFO >>> Success! Created or updated %s candles." % candles_touched)


def creategdaxcandles(symbol1, symbol2, starttime, endtime, interval, rewrite):
    # NOTE: Both starttime and endtime should be unix timestamps

    print("INFO >>> Creating GDAX candles...")
    basecurrency = Currency.objects.get(name=symbol1).symbol
    quotecurrency = Currency.objects.get(name=symbol2).symbol

    pair = '%s-%s' % (basecurrency.upper(), quotecurrency.upper())
    
    currenttime = starttime
    maxinterval = (interval * 100)
    maxtime = starttime + maxinterval
    
    numerrors = 0
    errors = []

    while currenttime < (endtime + interval):
        candles = []
        maxtime = endtime if (maxtime > endtime) else maxtime
        
        if currenttime == maxtime:
            break

        gdaxresponse = getgdaxhistorical(pair, currenttime, maxtime, interval)

        if len(gdaxresponse) == 0:
            print("INFO >>> No data for time period %s to %s" %
                  (prettytime(currenttime), prettytime(maxtime)))
            errors.append("INFO >>> No data for time period %s to %s" %
                          (prettytime(currenttime), prettytime(maxtime)))
            numerrors += 1
            maxtime += maxinterval/10
            continue
        
        while numerrors < 10 and "message" in gdaxresponse:
            print(("INFO >>> Unrecognized response for period %s to %s. " + 
                   "Response: {0}")
                  .format(
                    prettytime(currenttime),
                    prettytime(maxtime),
                    gdaxresponse))
            errors.append(("INFO >>> Unrecognized response for period " + 
                           "{0} to {1}. Response: {2}")
                          .format(
                              prettytime(currenttime),
                              prettytime(maxtime),
                              gdaxresponse))
            numerrors += 1
            continue

        if numerrors > 50:
            print("INFO >>> Too many errors!")
            for i, error in enumerate(errors):
                print("Error #%s: %s" % (i+1, error))
            break

        try:
            for candle in gdaxresponse:
                if candle[0] < starttime:
                    continue
                ohlc_period = {"period": candle[0],
                               "interval": interval,
                               "low": candle[1],
                               "high": candle[2],
                               "open": candle[3],
                               "close": candle[4],
                               "volume": candle[5]}
                new_candle = createcandle(ohlc_period, "GDAX",
                                          symbol1, symbol2, rewrite)
                if new_candle:
                    candles.append(new_candle)
        except Exception as e:
            print("INFO >>> Unrecognized response for period {0} to {1}: {2}"
                  .format(currenttime, maxtime, str(e)))
            errors.append(("INFO >>> Unrecognized response for period " + 
                           "{0} to {1}: {2}")
                          .format(currenttime, maxtime, str(e)))

        if len(candles) > 0:
            print("INFO >>> Saving new candles to the database...")
            try:
                Candle.objects.bulk_create(candles)
            except Exception as e:
                print("ERROR >>> Error creating candles: " + str(e))

        maxtime += maxinterval
        currenttime += maxinterval
                
    return


def createbinancecandles(symbol1, symbol2,
                         starttime, endtime,
                         interval, rewrite):
    # NOTE: Both starttime and endtime should be unix timestamps

    basecurrency = Currency.objects.get(name=symbol1).symbol
    quotecurrency = Currency.objects.get(name=symbol2).symbol

    pair = '%s%s' % (basecurrency.upper(), quotecurrency.upper())

    currenttime = starttime
    maxinterval = (interval * 100)
    maxtime = starttime + maxinterval
    
    numerrors = 0
    errors = []
    previousmaxtime = starttime

    while currenttime < (endtime + interval):
        candles = []
        maxtime = endtime if (maxtime > endtime) else maxtime
        
        if currenttime == maxtime or previousmaxtime == maxtime:
            print("INFO >>> This time period cannot be processed!")
            break
        
        previousmaxtime = maxtime
        binanceresponse = getbinancehistorical(pair, currenttime,
                                               maxtime, interval)

        if len(binanceresponse) == 0:
            print("INFO >>> No data for time period %s to %s" %
                  (prettytime(currenttime), prettytime(maxtime)))
            errors.append("No data for time period %s to %s" %
                          (prettytime(currenttime), prettytime(maxtime)))
            numerrors += 1
            maxtime += int(math.floor(maxinterval/10))
            continue
        
        while numerrors < 10 and "message" in binanceresponse:
            print(("INFO >>> Unrecognized response for {0} - {1}. " +
                   "Response: {2}").format(
                    prettytime(currenttime),
                    prettytime(maxtime),
                    binanceresponse))
            errors.append(("INFO >>> Unrecognized response for {0} - {1}. " +
                           "Response: {2}").format(
                    prettytime(currenttime),
                    prettytime(maxtime),
                    binanceresponse))
            numerrors += 1
            continue

        if numerrors > 50:
            print("INFO >>> Too many errors!")
            for i, error in enumerate(errors):
                print("Error #%s: %s" % (i+1, error))
            break

        try:
            for candle in binanceresponse:
                ohlc_period = {
                    "period": int(candle["opentime"]),
                    "interval": interval,
                    "open": float(candle["open"]),
                    "high": float(candle["high"]),
                    "low": float(candle["low"]),
                    "close": float(candle["close"]),
                    "volume": float(candle["volume"])}
                new_candle = createcandle(ohlc_period, "Binance",
                                          symbol1, symbol2, rewrite)
                if new_candle:
                    candles.append(new_candle)
        except Exception as e:
            print("INFO >>> Unrecognized response for period {0} to {1}: {2}"
                  .format(currenttime, maxtime, str(e)))
            errors.append(("INFO >>> Unrecognized response for period " +
                           "{0} to {1}: {2}").format(currenttime, maxtime, str(e)))
            numerrors += 1
            previousmaxtime = maxtime

        if len(candles) > 0:
            print("INFO >>> Saving new candles to the database...")
            try:
                Candle.objects.bulk_create(candles)
            except Exception as e:
                print("ERROR >>> Error creating candles: " + str(e))

        maxtime += maxinterval
        currenttime += maxinterval


def createcandle(ohlc_period, source, symbol1, symbol2, rewrite):
    base = Currency.objects.get(name=symbol1)
    quote = Currency.objects.get(name=symbol2)

    try:
        Candle.objects.get(
            symbol1=base,
            symbol2=quote,
            startdate=ohlc_period["period"],
            interval=ohlc_period["interval"],
            source=Exchange.objects.get(name=source)
            )

        if rewrite:
            Candle.objects.filter(
                symbol1=base,
                symbol2=quote,
                startdate=ohlc_period["period"],
                interval=ohlc_period["interval"],
                source=Exchange.objects.get(name=source)
                ).update(
                open=ohlc_period["open"],
                high=ohlc_period["high"],
                low=ohlc_period["low"],
                close=ohlc_period["close"],
                volume=ohlc_period["volume"])

            print(("INFO >>> {0}s candle updated ({1} {2}-{3}) | P: {4} " +
                   "| O:{5:>#.6f} H:{6:>#.6f} L:{7:>#.6f} C:{8:>#.6f} ").format(
                    ohlc_period["interval"], source, base.symbol, quote.symbol,
                    prettytime(ohlc_period["period"]),
                    ohlc_period["open"], ohlc_period["high"],
                    ohlc_period["low"], ohlc_period["close"]))
        else:
            print(("INFO >>> {0}s candle skipped ({1} {2}-{3}) | P: {4} " +
                   "| O:{5:>#.6f} H:{6:>#.6f} L:{7:>#.6f} C:{8:>#.6f} ").format(
                    ohlc_period["interval"], source, base.symbol, quote.symbol,
                    prettytime(ohlc_period["period"]),
                    ohlc_period["open"], ohlc_period["high"],
                    ohlc_period["low"], ohlc_period["close"]))

    except ObjectDoesNotExist:
        candle = Candle(
            source=Exchange.objects.get(name=source),
            symbol1=base,
            symbol2=quote,
            startdate=ohlc_period["period"],
            interval=ohlc_period["interval"],
            open=ohlc_period["open"],
            high=ohlc_period["high"],
            low=ohlc_period["low"],
            close=ohlc_period["close"],
            volume=ohlc_period["volume"],
            sma20=0,
            sma50=0,
            sma100=0,
            tsi=0,
            tsi_signal=0,
            rsi=0,
            adx=0,
            wvf=0,
            )

        print(("INFO >>> {0}s candle created ({1} {2}-{3}) | P: {4} " +
               "| O:{5:>#.6f} H:{6:>#.6f} L:{7:>#.6f} C:{8:>#.6f} ").format(
                    ohlc_period["interval"], source, base.symbol, quote.symbol,
                    prettytime(ohlc_period["period"]),
                    ohlc_period["open"], ohlc_period["high"],
                    ohlc_period["low"], ohlc_period["close"]))

        return candle
    
    except:
        print("ERROR >>> Unexpected error: ", sys.exc_info()[0])
        raise


def deletecandles(source, symbol1, symbol2, starttime, endtime, interval):
    try:
        starttime = unixtime(starttime)
        endtime = unixtime(endtime)
    except:
        print("ERROR >>> Bad input provided... exiting prematurely.")
        return

    candles = Candle.objects.filter(
        symbol1=Currency.objects.get(name=symbol1),
        symbol2=Currency.objects.get(name=symbol2),
        startdate__gte=starttime,
        startdate__lte=endtime,
        interval=interval,
        source=Exchange.objects.get(name=source)
        )
    if len(candles) == 0:
        print("INFO >>> No candles to delete during this time period.")
        return
    else:
        print("INFO >>> {0} candles will be deleted during this time period."
              .format(len(candles)))
        candles.delete()


# This method is used to combine multiple candles into one
# e.g. 1hr to 2hr, or 1d to 1w
def combinecandles(source, symbol1, symbol2, starttime, endtime, update=False,
                   intervals=ACTIVE_INTERVALS):
    try:
        starttime = unixtime(starttime)
        endtime = unixtime(endtime)
    except:
        print("ERROR >>> Invalid start or end time.")
        return

    starttime = fix_starttime(int(starttime), 300)

    # Make sure you don't try to pull candles from the future!
    if endtime > time.time():
        endtime = math.floor(time.time())
    adjusted_endtime = fix_starttime(int(endtime), 300)
    if adjusted_endtime > endtime:
        endtime = adjusted_endtime - 300

    def get_interval_starttime(from_interval):
        if (endtime - starttime) < int(from_interval):
            print("INFO >>> Start time and end time must be separated " + 
                  "by at least {0}!".format(get_period_name(from_interval)))
            new_starttime = endtime - int(from_interval)
            print("INFO >>> Returning %s" % prettytime(new_starttime))
            return fix_starttime(int(new_starttime), int(from_interval))

        print("INFO >>> Getting latest {0} candle..."
              .format(get_period_name(interval)))
        latest_time = Candle.objects.filter(
            source=Exchange.objects.get(name=source),
            symbol1=Currency.objects.get(name=symbol1),
            symbol2=Currency.objects.get(name=symbol2),
            interval=interval,
            startdate__gte=starttime,
            startdate__lt=endtime,
            ).aggregate(Max('startdate'))['startdate__max']
        if latest_time is None:
            print("INFO >>> No {0}s candles after {1}!"
                  .format(get_period_name(interval), prettytime(starttime)))
            latest_time = starttime

        if latest_time > starttime:
            new_starttime = latest_time
        else:
            new_starttime = starttime
        
        print("INFO >>> Returning {0} as interval start time."
              .format(prettytime(fix_starttime(int(new_starttime), int(from_interval)))))
        return fix_starttime(int(new_starttime), int(from_interval))

    interval_data = {}

    for interval in intervals:
        interval_starttime = get_interval_starttime(interval)
        interval_data[interval] = {
            'starttime': interval_starttime,
            'prettytime': prettytime(interval_starttime)
        }

    # Start combining candles!
    # -------------------------------------------------------------------------
    
    def get_and_combine_candles(to_interval, from_interval='300'):
        print(("INFO >>> Creating {0} candles from {1} candles " + 
               "between {2} and {3}.").format(
            get_period_name(to_interval), get_period_name(from_interval),
            interval_data[to_interval]['prettytime'],
            prettytime(endtime)))

        try:
            createcombinedcandles(source, symbol1, symbol2, update,
                                  int(from_interval), int(to_interval),
                                  interval_data[to_interval]['starttime'],
                                  endtime, False)
        except AttributeError:
            print("INFO >>> No {0}s candles during this time period!"
                  .format(get_period_name(from_interval)))

    for interval in intervals:
        get_and_combine_candles(interval, '300')


def createcombinedcandles(source, symbol1, symbol2, update,
                          frominterval, tointerval, starttime, endtime,
                          debug=True):
    
    def save_new_candles(candles_batch):
        print("INFO >>> Saving {0} new {1}s candles to the database..."
              .format(len(candles_batch), tointerval))
        try:
            Candle.objects.bulk_create(candles_batch, batch_size=50)
        except Exception as e:
            print("ERROR >>> Error creating candles: {0}".format(e))

    periods = []
    currenttime = starttime
    while currenttime < endtime:
        periods.append({'start': currenttime, 'end': currenttime+tointerval})
        currenttime += tointerval

    new_candles = []
    for p in periods:
        candles = sequence_ohlc_dict(symbol1, symbol2, source, int(p['start']),
                                     int(p['end']), frominterval)
        if candles is None:
            continue
        candles = sorted(candles, key=lambda candle: candle['period'])

        try:
            p = p['start']
            o = candles[0]['open']
            h = max([candle['high'] for candle in candles])
            l = min([candle['low'] for candle in candles])
            c = candles[-1]['close']
            v = sum([candle['volume'] for candle in candles])
        except IndexError as e:
            print("ERROR >>> Index error setting candle values: {0}".format(e))
            break
        except ValueError:
            print("ERROR >>> Value error setting candle values.")
            break
    
        ohlc_period = {
            "period": int(p),
            "interval": tointerval,
            "open": float(o),
            "high": float(h),
            "low": float(l),
            "close": float(c),
            "volume": float(v)}

        newcandle = createcandle(ohlc_period, source, symbol1, symbol2, update)
        if newcandle is not None:
            new_candles.append(newcandle)

        if len(new_candles) >= 50:
            print("INFO >>> Saving the current batch of new candles...")
            if not debug:
                save_new_candles(new_candles)
            print("INFO >>> Clearing the current batch of new candles...")
            new_candles = []

    if len(new_candles) > 0:
            print("INFO >>> Saving the final batch of new candles...")
            if not debug:
                save_new_candles(new_candles)
