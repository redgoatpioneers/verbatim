from __future__ import division

import time
import datetime
import random
import itertools
import operator
import math

from django.core import serializers

from jads.common.utilities import prettytime
from jads.models import Exchange, Currency, Candle

'''
SEQEUNCER.PY
------------------------------
Created by Jacob Ashton
Red Goat Pioneers (c) October 2017

'''

def mean(a):
    return sum(a) / len(a)

def sequence_ohlc(symbol1, symbol2, source, starttime, endtime, interval):
    '''
    This function produces a JSON with the following structure:

    [
        {
            period: 1487228700,
            interval: 300,
            open: 1026.3300000000,
            high: 1029.5000000000,
            low: 1018.6400000000,
            close: 1029.2100000000,
        },
        ...
    ]
    '''

    # Validate the inputs
    # --------------------------------------------------------------------------------------------------------

    try:
        starttime = int(time.mktime(datetime.datetime.strptime(starttime, "%Y-%m-%d %I:%M %p").timetuple()))
        endtime = int(time.mktime(datetime.datetime.strptime(endtime, "%Y-%m-%d %I:%M %p").timetuple()))
        interval = int(interval)
    except:
        errorstring = "ERROR: Bad input provided... exiting prematurely."
        print(errorstring)
        return {"error": errorstring}

    if starttime < 1262304000 or endtime > 2145916800:
        errorstring = "ERROR: That time is out of range! Please try a time greater than %s and less than %s." % \
                      (prettytime(1262304000), prettytime(2145916800))
        print(errorstring)
        return {"error": errorstring}

    if starttime == endtime:
        errorstring = "ERROR: The end time is the same as the start time."
        print(errorstring)
        return {"error": errorstring}

    if endtime - starttime < interval:
        errorstring = "ERROR: The end date must be separated by the start date by at least %.0f minutes." % \
                      (interval/60)
        print(errorstring)
        return {"error": errorstring}

    # Normalize the times
    # --------------------------------------------------------------------------------------------------------

    starttime = starttime - (starttime % interval)
    endtime = endtime - (endtime % interval) + interval
    periods = math.floor((endtime - starttime) / interval)

    # Retrieve candles from the database and return them as an OHLC sequence of periods (candles)
    # --------------------------------------------------------------------------------------------------------

    candles = Candle.objects.filter(symbol1 = Currency.objects.get(name=symbol1),
                                    symbol2 = Currency.objects.get(name=symbol2),
                                    startdate__gte = starttime,
                                    startdate__lte = endtime+interval,
                                    interval = interval,
                                    source = Exchange.objects.get(name=source))[:periods]

    ohlc_sequence = [{"period": int(candle.startdate),
                      "interval": int(candle.interval),
                      "open": float(candle.open),
                      "high": float(candle.high),
                      "low": float(candle.low),
                      "close": float(candle.close),
                      "volume": float(candle.volume)}
                     for candle in candles]

    return ohlc_sequence