import time
import datetime

from math import floor

from jads.datasources.jsonmethods import getjson
from jads.models import Exchange, Currency

'''
GDAX.PY
------------------------------
Created by Jacob Ashton
Red Goat Pioneers (c) October 2017

'''

if Exchange.objects.filter(name='Binance').count() > 0:
    apisource = Exchange.objects.get(name='Binance').api_url
else:
    apisource = 'https://api.binance.com/'

def seconds_to_interval(seconds):
    seconds = str(seconds)
    return {
        '300': '5m',
        '900': '15m',
        '3600': '1h',
        '72200': '2h',
        '14400': '4h',
        '86400': '1d',
        '604800': '1w'
    }.get(seconds, '1h')

def iso8601time(unixtimestamp):
    return datetime.datetime.fromtimestamp(int(unixtimestamp)).strftime('%Y-%m-%dT%H:%M:%S')

def get_stored_pairs():
    storedcurrencies = [c.symbol for c in Currency.objects.all()]
    stored_pairs = {}
    for currency in storedcurrencies:
        for pair in storedcurrencies:
            if currency != pair:
                stored_pairs[currency+pair] = [currency, pair]
    return stored_pairs
'''
{
  'BTCETH': ['BTC', 'ETH'],
  'BTCLTC': ['BTC', 'LTC'],
  'BTCXRP': ['BTC', 'XRP'],
  'ETHBTC': ['ETH', 'BTC'],
  'ETHLTC': ['ETH', 'LTC'],
  'ETHXRP': ['ETH', 'XRP'],
  'LTCBTC': ['LTC', 'BTC'],
  'LTCETH': ['LTC', 'ETH'],
  'LTCXRP': ['LTC', 'XRP'],
  'XRPBTC': ['XRP', 'BTC'],
  'XRPETH': ['XRP', 'ETH'],
  'XRPLTC': ['XRP', 'LTC']
}
'''

def getbinanceticker(pair):
    if 'USD' in pair and 'USDT' not in pair:
        pair = pair.replace('USD', 'USDT')

    ticker = getjson(apisource + 'api/v3/ticker/price?symbol=' + pair.upper())['body']
    ticker['pair'] = pair
    return ticker
'''
{
  "symbol":"ETHBTC",
  "price":"0.09098600"
}
'''

def getbinancepairs():
    alltickers = getjson(apisource + 'api/v3/ticker/price')['body']
    pairs = [t['symbol'] for t in alltickers]
    return pairs

def getbinancecurrencies(pairs):
    currencies = []
    storedpairs = get_stored_pairs()

    for p in pairs:
        if 'USD' in p and 'USDT' not in p:
            p = p.replace('USD', 'USDT')

        if p in storedpairs:
            base = storedpairs[p][0]
            quote = storedpairs[p][1]
            if base not in currencies:
                currencies.append(base)
            if quote not in currencies:
                currencies.append(quote)

    return currencies

def getbinancecurrent(pair):
    current = getjson(apisource + 'api/v3/ticker/bookTicker?symbol=' + pair.upper())['body']
    return current
'''
{
  "symbol":"ETHBTC",
  "bidPrice":"0.09098700",
  "bidQty":"0.20000000",
  "askPrice":"0.09108900",
  "askQty":"0.20100000"
}
'''

def getbinance24hr(pair):
    if 'USD' in pair and 'USDT' not in pair:
        print(">>>>> DEBUG >>>>> pair: %s" % pair)
        pair = pair.replace('USD', 'USDT')

    sendstring = apisource + 'api/v1/ticker/24hr?symbol=' + pair.upper()
    print("Sending %s to Binance..." % sendstring)
    last24hr = getjson(sendstring)['body']
    return last24hr
'''
{
  "symbol": "BNBBTC",
  "priceChange": "-94.99999800",
  "priceChangePercent": "-95.960",
  "weightedAvgPrice": "0.29628482",
  "prevClosePrice": "0.10002000",
  "lastPrice": "4.00000200",
  "lastQty": "200.00000000",
  "bidPrice": "4.00000000",
  "askPrice": "4.00000200",
  "openPrice": "99.00000000",
  "highPrice": "100.00000000",
  "lowPrice": "0.10000000",
  "volume": "8913.30000000",
  "quoteVolume": "15.30000000",
  "openTime": 1499783499040,
  "closeTime": 1499869899040,
  "fristId": 28385,   // First tradeId
  "lastId": 28460,    // Last tradeId
  "count": 76         // Trade count
}
'''

def getbinancehistorical(pair, start_unixtime, end_unixtime, seconds):
    if 'USD' in pair and 'USDT' not in pair:
        pair = pair.replace('USD', 'USDT')

    # Clean start and end times to avoid decimals
    starttime = int(floor(start_unixtime))
    endtime = int(floor(end_unixtime))

    interval = seconds_to_interval(seconds)
    sendstring = apisource + 'api/v1/klines?' + \
                'symbol=' + pair.upper() + \
                '&interval=' + str(interval) + \
                '&startTime=' + str(starttime) + '000' + \
                '&endTime=' + str(endtime) + '000'
    print("Sending %s to Binance..." % sendstring)
    response = getjson(sendstring)['body']

    historical = [{
        "opentime": str(candle[0])[:-3],
        "open": candle[1],
        "high": candle[2],
        "low": candle[3],
        "close": candle[4],
        "volume": candle[5],
        "closetime": str(candle[6])[:-3],
        "quoteassetvolume": candle[7],
        "numberoftrades": candle[8],
        "takerbuybaseassetvolume": candle[9],
        "takerbuyquoteassetvolume": candle[10],
        "ignore": candle[11]
    } for candle in response]

    return historical
'''
[
  [
    1499040000000,      // Open time
    "0.01634790",       // Open
    "0.80000000",       // High
    "0.01575800",       // Low
    "0.01577100",       // Close
    "148976.11427815",  // Volume
    1499644799999,      // Close time
    "2434.19055334",    // Quote asset volume
    308,                // Number of trades
    "1756.87402397",    // Taker buy base asset volume
    "28.46694368",      // Taker buy quote asset volume
    "17928899.62484339" // Ignore
  ]
]
'''