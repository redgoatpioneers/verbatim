from __future__ import division

import time
import datetime
import math

import pandas

from jads.common.utilities import prettytime
from jads.models import Exchange, Currency, Candle

'''
SEQEUNCER.PY
------------------------------
Created by Jacob Ashton
Red Goat Pioneers (c) February 2018

'''


'''
This function takes a list of float values and produces the mean value
of the numbers.
'''


def mean(a):
    return sum(a) / len(a)


'''
This function produces a pandas dataframe in the following format:

----------------------------------------------------------------------
| Period | Interval | Open  | High  | Low   | Close | OHLC4 | Volume |
----------------------------------------------------------------------
| Int    | Int      | Float | Float | Float | Float | Float | Float  |
----------------------------------------------------------------------

Period: Start time of the candle in Unixtime, no milliseconds
Interval: Total length of the candle, in seconds (e.g. 300 = 5 m candles)
Open/High/Low/Close: Decimal values for the OHLC prices.
OHLC4: Decimal value representing (open+high+low+close)/4.
Volume: Decimal value for the volume over this interval.
'''


def sequence_ohlc(symbol1, symbol2, source, starttime, endtime, interval,
                  indicators=False):

    # Validate the inputs
    # -------------------------------------------------------------------------

    # Convert starttime and endtime to Unixtime if not already
    try:
        if not isinstance(starttime, int):
            starttime = (int(time.mktime(datetime
                                         .datetime
                                         .strptime(starttime, "%Y-%m-%d %I:%M %p")
                                         .timetuple())))
        
        if not isinstance(endtime, int):
            endtime = (int(time.mktime(datetime
                                         .datetime
                                         .strptime(endtime, "%Y-%m-%d %I:%M %p")
                                         .timetuple())))

        interval = int(interval)
    except:
        errorstring = "ERROR >>> Bad input provided... exiting prematurely."
        print(errorstring)
        return {"error": errorstring}

    if starttime < 1262304000 or endtime > 2145916800:
        errorstring = (("ERROR >>> That time is out of range! Please try " +
                        "a time greater than {0} and less than {1}.")
                       .format(prettytime(1262304000), prettytime(2145916800)))
        print(errorstring)
        return {"error": errorstring}

    if starttime == endtime:
        errorstring = "ERROR >>> The end time is the same as the start time."
        print(errorstring)
        return {"error": errorstring}

    if endtime - starttime < interval:
        errorstring = (("ERROR >>> The end date must be separated by the " +
                        "start date by at least {0:.0f} minutes.")
                       .format(interval/60))
        print(errorstring)
        return {"error": errorstring}

    # Normalize the times
    # -------------------------------------------------------------------------
    starttime = starttime - (starttime % interval)
    endtime = endtime - (endtime % interval) + interval
    periods = math.floor((endtime - starttime) / interval)

    # Retrieve candles from the database and return them as an OHLC sequence
    # -------------------------------------------------------------------------
    candles = (Candle.objects
               .filter(symbol1=Currency.objects.get(name=symbol1),
                       symbol2=Currency.objects.get(name=symbol2),
                       startdate__gte=starttime,
                       startdate__lte=endtime+interval,
                       interval=interval,
                       source=Exchange.objects.get(name=source))[:periods])
    
    if indicators:
        ohlc_sequence = (pandas.DataFrame.from_records(
            candles.values('startdate', 'interval',
                           'open', 'high', 'low', 'close', 'volume',
                           'sma20', 'sma50', 'sma100',
                           'tsi', 'tsi_signal',
                           'rsi', 'adx', 'wvf', 'vbi'), coerce_float=True))
        if len(ohlc_sequence) == 0:
            return None

        ohlc_sequence = (ohlc_sequence
            .sort_values(by='startdate')
            .rename(index=str, columns={'startdate': 'period'})
            .reset_index(drop=True))
        ohlc_sequence['ohlc4'] = ohlc_sequence.apply(lambda row: (row['open']
            + row['high'] + row['low'] + row['close'])/4, axis=1)

        ohlc_sequence = ohlc_sequence[[
            'period',
            'open',
            'high',
            'low',
            'close',
            'ohlc4',
            'volume',
            'sma20',
            'sma50',
            'sma100',
            'tsi',
            'tsi_signal',
            'rsi',
            'adx',
            'wvf',
            'vbi',
            ]]
    else:
        ohlc_sequence = (pandas.DataFrame.from_records(
                            candles.values('startdate', 'interval',
                                           'open', 'high', 'low', 'close',
                                           'volume'), coerce_float=True))
        if len(ohlc_sequence) == 0:
            return None

        ohlc_sequence = (
            ohlc_sequence
            .sort_values(by='startdate')
            .rename(index=str, columns={'startdate': 'period'})
            .reset_index(drop=True))
        ohlc_sequence['ohlc4'] = ohlc_sequence.apply(
            lambda row:
            (row['open'] + row['high'] + row['low'] + row['close'])/4, axis=1)
        
        ohlc_sequence = ohlc_sequence[[
            'period',
            'open',
            'high',
            'low',
            'close',
            'ohlc4',
            'volume',
            ]]
    
    return ohlc_sequence


def sequence_ohlc_dict(symbol1, symbol2, source, starttime, endtime, interval):
    sequence = sequence_ohlc(symbol1, symbol2, source,
                             starttime, endtime, interval)
    if sequence is None:
        return None
    dict_sequence = sequence.to_dict(orient='records')

    return dict_sequence
