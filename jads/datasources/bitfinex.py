from jads.datasources.jsonmethods import getjson
from jads.models import Exchange
from jads.models import Currency

'''
BITFINEX.PY
------------------------------
Created by Jacob Ashton
Red Goat Pioneers (c) October 2017

'''

if Exchange.objects.filter(name='Bitfinex').count() > 0:
  apisource = Exchange.objects.get(name='Bitfinex').api_url
else:
  apisource = 'https://api.bitfinex.com/v1/'

apisource_alt = 'https://api.bitfinex.com/v2/trades/t'

def getbfxpairs():
    symbols = getjson(apisource + 'symbols')['body']
    pairs = []
    for pair in symbols:
        basecurrency = pair[0:3]
        quotecurrency = pair[3:6]
        pairs.append([basecurrency, quotecurrency])
    return pairs

def getbfxticker(basecurrency, quotecurrency):
    pair = '%s%s' % (basecurrency.lower(), quotecurrency.lower())
    ticker = getjson(apisource + 'pubticker/' + pair)['body']
    ticker['pair'] = '%s-%s' % (basecurrency.upper(), quotecurrency.upper())
    return ticker

'''
{
  "mid":"244.755",
  "bid":"244.75",
  "ask":"244.76",
  "last_price":"244.82",
  "low":"244.2",
  "high":"248.19",
  "volume":"7842.11542563",
  "timestamp":"1444253422.348340958"
}
'''

def getbfxhistorical(symbol, start, end):
    historical = getjson(apisource_alt + symbol.upper() + \
                         '/hist?start=' + str(start) + '&end=' + str(end))['body']
    return historical