import sys
import json
import pprint
from urllib.request import urlopen
from sys import argv
 
def isfloat(value):
    try:
        float(value)
        return True
    except ValueError:
        return False

def generate(pair):
    html = 'https://shapeshift.io/marketinfo/' + pair
 
    with urlopen(html) as url:
        headers = url.info()
        data = url.read().decode(headers.get_content_charset())
 
    marketinfo = json.loads(data)
    result = {'headers': headers.items(), 'body': marketinfo}
    return result
 
def getrate(pair):
    json = generate(pair)
    if 'error' in json['body']:
        return json['body']['error']
    else:
        return json['body']['rate']
 
def displayhelp():
    print('Price: To view the price for a single pair, type shapeshift.py -i [pair], where pair is symbol1_symbol2.')
    print('Pairs: To view the price for a standard list of pairs, type shapeshift.py -l (not all pairs are listed)')
    print('Price List: To view the prices for a common set of pairs, type shapeshift.py -a')
    exit()

def getall():
    currencies = ['btc', 'eth', 'xrp', 'etc', 'ltc', 'dash', 'xmr', 'zec', 'gnt', 'gno', 'rep', 'zec']
    pairs = []
    pairlist = []
    i = 1
    for currency in currencies:
        for match in currencies[i:]:
            try:
                if currency != match : pairs.append([currency, match])
            except:
                break
        i += 1
    for pair in pairs:
        pairlist.append('%s_%s' % (pair[0], pair[1]))
    return pairlist

def pricelist(pairs): # expects a list of strings of format "symbol1_symbol2"
     for pair in pairs:
        price = getrate(pair)
        display_name = pair.replace('/_', '//').upper()
        price = format(price, '.8f') if isfloat(price) else price
        print('%s: %s' % (display_name, price))

# This section used if the file is run by itself
if __name__ == '__main__':
    if len(argv) == 1 or len(argv) > 3:
        displayhelp()
    
    if len(argv) == 3 and argv[1] == '-i':
        pprint.pprint(generate(argv[2])['body'])
    elif len(argv) == 2 and argv[1] == '-a':
        pairs = ['usdt_btc', 'usdt_eth', 'clam_btc', 'dash_btc', 'doge_btc', 'etc_btc', 'eth_btc', 'fct_btc', 'gno_btc', 'gnt_btc', 'ltc_btc', 'maid_btc', 'xmr_btc', 'rep_btc', 'xrp_btc', 'zec_btc']
        pricelist(pairs)
    elif len(argv) == 2 and argv[1] == '-l':
        for pair in getall():
            print(pair)
    elif len(argv) == 2 and argv[1] == '-f':
        pricelist(getall())
    else:
        displayhelp()
