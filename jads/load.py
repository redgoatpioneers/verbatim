import csv
#import sqlite3
import psycopg2
import time
import datetime
import gzip
import os

from jads import models
from jads.datasources import bitfinex
from jads.datasources import gdax

from sys import argv

def isint(value):
    try:
        int(value)
        return True
    except ValueError:
        return False

def decompress(filename):
    base = os.path.basename(filename)
    newfile = os.getcwd() + '/' + os.path.splitext(base)[0]

    infile = gzip.open(filename, 'rb')
    s = infile.read()
    infile.close()

    outfile = open(newfile, 'wb')
    outfile.write(s)
    outfile.close()
    
    print('Successfully decompressed file to %s.' % newfile)
    return newfile

def show_file():
    with gzip.open(filename, 'rb') as csvfile:
        reader = csv.reader(csvfile, delimiter=',', quotechar='|')
        for row in reader:
            print(', '.join(row))

def read_first_line(filename):
    with open(filename, 'rb') as csvfile:
        reader = csv.reader(csvfile, delimiter=',', quotechar='|')
        row = next(reader)
    return row

def get_status(source):
    if source not in list_services():
        print('Sorry, this service is not currently supported. Use -l to see a list of supported services.')
        return

    results = []
    
    latest_trade = Trade.objects.order_by('-id')[0:1].get()
    latest_trade_bydate = Trade.objects.order_by('-tradedate')[0:1].get()

    results.append(latest_trade.tradeid)
    results.append(latest_trade_bydate.tradedate)

    return results

def load_file(filename, status):
    print('Opening database...')
    conn = psycopg2.connect(db)
    c = conn.cursor()
    if os.path.splitext(filename)[1] == '.gz':
        infile = decompress(filename)
    else:
        infile = filename
    with open(infile, 'rt') as csvfile:
        reader = csv.reader(csvfile, delimiter=',', quotechar='|')
        tradeId = 1 + status[0]
        numrows = 0
        mostrecent = 0
        for row in reader:
            if int(row[0]) > mostrecent: mostrecent = int(row[0])
            numrows += 1
            if int(row[0]) > int(status[1]):
                c.execute('INSERT INTO trades VALUES (' + str(tradeId) + ',\'BTC\',\'USD\',' + row[1] + ',' +\
                          row[2] + ',' + row[0] + ',\'bitfinex\')')
                tradeId += 1
    save_changes(conn)
    newrows = tradeId - status[0] - 1
    print('Processed %s rows of data. Most recent trade: %s' % (numrows, mostrecent))
    if newrows > 0:
        print('Imported %s new rows.' % newrows) 
    else:
        print('No new data to import!')

def list_services():
    return ['bitfinex', 'bitcoincharts', 'shapeshift']

def update_db(service):
    if service.lower() == 'bitfinex':
        svc_bitfinex()
    elif service.lower() == 'bitcoincharts':
        svc_bitcoincharts()
    elif service.lower() == 'shapeshift':
        svc_shapeshift()
    else:
        print('Sorry, this service is not currently supported. Use -l to see a list of supported services.')
    
def svc_bitfinex():
    status = get_status('bitfinex')
    tradeId = status[0]    

    #Need to convert s to ms for Bitfinex API
    start = status[1] * 1000
    end = start + 3600000 #Grab 1 hour's worth of data
    now = int(time.time())

    prettystart = datetime.datetime.fromtimestamp(status[1]).strftime('%Y-%m-%d %H:%M')
    prettyend = datetime.datetime.fromtimestamp(status[1] + 3600).strftime('%Y-%m-%d %H:%M')

    print('Asking Bitfinex for history between %s and %s...' % (prettystart, prettyend))
    update = bitfinex.gethistorical('btcusd', start, end)
    
    conn = sqlite3.connect(db)
    c = conn.cursor()
    
    numrows = 0
    mostrecent = 0
    numerrors = 0
    
    if update[0] == 'error':
        print('Error while retrieving data: %s - %s' % (str(update[1]), update[2]))
        return
    
    for trade in update:
        numrows += 1
        if len(trade) == 1:
            numerrors += 1
            print('Error reading trade (%s occurences): %s' % (numerrors, trade))
            print('Response: %s' % update)
            if numerrors >= 5:
                print('Too many errors! Please try again later.')
                return
            else:
                print('Bitfinex has a 60 requests/min limit. Sleeping for 20 seconds then trying again...')
                time.sleep(20)
                break
        tradedate = int(int(trade[1]) / 1000) #Convert back to s from ms for database (unixtime)
        if int(tradedate) > mostrecent: mostrecent = tradedate
        if int(tradedate) > int(status[1]):
            c.execute('INSERT INTO trades VALUES (' + str(tradeId + 1) + ',\'BTC\',\'USD\',' + \
                      str(trade[3]) + ',' + str(abs(trade[2])) + ',' + str(tradedate) + ',\'bitfinex\')')
            tradeId += 1
    save_changes(conn)

    newrows = tradeId - status[0]
    prettyrecent = datetime.datetime.fromtimestamp(mostrecent).strftime('%Y-%m-%d %H:%M')
    print('Processed %s rows of data. Most recent trade: %s' % (numrows, prettyrecent))
    if newrows > 0:
        print('Imported %s new rows.' % newrows)
    else:
        print('No new data to import in this time range!')

    if mostrecent < now - 3600:
        time.sleep(1) #This should throttle the repeats to 1 per second so we don't hit the 60rqsts/min limit
        svc_bitfinex()

def printstatus(service):
    latest = get_status(service.lower())
    if latest != None : print('Latest time: %s | Highest Id: %s' % (latest[1], latest[0]))

def printhelp():
    print('    To view the latest time and ID in the database, type "loadBTCUSD.py -l service".')
    print('    To load historical data from a .csv or .csv.gz file, type "loadBTCUSD.py -h filepathandname".')
    print('    To update the database from a public API, type "loadBTCUSD.py -u service".')
    print('    To view the current list of supported API services, type "loadBTCUSD.py -v".')

if __name__ == '__main__':
    if len(argv) == 1:
        printhelp()
    elif len(argv) > 2 and argv[1] == '-l':
        printstatus(argv[2])
    elif len(argv) > 2 and argv[1] == '-h':
        load_file(argv[2])
    elif len(argv) > 2 and argv[1] == '-u':
        update_db(argv[2].lower())
    elif len(argv) > 1 and argv[1] == '-v':
        print(list_services())
    else:
        printhelp()
