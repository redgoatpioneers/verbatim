from django.db import models


class Currency(models.Model):
    symbol = models.CharField(max_length=15)
    symbol3 = models.CharField(max_length=15)
    name = models.CharField(max_length=50)

    def __str__(self):
        return "%s - %s" % (self.symbol, self.name)


class Exchange(models.Model):
    name = models.CharField(max_length=15)
    api_url = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class Trade(models.Model):
    source = models.ForeignKey(Exchange, on_delete=models.PROTECT)
    symbol1 = models.ForeignKey(Currency, related_name='symbol1_set',
                                on_delete=models.PROTECT)
    symbol2 = models.ForeignKey(Currency, related_name='symbol2_set',
                                on_delete=models.PROTECT)
    price = models.DecimalField(max_digits=20, decimal_places=10)
    amount = models.DecimalField(max_digits=20, decimal_places=10)
    tradedate = models.IntegerField()

    def __str__(self):
        return ("%s %s for %s at %s @ %s (%s)" %
                (self.amount, self.symbol1.symbol, self.symbol2.symbol,
                 self.price, self.tradedate, self.source))


class Candle(models.Model):
    source = models.ForeignKey(Exchange, on_delete=models.PROTECT)
    symbol1 = models.ForeignKey(Currency, related_name='symbol1_candleset',
                                on_delete=models.PROTECT)
    symbol2 = models.ForeignKey(Currency, related_name='symbol2_candleset',
                                on_delete=models.PROTECT)
    startdate = models.IntegerField()
    interval = models.IntegerField()
    open = models.DecimalField(max_digits=20, decimal_places=10)
    high = models.DecimalField(max_digits=20, decimal_places=10)
    low = models.DecimalField(max_digits=20, decimal_places=10)
    close = models.DecimalField(max_digits=20, decimal_places=10)
    volume = models.DecimalField(max_digits=20, decimal_places=10)
    sma20 = models.DecimalField(max_digits=20, decimal_places=10, default=0)
    sma50 = models.DecimalField(max_digits=20, decimal_places=10, default=0)
    sma100 = models.DecimalField(max_digits=20, decimal_places=10, default=0)
    tsi = models.DecimalField(max_digits=20, decimal_places=10, default=0)
    tsi_signal = models.DecimalField(max_digits=20, decimal_places=10, default=0)
    rsi = models.DecimalField(max_digits=20, decimal_places=10, default=0)
    adx = models.DecimalField(max_digits=20, decimal_places=10, default=0)
    wvf = models.DecimalField(max_digits=20, decimal_places=10, default=0)
    atr = models.DecimalField(max_digits=20, decimal_places=10, default=0)

    def __str__(self):
        return (("%s candle for %s/%s at %s on %s | " +
                 "O: %.2f H: %.2f L: %.2f C: %.2f") %
                (self.interval, self.symbol1.symbol, self.symbol2.symbol,
                 self.startdate, self.source,
                 self.open, self.high, self.low, self.close))


class VerbatimIndex(models.Model):
    source = models.ForeignKey(Exchange, on_delete=models.PROTECT)
    symbol1 = models.ForeignKey(Currency, related_name='symbol1_indexset',
                                on_delete=models.PROTECT)
    symbol2 = models.ForeignKey(Currency, related_name='symbol2_indexset',
                                on_delete=models.PROTECT)
    startdate = models.IntegerField()
    strength = models.DecimalField(max_digits=20, decimal_places=10, default=0)
    trend = models.DecimalField(max_digits=20, decimal_places=10, default=0)
    momentum = models.DecimalField(max_digits=20, decimal_places=10, default=0)
    volatility = models.DecimalField(max_digits=20, decimal_places=10, default=0)
    volume = models.DecimalField(max_digits=20, decimal_places=10, default=0)
    verbatim = models.IntegerField(default=0)

    def __str__(self):
        return (('{0} {1}/{2} at {3}: S: {4} | T: {5} | M: {6} | V: {7} | ' +
                 'Verbatim: {8}').format(
            self.source.name,
            self.symbol1.symbol, self.symbol2.symbol,
            self.startdate,
            self.strength, self.trend, self.momentum, self.volatility,
            self.verbatim
        ))
