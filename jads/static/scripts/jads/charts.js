$(document).ready(function() {

    // AJAX GET
    $('#loadchart').click(function(){
        //$('#candlestick_chart').html("");
        $('#chartresults').html("");
        $('#loadstatus').html("<font color=\"#cccccc\">Loading chart data...</font>");
        $.ajax({
            type: "GET",
            url: "/verbatim/getchart/",
            dataType: "json",
            data: { "basecurrency": $("#basecurrency").val(),
                    "quotecurrency": $("#quotecurrency").val(),
                    "datasource": $("#datasource").val(),
                    "starttime": $("#starttime").val(),
                    "endtime": $("#endtime").val(),
                    "interval": $("#interval").val()},
            success: function(response) {
                $('#loadstatus').html("<font color=\"#4499ff\"><b>Success!</b></font>");
                
                var data = response;
                var table = $('<table id="dt-results"></table>')
                    .addClass('table')
                    .addClass('table-striped')
                    .addClass('table-bordered')
                    .addClass('table-hover')
                    .addClass('text-nowrap');
                
                tabledata = []
                data.forEach(function(period){
                    tabledata.push({
                        "unixtime": period.period,
                        "prettytime": timeConverter(period.period),
                        "Open": period.open,
                        "High": period.high,
                        "Low": period.low,
                        "Close": period.close,
                        "Volume": period.volume,
                    });
                });

                table.DataTable({
                    "paging": false,
                    "autoWidth": false,
                    "data": tabledata,
                    "columns": [
                        {"data": "unixtime", "bVisible": false},
                        {"data": "prettytime", "title": "Period",
                            "bSortable": true, "iDataSort": 0},
                        {"data": "Open", "title": "Open",
                            "bSortable": false},
                        {"data": "High", "title": "High",
                            "bSortable": false},
                        {"data": "Low", "title": "Low",
                            "bSortable": false},
                        {"data": "Close", "title": "Close",
                            "bSortable": false},
                        {"data": "Volume", "title": "Volume",
                            "bSortable": false},
                        ],
                });

                if ($("#showcandlechart").is(':checked')) {
                  createPlotlyChart(data);
                }
                
                $('#chartresults').html(table).trigger("create");
                //$('#chartresults').html(sequence);
            },
            error: function(response) {
                var error = response.responseJSON;
                $('#loadstatus').html("<font color=\"#aa0000\"><b>Error!</b></font>");
                $('#chartresults').html(error.error);
            }
        });
    });

    // AJAX POST
    // N/A

    // CSRF code

    // This function gets cookie with a given name
    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }
    var csrftoken = getCookie('csrftoken');

    /*
    The functions below will create a header with csrftoken
    */

    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }

    function sameOrigin(url) {
        // test that a given url is a same-origin URL
        // url could be relative or scheme relative or absolute
        var host = document.location.host; // host + port
        var protocol = document.location.protocol;
        var sr_origin = '//' + host;
        var origin = protocol + sr_origin;
        // Allow absolute or scheme relative URLs to same origin
        return (url == origin || url.slice(0, origin.length + 1) == origin + '/') ||
            (url == sr_origin || url.slice(0, sr_origin.length + 1) == sr_origin + '/') ||
            // or any other URL that isn't scheme relative or absolute i.e relative.
            !(/^(\/\/|http:|https:).*/.test(url));
    }

    $.ajaxSetup({
        crossDomain: false, // obviates need for sameOrigin test
        beforeSend: function(xhr, settings) {
            if (!csrfSafeMethod(settings.type) && sameOrigin(settings.url)) {
                // Send the token to same-origin, relative URLs only.
                // Send the token only if the method warrants CSRF protection
                // Using the CSRFToken value acquired earlier
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });
});

function timeConverter(UNIX_timestamp){
  var a = new Date(UNIX_timestamp * 1000);
  
  var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
  var year = a.getFullYear();
  var month = months[a.getMonth()];
  var date = a.getDate();
  var hour = ("0" + a.getHours()).slice(-2);
  var min = ("0" + a.getMinutes()).slice(-2);
  var sec = ("0" + a.getSeconds()).slice(-2);
  
  var time = date + ' ' + month + ' ' + year + ' ' + hour + ':' + min + ':' + sec;
  return time;
}

function chartTimeConverter(UNIX_timestamp){
  var a = new Date(UNIX_timestamp * 1000);
  
  var months = ['01','02','03','04','05','06','07','08','09','10','11','12'];
  var year = a.getFullYear();
  var month = months[a.getMonth()];
  var date = a.getDate();
  var hour = ("0" + a.getHours()).slice(-2);
  var min = ("0" + a.getMinutes()).slice(-2);
  
  var time = year + '-' + month + '-' + date + ' ' + hour + ':' + min;
  return time;
}

function createPlotlyChart(jsonResponse) {
    var trace = {
        x: [],
        open: [],
        high:[],
        low: [],
        close:[],

        // cutomise colors
        increasing: {line: {color: 'teal'}},
        decreasing: {line: {color: 'black'}},

    type: 'candlestick',
        xaxis: 'x',
        yaxis: 'y'
    };

    jsonResponse.forEach(function(period){
        trace['x'].push(chartTimeConverter(period.period));
        trace['open'].push(period.open);
        trace['high'].push(period.high);
        trace['low'].push(period.low);
        trace['close'].push(period.close);
    });

    var data = [trace];

    var layout = {
      dragmode: 'zoom',
      showlegend: false,
      xaxis: {
        autorange: true,
        title: 'Date',
         rangeselector: {
            x: 0,
            y: 1.2,
            xanchor: 'left',
            font: {size:8},
            buttons: [{
                step: 'month',
                stepmode: 'backward',
                count: 1,
                label: '1 month'
            }, {
                step: 'month',
                stepmode: 'backward',
                count: 6,
                label: '6 months'
            }, {
                step: 'all',
                label: 'All dates'
            }]
          }
      },
      yaxis: {
        autorange: true,
      }
    };

    Plotly.plot('candlestick_chart', data, layout);
};