$(document).ready(function() {

    $('[data-toggle="tooptip"]').tooltip({
      animation: true,
      delay: {show: 100, hide: 100}
    });

    $('.tickerchoices').hide();
    $('#datasource').get(0).selectedIndex = 0;
    $('#datasource').change(function () {
        $('.tickerchoices').hide();
        $('#'+$(this).val()).show();
        $('.pairbox').attr('checked', false);
        $('#tickers').html("");
        $('#loadstatus').html("");
    })

    // AJAX GET
    $('#generate').click(function(){
        var datasource = $("#datasource").val();

        if (datasource == "") {
            return;
        }

        $('#tickers').html("");
        $('#loadstatus').html("<font color=\"#cccccc\">Loading chart data...</font>");
        
        var pairs = [];
        $(".pairbox:checked").each(function(){
            pairs.push(this.id);
        })
        //alert("Current pairs selected: " + pairs)

        $.ajax({
            type: "GET",
            url: "/verbatim/gettickers/",
            dataType: "json",
            data: { "datasource": datasource,
                    "pairs": pairs},
            success: function(response) {
                $('#loadstatus').html("<font color=\"#4499ff\"><b>Success!</b></font>");
                
                var data = response;
                var table = $('<table></table>').addClass('tickers');
                $('<thead>').append(
                    $('<td>').text("Pair"),
                    $('<td>').text("Current Price"),
                    $('<td>').text("Last Trade Size"),
                    $('<td>').text("Latest Bid"),
                    $('<td>').text("Latest Ask"),
                    $('<td>').text("24h Volume")
                    ).appendTo(table);
                $.each(data, function(interval, properties) {
                    var $tr = $('<tr>').append(
                        $('<td>').text(properties.pair),
                        $('<td>').text(properties.price),
                        $('<td>').text(properties.size),
                        $('<td>').text(properties.bid),
                        $('<td>').text(properties.ask),
                        $('<td align="right">').text(properties.volume),
                        ).appendTo(table);
                })
                $('#tickers').html(table).trigger("create");
            },
            error: function(response) {
                var error = response.responseJSON;
                $('#loadstatus').html("<font color=\"#aa0000\"><b>Error!</b></font>");
                $('#tickers').html("Error retrieving tickers: "+error.errordescription);
            }
        });
    });

    $('#top_pairs').click(function(){
        var datasource = $("#datasource").val();

        if (datasource == "") {
            return;
        }

        var toppairs = { 1: {"GDAX": "BTC-USD",  "Bitfinex": "BTC-USD",  "Binance": "BTCUSDT"  }, // Bitcoin
                         2: {"GDAX": "ETH-BTC",  "Bitfinex": "ETH-BTC",  "Binance": "ETHBTC"   }, // Ethereum
                         3: {"GDAX": "",         "Bitfinex": "ADA-BTC",  "Binance": "ADABTC"   }, // Cardano
                         4: {"GDAX": "",         "Bitfinex": "DSH-BTC",  "Binance": "DASHBTC"  }, // Dash
                         5: {"GDAX": "ZEC-BTC",  "Bitfinex": "ZEC-BTC",  "Binance": "ZECBTC"   }, // ZCash
                         6: {"GDAX": "",         "Bitfinex": "NEO-BTC",  "Binance": "NEOBTC"   }, // NEO
                         7: {"GDAX": "LTC-BTC",  "Bitfinex": "LTC-BTC",  "Binance": "LTCBTC"   }, // Litecoin
                         8: {"GDAX": "",         "Bitfinex": "XMR-BTC",  "Binance": "XMRBTC"   }, // Monero
                         9: {"GDAX": "",         "Bitfinex": "IOT-BTC",  "Binance": "IOTABTC"  }, // Iota
                        10: {"GDAX": "",         "Bitfinex": "EOS-BTC",  "Binance": "EOSBTC"   }, // EOS
                        11: {"GDAX": "",         "Bitfinex": "",         "Binance": ""         }, // NEM
                        12: {"GDAX": "",         "Bitfinex": "TRX-BTC",  "Binance": "TRXBTC"   }, // Tronix
                        13: {"GDAX": "",         "Bitfinex": "VEN-BTC",  "Binance": "VENBTC"   }, // VeChain
                        14: {"GDAX": "",         "Bitfinex": "QTM-BTC",  "Binance": "QTUMBTC"  }, // Qtum
                        15: {"GDAX": "",         "Bitfinex": "",         "Binance": "ICXBTC"   }, // ICON
                        16: {"GDAX": "",         "Bitfinex": "LSK-BTC",  "Binance": "LSKBTC"   }, // Lisk
                        17: {"GDAX": "",         "Bitfinex": "",         "Binance": ""         }, // RaiBlocks
                        18: {"GDAX": "",         "Bitfinex": "PPT-BTC",  "Binance": "PPTBTC"   }, // Populous
                        19: {"GDAX": "",         "Bitfinex": "",         "Binance": ""         }, // Steem
                        20: {"GDAX": "",         "Bitfinex": "",         "Binance": "BNBBTC"   }};// Binance Coin
        
        var pairs = [];
        var available_pairs;
        switch(datasource) {
            case "GDAX":
                available_pairs = $('#gdaxpairs').data()["name"];
                break;
            case "Bitfinex":
                available_pairs = $('#bfxpairs').data()["name"];
                break;
            case "Binance":
                available_pairs = $('#binancepairs').data()["name"];
                break;
            default:
                available_pairs = [];
        }
        
        for (var i = 1; i <= 20; i++) {
            if (available_pairs.indexOf(toppairs[i][datasource])) {
                pairs.push(toppairs[i][datasource]);
            }
        };

        $.ajax({
            type: "GET",
            url: "/verbatim/gettickers/",
            dataType: "json",
            data: { "datasource": datasource,
                    "pairs": pairs},
            success: function(response) {
                $('#loadstatus').html("<font color=\"#4499ff\"><b>Success!</b></font>");
                
                var data = response;
                var table = $('<table></table>').addClass('tickers');
                $('<thead>').append(
                    $('<td>').text("Rank"),
                    $('<td>').text("Pair"),
                    $('<td>').text("Current Price"),
                    $('<td>').text("Last Trade Size"),
                    $('<td>').text("Latest Bid"),
                    $('<td>').text("Latest Ask"),
                    $('<td align="right">').text("24h Volume")
                    ).appendTo(table);
                var rank = 0;
                $.each(data, function(interval, properties) {
                    rank++;
                    var $tr = $('<tr>').append(
                        $('<td>').text(rank),
                        $('<td>').text(properties.pair),
                        $('<td>').text(properties.price),
                        $('<td>').text(properties.size),
                        $('<td>').text(properties.bid),
                        $('<td>').text(properties.ask),
                        $('<td>').text(properties.volume),
                        ).appendTo(table);
                })
                $('#tickers').html(table).trigger("create");
            },
            error: function(response, textStatus, thrownError) {
                $('#loadstatus').html("<font color=\"#aa0000\"><b>Error!</b></font>");
                $('#chartresults').html(JSON.stringify(response));
            }
        });

        $('#tickers').html("");
        $('#loadstatus').html("<font color=\"#cccccc\">Loading chart data...</font>");
    });

    // AJAX POST
    // N/A

    // CSRF code

    // This function gets cookie with a given name
    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }
    var csrftoken = getCookie('csrftoken');

    /*
    The functions below will create a header with csrftoken
    */

    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }

    function sameOrigin(url) {
        // test that a given url is a same-origin URL
        // url could be relative or scheme relative or absolute
        var host = document.location.host; // host + port
        var protocol = document.location.protocol;
        var sr_origin = '//' + host;
        var origin = protocol + sr_origin;
        // Allow absolute or scheme relative URLs to same origin
        return (url == origin || url.slice(0, origin.length + 1) == origin + '/') ||
            (url == sr_origin || url.slice(0, sr_origin.length + 1) == sr_origin + '/') ||
            // or any other URL that isn't scheme relative or absolute i.e relative.
            !(/^(\/\/|http:|https:).*/.test(url));
    }

    $.ajaxSetup({
        crossDomain: false, // obviates need for sameOrigin test
        beforeSend: function(xhr, settings) {
            if (!csrfSafeMethod(settings.type) && sameOrigin(settings.url)) {
                // Send the token to same-origin, relative URLs only.
                // Send the token only if the method warrants CSRF protection
                // Using the CSRFToken value acquired earlier
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });
});