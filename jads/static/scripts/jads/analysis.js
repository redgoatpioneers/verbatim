$(document).ready(function() {

  $("#showindicatorchart").checked = false;

  $('#analyze').click(function(){
    $('#chartresults').html("");
    $('#loadstatus').html("<font color=\"#cccccc\">Loading analysis results...</font>");

    var saveanalysis = false;
    if ($("#saveanalysis").is(':checked')) {
      saveanalysis = true;
      }

    var sendtype = saveanalysis ? "POST" : "GET";
    var sendurl = saveanalysis ? "/verbatim/saveanalysis/" : "/verbatim/runanalysis/";

    $.ajax({
      type: sendtype,
      url: sendurl,
      dataType: "json",
      data: {
        "basecurrency": $("#basecurrency").val(),
        "quotecurrency": $("#quotecurrency").val(),
        "datasource": $("#datasource").val(),
        "starttime": $("#starttime").val(),
        "endtime": $("#endtime").val(),
        "interval": $("#interval").val(),
        "tsi_short": $("#tsi_short").val(),
        "tsi_long": $("#tsi_long").val(),
        "rsi_length": $("#rsi_length").val(),
        "dilen": $("#dilen").val(),
        "adxlen": $("#adxlen").val(),
        "wvf_length": $("#wvf_length").val(),
      },
      success: function(response) {
        $('#loadstatus').html("<font color=\"#4499ff\"><b>Success!</b></font>");
        var data = response;
        var table = $('<table id="dt-results"></table>')
          .addClass('table')
          .addClass('table-striped')
          .addClass('table-bordered')
          .addClass('table-hover')
          .addClass('text-nowrap');
        
        tabledata = []
        data.forEach(function(period){
          tabledata.push({
            "unixtime": period.period,
            "prettytime": timeConverter(period.period),
            "Open": period.open,
            "High": period.high,
            "Low": period.low,
            "Close": period.close,
            "Volume": period.volume,
            "SMA20": period.sma20,
            "SMA50": period.sma50,
            "SMA100": period.sma100,
            "TSI": period.tsi,
            "TSI Signal": period.tsi_signal,
            "RSI": period.rsi,
            "ADX": period.adx,
            "Vix Fix": period.wvf,
            "Verbatim": period.vbi,
          });
        });

        table.DataTable({
          "paging": false,
          "autoWidth": false,
          "data": tabledata,
          "columns": [
            {"data": "unixtime", "bVisible": false},
            {"data": "prettytime", "title": "Period",
              "bSortable": true, "iDataSort": 0},
            {"data": "Open", "title": "Open", "bSortable": false,
              "bVisible": $("#open").is(':checked')},
            {"data": "High", "title": "High", "bSortable": false,
              "bVisible": $("#high").is(':checked')},
            {"data": "Low", "title": "Low", "bSortable": false,
              "bVisible": $("#low").is(':checked')},
            {"data": "Close", "title": "Close", "bSortable": false,
              "bVisible": $("#close").is(':checked')},
            {"data": "Volume", "title": "Volume", "bSortable": false,
              "bVisible": $("#volume").is(':checked')},
            {"data": "SMA20", "title": "SMA20", "bSortable": false,
              "bVisible": $("#sma20").is(':checked')},
            {"data": "SMA50", "title": "SMA50", "bSortable": false,
              "bVisible": $("#sma50").is(':checked')},
            {"data": "SMA100", "title": "SMA100", "bSortable": false,
              "bVisible": $("#sma100").is(':checked')},
            {"data": "TSI", "title": "TSI", "bSortable": false,
              "bVisible": $("#tsi").is(':checked')},
            {"data": "TSI Signal", "title": "TSI Signal", "bSortable": false,
              "bVisible": $("#tsi_signal").is(':checked')},
            {"data": "RSI", "title": "RSI", "bSortable": false,
              "bVisible": $("#rsi").is(':checked')},
            {"data": "ADX", "title": "ADX", "bSortable": false,
              "bVisible": $("#adx").is(':checked')},
            {"data": "Vix Fix", "title": "Vix Fix", "bSortable": false,
              "bVisible": $("#wvf").is(':checked')},
            {"data": "Verbatim", "title": "Verbatim", "bSortable": false,
              "bVisible": $("#vbi").is(':checked')},
            ],
        });

        if ($("#showcandlechart").is(':checked')) {
          google.charts.setOnLoadCallback(drawCandlestickChart(data));
        }
        if ($("#showindicatorchart").is(':checked')) {
          google.charts.setOnLoadCallback(drawIndicatorsLineChart(data));
        }
        
        $('#chartresults').html(table).trigger("create");
        },
      error: function(response) {
        var error = response.responseJSON;
        $('#loadstatus').html("<font color=\"#aa0000\"><b>Error!</b></font>");
        $('#chartresults').html(error.error);
        }
    });
  });

  // CSRF code

  // This function gets cookie with a given name
  function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
      var cookies = document.cookie.split(';');
      for (var i = 0; i < cookies.length; i++) {
        var cookie = jQuery.trim(cookies[i]);
        // Does this cookie string begin with the name we want?
        if (cookie.substring(0, name.length + 1) == (name + '=')) {
          cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
          break;
        }
      }
    }
    return cookieValue;
  }
  var csrftoken = getCookie('csrftoken');

  /*
  The functions below will create a header with csrftoken
  */

  function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
  }

  function sameOrigin(url) {
    // test that a given url is a same-origin URL
    // url could be relative or scheme relative or absolute
    var host = document.location.host; // host + port
    var protocol = document.location.protocol;
    var sr_origin = '//' + host;
    var origin = protocol + sr_origin;
    // Allow absolute or scheme relative URLs to same origin
    return (url == origin || url.slice(0, origin.length + 1) == origin + '/') ||
      (url == sr_origin || url.slice(0, sr_origin.length + 1) == sr_origin + '/') ||
      // or any other URL that isn't scheme relative or absolute i.e relative.
      !(/^(\/\/|http:|https:).*/.test(url));
  }

  $.ajaxSetup({
    crossDomain: false, // obviates need for sameOrigin test
    beforeSend: function(xhr, settings) {
      if (!csrfSafeMethod(settings.type) && sameOrigin(settings.url)) {
        // Send the token to same-origin, relative URLs only.
        // Send the token only if the method warrants CSRF protection
        // Using the CSRFToken value acquired earlier
        xhr.setRequestHeader("X-CSRFToken", csrftoken);
      }
    }
  });
});

function timeConverter(UNIX_timestamp){
  var a = new Date(UNIX_timestamp * 1000);
  
  var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
  var year = a.getFullYear();
  var month = months[a.getMonth()];
  var date = a.getDate();
  var hour = ("0" + a.getHours()).slice(-2);
  var min = ("0" + a.getMinutes()).slice(-2);
  var sec = ("0" + a.getSeconds()).slice(-2);
  
  var time = date + ' ' + month + ' ' + year + ' ' + hour + ':' + min + ':' + sec;
  return time;
}

function chartTimeConverter(UNIX_timestamp){
  var a = new Date(UNIX_timestamp * 1000);
  
  var months = ['01','02','03','04','05','06','07','08','09','10','11','12'];
  var year = a.getFullYear();
  var month = months[a.getMonth()];
  var date = a.getDate();
  var hour = ("0" + a.getHours()).slice(-2);
  var min = ("0" + a.getMinutes()).slice(-2);
  
  var time = year + '-' + month + '-' + date + ' ' + hour + ':' + min;
  return time;
}

function drawCandlestickChart(jsonResponse) {
  var chart = new google.visualization.CandlestickChart(document.getElementById('candlestick_chart'));
  var jsonData = getCandlestickJSONData(jsonResponse);
  var dataTable = new google.visualization.DataTable(jsonData);
  var options = {
    legend: 'none',
    vAxis: {title: 'Price (USD)'}
  };

  chart.draw(dataTable, options);
};

function drawIndicatorsLineChart(jsonResponse) {
  var chart = new google.visualization.LineChart(document.getElementById('indicators_line_chart'));
  var jsonData = getIndicatorsJSONData(jsonResponse);
  var dataTable = new google.visualization.DataTable(jsonData);
  var options = {
    vAxis: {title: 'Indicator Value'}
  };

  chart.draw(dataTable, options);
};

function getCandlestickJSONData(jsonResponse) {
  jsonData = {}
  jsonData['cols'] = [{'id': 'period', 'label': 'Period', 'type': 'string'},
          {'id': 'low', 'label': 'L', 'type': 'number'},
          {'id': 'open', 'label': 'O', 'type': 'number'},
          {'id': 'close', 'label': 'C', 'type': 'number'},
          {'id': 'high', 'label': 'H', 'type': 'number'}]
  jsonData['rows'] = []
  jsonResponse.forEach(function(period){
    jsonData['rows'].push({'c':[{'v': chartTimeConverter(period.period)},
                                {'v': period.low},
                                {'v': period.open},
                                {'v': period.close},
                                {'v': period.high}]})
  });

  return jsonData;
};

function getIndicatorsJSONData(jsonResponse) {
  jsonData = {}
  jsonData['cols'] = [{'id': 'period', 'label': 'Period', 'type': 'string'}]
  indicators = [{'name': 'tsi', 'label': 'TSI'},
          {'name': 'rsi', 'label': 'RSI'},
          {'name': 'adx', 'label': 'ADX'},
          {'name': 'wvf', 'label': 'VixFix'},
          {'name': 'jads', 'label': 'JADS'}]
  indicators.forEach(function(indicator){
    if (indicator.name in jsonResponse[0]){
      jsonData['cols'].push({'id': indicator.name, 'label': indicator.label, 'type': 'number'})
    };
  });

  jsonData['rows'] = []
  jsonResponse.forEach(function(period){
    jsonData['rows'].push({'c':[{'v': chartTimeConverter(period.period)},
                  {'v': 'tsi' in period ? period.tsi : null},
                  {'v': 'rsi' in period ? period.rsi : null},
                  {'v': 'adx' in period ? period.adx : null},
                  {'v': 'wvf' in period ? period.wvf : null},
                  {'v': 'jads' in period ? period.jads : null}]})
  });

  return jsonData;
};