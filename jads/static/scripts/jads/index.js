$(document).ready(function() {

  $('[data-toggle="tooptip"]').tooltip({
    animation: true,
    delay: {show: 100, hide: 100}
  });

  // AJAX GET
  $('#loadverbatim').click(function(){
    $('#indextables').html("");
    $('#loadstatus').html("<font color=\"#cccccc\">Loading index data...</font>");
    
    $.ajax({
      type: "GET",
      url: "/verbatim/loadverbatim/",
      dataType: "json",
      data: { "datasource": "Binance"},
      success: function(response) {
        $('#loadstatus').html("<font color=\"#4499ff\"><b>Success!</b></font>");
        $.each(response, function(pair, properties) {
          var table = $('<table></table>').addClass('verbatim');
          $('<tr>').append(
            $('<td>').text("Binance: "+properties.pair[0]+"/"+properties.pair[1])
              .attr('colspan',8)
              .addClass('indextitle')
            ).appendTo(table)
          $('<tr>').append(
            $('<th>').text(""),
            $('<th>').text("Last Candle"),
            $('<th>').text("TSI"),
            $('<th>').text("RSI"),
            $('<th>').text("ADX"),
            $('<th>').text("Vix Fix"),
            $('<th>').text("Volume"),
            $('<th>').text("JADS"),
            ).appendTo(table);
          $.each(properties.intervals, function(interval, properties) {
            var $tr = $('<tr>').append(
              $('<td>').text(properties.interval),
              $('<td>').text(timeConverter(properties.period)),
              $('<td align="right">').text(properties.tsi),
              $('<td align="right">').text(properties.rsi),
              $('<td align="right">').text(properties.adx),
              $('<td align="right">').text(properties.wvf),
              $('<td align="right">').text(properties.volume),
              $('<td align="right">').text(properties.jads)
              ).appendTo(table);
          });
          $('#indextables').append(table);
        });
      },
      error: function(response) {
          var error = response.responseJSON;
          $('#loadstatus').html("<font color=\"#aa0000\"><b>Error!</b></font>");
          $('#indextables').html(error.error);
      }
    });
  });

  // AJAX POST
  // N/A

  // CSRF code

  // This function gets cookie with a given name
  function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
      var cookies = document.cookie.split(';');
      for (var i = 0; i < cookies.length; i++) {
        var cookie = jQuery.trim(cookies[i]);
        // Does this cookie string begin with the name we want?
        if (cookie.substring(0, name.length + 1) == (name + '=')) {
          cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
          break;
        }
      }
    }
    return cookieValue;
  }
  var csrftoken = getCookie('csrftoken');

  /*
  The functions below will create a header with csrftoken
  */

  function csrfSafeMethod(method) {
      // these HTTP methods do not require CSRF protection
      return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
  }

  function sameOrigin(url) {
    // test that a given url is a same-origin URL
    // url could be relative or scheme relative or absolute
    var host = document.location.host; // host + port
    var protocol = document.location.protocol;
    var sr_origin = '//' + host;
    var origin = protocol + sr_origin;
    // Allow absolute or scheme relative URLs to same origin
    return (url == origin || url.slice(0, origin.length + 1) == origin + '/') ||
      (url == sr_origin || url.slice(0, sr_origin.length + 1) == sr_origin + '/') ||
      // or any other URL that isn't scheme relative or absolute i.e relative.
      !(/^(\/\/|http:|https:).*/.test(url));
  }

  $.ajaxSetup({
    crossDomain: false, // obviates need for sameOrigin test
    beforeSend: function(xhr, settings) {
      if (!csrfSafeMethod(settings.type) && sameOrigin(settings.url)) {
        // Send the token to same-origin, relative URLs only.
        // Send the token only if the method warrants CSRF protection
        // Using the CSRFToken value acquired earlier
        xhr.setRequestHeader("X-CSRFToken", csrftoken);
      }
    }
  });
});

function timeConverter(UNIX_timestamp){
  var a = new Date(UNIX_timestamp * 1000);
  
  var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
  var year = a.getFullYear();
  var month = months[a.getMonth()];
  var date = a.getDate();
  var hour = ("0" + a.getHours()).slice(-2);
  var min = ("0" + a.getMinutes()).slice(-2);
  var sec = ("0" + a.getSeconds()).slice(-2);
  
  var time = date + ' ' + month + ' ' + year + ' ' + hour + ':' + min + ':' + sec;
  return time;
}