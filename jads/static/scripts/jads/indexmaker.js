$(document).ready(function() {

    //AJAX GET
    $('#reviewverbatim').click(function(){
        $('#candlestick_chart').html("");
        $('#verbatim_chart').html("");
        $('#chartresults').html("");
        $('#loadstatus').html("<font color=\"#cccccc\">Retrieving indices...</font>");
        $.ajax({
            type: "GET",
            url: "/verbatim/reviewverbatim/",
            dataType: "json",
            data: {
              "basecurrency": $("#basecurrency").val(),
              "quotecurrency": $("#quotecurrency").val(),
              "datasource": $("#datasource").val(),
              "starttime": $("#starttime").val(),
              "endtime": $("#endtime").val(),
            },
            success: function(response) {
              $('#loadstatus').html("<font color=\"#4499ff\"><b>Success!</b></font>");
                
              var data = response;
              var table = $('<table id="dt-results"></table>')
                .addClass('table')
                .addClass('table-striped')
                .addClass('table-bordered')
                .addClass('table-hover')
                .addClass('text-nowrap');
              
              tabledata = []
              data.forEach(function(period){
                tabledata.push({
                  "unixtime": period.period,
                  "prettytime": timeConverter(period.period),
                  "Strength": period.strength,
                  "Trend": period.trend,
                  "Momentum": period.momentum,
                  "Volatility": period.volatility,
                  "Volume": period.volume,
                  "Verbatim": period.verbatim,
                });
              });

              table.DataTable({
                "paging": false,
                "autoWidth": false,
                "data": tabledata,
                "columns": [
                  {"data": "unixtime", "bVisible": false},
                  {"data": "prettytime", "title": "Period",
                    "bSortable": true, "iDataSort": 0},
                  {"data": "Strength", "title": "Strength",
                    "bSortable": false},
                  {"data": "Trend", "title": "Trend",
                    "bSortable": false},
                  {"data": "Momentum", "title": "Momentum",
                    "bSortable": false},
                  {"data": "Volatility", "title": "Volatility",
                    "bSortable": false},
                  {"data": "Volume", "title": "Volume",
                    "bSortable": false},
                  {"data": "Verbatim", "title": "Verbatim",
                    "bSortable": false},
                  ],
              });

              if ($("#showcandlechart").is(':checked')) {
                getCandlestickChart();
              }
        
              if ($("#show_vbi_chart").is(':checked')) {
                google.charts.setOnLoadCallback(drawVerbatimChart(data));
              }
        
              $('#chartresults').html(table).trigger("create");
            },
            error: function(response) {
              var error = response.responseJSON;
              $('#loadstatus').html("<font color=\"#aa0000\"><b>Error!</b></font>");
              $('#chartresults').html(error);
            }
        });
    });

    //AJAX POST
    $('#saveverbatim').click(function(){
        $('#candlestick_chart').html("");
        $('#verbatim_chart').html("");
        $('#chartresults').html("");
        $('#loadstatus').html("<font color=\"#cccccc\">Processing indices...</font>");
        $.ajax({
            type: "POST",
            url: "/verbatim/createverbatim/",
            dataType: "json",
            data: {
              "basecurrency": $("#basecurrency").val(),
              "quotecurrency": $("#quotecurrency").val(),
              "datasource": $("#datasource").val(),
              "starttime": $("#starttime").val(),
              "endtime": $("#endtime").val(),
              "interval": $("#interval").val(),
            },
            success: function(response) {
              $('#loadstatus').html("<font color=\"#4499ff\"><b>Success!</b></font>");
                
              var data = response;
              var table = $('<table id="dt-results"></table>')
                .addClass('table')
                .addClass('table-striped')
                .addClass('table-bordered')
                .addClass('table-hover')
                .addClass('text-nowrap');
              
              tabledata = []
              data.forEach(function(period){
                tabledata.push({
                  "unixtime": period.period,
                  "prettytime": timeConverter(period.period),
                  "Strength": period.strength,
                  "Trend": period.trend,
                  "Momentum": period.momentum,
                  "Volatility": period.volatility,
                  "Volume": period.volume,
                  "Verbatim": period.verbatim,
                });
              });

              table.DataTable({
                "paging": false,
                "autoWidth": false,
                "data": tabledata,
                "columns": [
                  {"data": "unixtime", "bVisible": false},
                  {"data": "prettytime", "title": "Period",
                    "bSortable": true, "iDataSort": 0},
                  {"data": "Strength", "title": "Strength",
                    "bSortable": false},
                  {"data": "Trend", "title": "Trend",
                    "bSortable": false},
                  {"data": "Momentum", "title": "Momentum",
                    "bSortable": false},
                  {"data": "Volatility", "title": "Volatility",
                    "bSortable": false},
                  {"data": "Volume", "title": "Volume",
                    "bSortable": false},
                  {"data": "Verbatim", "title": "Verbatim",
                    "bSortable": false},
                  ],
              });

              if ($("#showcandlechart").is(':checked')) {
                getCandlestickChart();
              }
        
              if ($("#show_vbi_chart").is(':checked')) {
                google.charts.setOnLoadCallback(drawVerbatimChart(data));
              }
                
              $('#chartresults').html(table).trigger("create");
            },
            error: function(response) {
              var error = response.responseJSON;
              $('#loadstatus').html("<font color=\"#aa0000\"><b>Error!</b></font>");
              $('#chartresults').html(error.error);
            }
        });
    });

    $('#deleteverbatim').click(function(){
        $('#candlestick_chart').html("");
        $('#verbatim_chart').html("");
        $('#chartresults').html("");
        $('#loadstatus').html("<font color=\"#cccccc\">Deleting indices...</font>");
        $.ajax({
            type: "POST",
            url: "/verbatim/deleteverbatim/",
            dataType: "json",
            data: {
              "basecurrency": $("#basecurrency").val(),
              "quotecurrency": $("#quotecurrency").val(),
              "datasource": $("#datasource").val(),
              "starttime": $("#starttime").val(),
              "endtime": $("#endtime").val(),
            },
            success: function(response) {
              $('#loadstatus').html("<font color=\"#4499ff\"><b>Success!</b></font>");
            },
            error: function(response) {
              var error = response.responseJSON;
              $('#loadstatus').html("<font color=\"#aa0000\"><b>Error!</b></font>");
              $('#chartresults').html(error);
            }
        });
    });

    // CSRF code

    // This function gets cookie with a given name
    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }
    var csrftoken = getCookie('csrftoken');

    /*
    The functions below will create a header with csrftoken
    */

    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }

    function sameOrigin(url) {
        // test that a given url is a same-origin URL
        // url could be relative or scheme relative or absolute
        var host = document.location.host; // host + port
        var protocol = document.location.protocol;
        var sr_origin = '//' + host;
        var origin = protocol + sr_origin;
        // Allow absolute or scheme relative URLs to same origin
        return (url == origin || url.slice(0, origin.length + 1) == origin + '/') ||
            (url == sr_origin || url.slice(0, sr_origin.length + 1) == sr_origin + '/') ||
            // or any other URL that isn't scheme relative or absolute i.e relative.
            !(/^(\/\/|http:|https:).*/.test(url));
    }

    $.ajaxSetup({
        crossDomain: false, // obviates need for sameOrigin test
        beforeSend: function(xhr, settings) {
            if (!csrfSafeMethod(settings.type) && sameOrigin(settings.url)) {
                // Send the token to same-origin, relative URLs only.
                // Send the token only if the method warrants CSRF protection
                // Using the CSRFToken value acquired earlier
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });
});

function getCandlestickChart(){
  $.ajax({
    type: "GET",
    url: "/verbatim/getchart/",
    dataType: "json",
    data: {
      "basecurrency": $("#basecurrency").val(),
      "quotecurrency": $("#quotecurrency").val(),
      "datasource": $("#datasource").val(),
      "starttime": $("#starttime").val(),
      "endtime": $("#endtime").val(),
      "interval": $("#interval").val()
    },
    success: function(response) {
      google.charts.setOnLoadCallback(drawCandlestickChart(response));
    },
    error: function(response) {
      var error = response.responseJSON;
      $('#candlestick_chart').html("<font color=\"#aa0000\"><b>Error!</b></font> "
        + error.error);
    }
  });
};

function timeConverter(UNIX_timestamp){
  var a = new Date(UNIX_timestamp * 1000);
  
  var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
  var year = a.getFullYear();
  var month = months[a.getMonth()];
  var date = a.getDate();
  var hour = ("0" + a.getHours()).slice(-2);
  var min = ("0" + a.getMinutes()).slice(-2);
  var sec = ("0" + a.getSeconds()).slice(-2);
  
  var time = date + ' ' + month + ' ' + year + ' ' + hour + ':' + min + ':' + sec;
  return time;
}

function chartTimeConverter(UNIX_timestamp){
  var a = new Date(UNIX_timestamp * 1000);
  
  var months = ['01','02','03','04','05','06','07','08','09','10','11','12'];
  var year = a.getFullYear();
  var month = months[a.getMonth()];
  var date = a.getDate();
  var hour = ("0" + a.getHours()).slice(-2);
  var min = ("0" + a.getMinutes()).slice(-2);
  
  var time = year + '-' + month + '-' + date + ' ' + hour + ':' + min;
  return time;
}

function drawCandlestickChart(jsonResponse) {
    var chart = new google.visualization.CandlestickChart(document
      .getElementById('candlestick_chart'));
    var jsonData = getCandlestickJSONData(jsonResponse);
    var dataTable = new google.visualization.DataTable(jsonData);
    var options = {
        legend: 'none',
        vAxis: {title: 'Price (USD)'}
    };

    chart.draw(dataTable, options);
};

function getCandlestickJSONData(jsonResponse) {
    jsonData = {}
    jsonData['cols'] = [{'id': 'period', 'label': 'Period', 'type': 'string'},
                    {'id': 'low', 'label': 'L', 'type': 'number'},
                    {'id': 'open', 'label': 'O', 'type': 'number'},
                    {'id': 'close', 'label': 'C', 'type': 'number'},
                    {'id': 'high', 'label': 'H', 'type': 'number'}]
    jsonData['rows'] = []
    jsonResponse.forEach(function(period){
        jsonData['rows'].push({'c':[{'v': chartTimeConverter(period.period)},
                                    {'v': period.low},
                                    {'v': period.open},
                                    {'v': period.close},
                                    {'v': period.high}]})
    });

    return jsonData;
};

function drawVerbatimChart(jsonResponse) {
    var chart = new google.visualization.LineChart(document
      .getElementById('verbatim_chart'));
    var jsonData = getVerbatimJSONData(jsonResponse);
    var dataTable = new google.visualization.DataTable(jsonData);
    var options = {
        legend: 'none',
        vAxis: {title: 'Verbatim Index'}
    };

    chart.draw(dataTable, options);
};

function getVerbatimJSONData(jsonResponse) {
    jsonData = {}
    jsonData['cols'] = [
      {'id': 'period', 'label': 'Period', 'type': 'string'},
      {'id': 'verbatim', 'label': 'VBI', 'type': 'number'},
    ]
    jsonData['rows'] = []
    jsonResponse.forEach(function(period){
        jsonData['rows'].push({'c':[
          {'v': chartTimeConverter(period.period)},
          {'v': period.verbatim},
        ]})
    });

    return jsonData;
};