from datetime import datetime
import time
import math

# Need to move times forward 4 days because UNIX time starts on Thursday
# Trading traditionally starts on Monday, so we will move this to Monday
FOUR_DAYS: int = 86400 * 4  # 259200


def unixtime(formattedtimestring):
    try:
        timestamp = (int(time.mktime(datetime
            .strptime(formattedtimestring, "%Y-%m-%d %I:%M %p")
            .timetuple())))
    except Exception as e:
        print("ERROR >>> There was a problem creating a UNIX timestamp: {0}"
              .format(e))
        raise e

    return timestamp


def prettytime(unixtimestamp):
    try:
        formattedtime = (datetime
                         .fromtimestamp(int(unixtimestamp))
                         .strftime('%Y-%m-%d %H:%M:%S'))
    except Exception as e:
        print("ERROR >>> Unable to format this UNIX timestamp: {0}"
              .format(e))
        raise e
        
    return formattedtime


def verbatimtime(unixtimestamp):
    try:
        formattedtime = (datetime
                         .fromtimestamp(int(unixtimestamp))
                         .strftime("%Y-%m-%d %I:%M %p"))
    except Exception as e:
        print("ERROR >>> Unable to format this UNIX timestamp: {0}"
              .format(e))
        raise e

    return formattedtime


def get_period_name(period_in_seconds):
    try:
        int(period_in_seconds)
    except Exception as e:
        print('ERROR >>> Only integer values can be converted to period name.')
        raise e

    return {
        '300': '5min',
        '900': '15min',
        '1800': '30min',
        '3600': '1hr',
        '7200': '2hr',
        '14400': '4hr',
        '28800': '8hr',
        '86400': '1day',
        '259200': '3day',
        '604800': '1wk',
    }[period_in_seconds]


def fix_starttime(unixtimestamp, interval):
    starttime = int(math.floor((unixtimestamp - (unixtimestamp % interval))))
    if interval == 604800:  # Weekly candles
        starttime += FOUR_DAYS
    return starttime
