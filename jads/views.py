import datetime
import os
from subprocess import Popen, PIPE

from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib import auth
from django.contrib.auth.decorators import login_required
from django.contrib.admin.views.decorators import staff_member_required
from django.views.decorators.csrf import csrf_protect

from jads.datasources.gdax import getgdaxpairs
from jads.datasources.bitfinex import getbfxpairs
from jads.datasources.binance import getbinancepairs, get_stored_pairs

from jads.models import Exchange, Currency

def index(request):
    return render(request, 'jads/home.html')

@login_required(login_url='/verbatim/accounts/login/')
def logs(request):
    logs = [{'name': 'NGINX Access', 'file': 'nginx-access',  'data': ''},
            {'name': 'NGINX Error',  'file': 'nginx-error',   'data': ''},
            {'name': 'uWSGI',  'file': 'uwsgi',         'data': ''},
            {'name': 'Celery Beat',   'file': 'celery-beat',   'data': ''},
            {'name': 'Celery Worker', 'file': 'celery-worker', 'data': ''}]

    for log in logs:
        call = ('tail -100 ' + os.getcwd() + '/static/logs/' + 
            log['file'] + '.txt')
        process = Popen(call, stdout=PIPE, shell=True)
        output = process.stdout.readlines()
        log['data'] = output
    
    return render(request, 'jads/logs.html', {'logs': logs})

def about(request):
    return render(request, 'jads/about.html')

def exchanges(request):
    exchanges = Exchange.objects.all()
    return render(request, 'jads/default.html', {'content': exchanges})

def loadfile(request):
    exchanges = Exchange.objects.all()
    currencies = Currency.objects.all().order_by('name')
    return render(request, 'jads/loadfile.html',
        {'exchanges': exchanges, 'currencies': currencies})

@login_required(login_url='/verbatim/accounts/login/')
@staff_member_required(login_url='/verbatim/accounts/login/')
def candlemaker(request):
    exchanges = Exchange.objects.all()
    currencies = Currency.objects.all().order_by('name')
    return render(request, 'jads/candlemaker.html',
        {'exchanges': exchanges, 'currencies': currencies})

@login_required(login_url='/verbatim/accounts/login/')
@staff_member_required(login_url='/verbatim/accounts/login/')
def indexmaker(request):
    exchanges = Exchange.objects.all()
    currencies = Currency.objects.all().order_by('name')
    return render(request, 'jads/indexmaker.html',
        {'exchanges': exchanges, 'currencies': currencies})

def charts(request):
    exchanges = Exchange.objects.all()
    currencies = Currency.objects.all().order_by('name')
    return render(request, 'jads/charts.html',
        {'exchanges': exchanges, 'currencies': currencies})

def indicators(request):
    exchanges = Exchange.objects.all()
    currencies = Currency.objects.all().order_by('name')
    return render(request, 'jads/indicators.html',
        {'exchanges': exchanges, 'currencies': currencies})

@login_required(login_url='/verbatim/accounts/login/')
@staff_member_required(login_url='/verbatim/accounts/login/')
def analysis(request):
    exchanges = Exchange.objects.all()
    currencies = Currency.objects.all().order_by('name')
    return render(request, 'jads/analysis.html',
        {'exchanges': exchanges, 'currencies': currencies})

def vbi(request):
    exchanges = Exchange.objects.all()
    currencies = Currency.objects.all().order_by('name')
    return render(request, 'jads/vbi.html',
        {'exchanges': exchanges, 'currencies': currencies})

def strategies(request):
    exchanges = Exchange.objects.all()
    currencies = Currency.objects.all().order_by('name')
    return render(request, 'jads/strategies.html',
        {'exchanges': exchanges, 'currencies': currencies})

def tickers(request):
    exchanges = Exchange.objects.all()
    
    #Load existing currencies
    currencies = Currency.objects.all().order_by('name')
    symbols = []
    for currency in currencies:
        symbols.append(currency.symbol)
    symbol3s = []
    for currency in currencies:
        symbol3s.append(currency.symbol3)

    # ------------- GDAX -------------
    #Load all GDAX pairs and only capture those for existing currencies
    gdaxpairs = []
    allgdaxpairs = getgdaxpairs()
    for gdaxpair in allgdaxpairs:
        if (gdaxpair.split('-')[0].lower() in [s.lower() for s in symbols]) and \
           (gdaxpair.split('-')[1].lower() in [s.lower() for s in symbols]):
            gdaxpairs.append(gdaxpair)

    # ------------- Bitfinex -------------
    #Load all BFX pairs and only capture those for existing currencies
    bfxlistpairs = []
    allbfxpairs = getbfxpairs()
    for bfxpair in allbfxpairs:
        if bfxpair[0].lower() in [s.lower() for s in symbol3s] and\
           bfxpair[1].lower() in [s.lower() for s in symbol3s]:
            bfxlistpairs.append(bfxpair)

    bfxpairs = ['%s-%s' % (pair[0].upper(), pair[1].upper())
        for pair in bfxlistpairs]

    # ------------- Binance -------------
    #Load all Binance currencies and only capture those for existing currencies
    storedpairs = get_stored_pairs()
    binancepairs = []
    for p in getbinancepairs():
    	binancepairs.append(p) if p in storedpairs else None

    top_pairs = "Bitcoin: BTC/USD(USDT)\n\
Ethereum: ETH/BTC\n\
Cardano: ADA/BTC\n\
Dash: DASH/BTC\n\
ZCash: ZEC/BTC\n\
NEO: NEO/BTC\n\
Litecoin: LTC/BTC\n\
Monero: XMR/BTC\n\
Iota: IOTA/BTC\n\
EOS: EOS/BTC\n\
NEM: NEM/BTC\n\
Tronix: TRX/BTC\n\
VeChain: VEN/BTC\n\
Qtum: QTUM/BTC\n\
Icon: ICX/BTC\n\
Lisk: LSK/BTC\n\
RaiBlocks: XRB/BTC\n\
Populous: PPT/BTC\n\
Steem: STEEM/BTC\n\
Binance Coin: BNB/BTC"

    return render(request, 'jads/tickers.html', {
        'exchanges': exchanges,
        'gdaxpairs': sorted(gdaxpairs),
        'bfxpairs': sorted(bfxpairs),
        'binancepairs': sorted(binancepairs),
        'top_pairs': top_pairs})

@csrf_protect
def login(request):
    c = {}
    if request.GET.get('invalid_login') == 'true':
        c['errormessage'] = ("Hmmm, that's not quite right." + 
        " Try again.")
    return render(request, 'jads/accounts/login.html', c)

def auth_view(request):
    username = request.POST.get('username', '')
    password = request.POST.get('password', '')
    user = auth.authenticate(username=username, password=password)

    if user is not None:
        auth.login(request, user)
        if request.POST.get('next') == '':
            return HttpResponseRedirect('/verbatim/about')
        else:
            return HttpResponseRedirect(request.POST.get('next'))
    else:
        return HttpResponseRedirect('/verbatim/accounts/invalid_login')

def loggedin(request):
    return render(request, 'jads/accounts/loggedin.html',
        {'full_name': request.user.username})

def invalid_login(request):
    return HttpResponseRedirect('/verbatim/accounts/login/?invalid_login=true')

def logout(request):
    auth.logout(request)
    return HttpResponseRedirect('/verbatim/about')