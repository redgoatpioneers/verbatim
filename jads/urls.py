from django.urls import path

from jads.views import (
    index,
    logs,
    about,
    exchanges,
    charts,
    indicators,
    tickers,
    strategies,
    analysis,
    vbi,
    loadfile,
    candlemaker,
    indexmaker,
    login,
    auth_view,
    logout,
    loggedin,
    invalid_login,
    )

from jads.ajax import (
    loadverbatim,
    loadfilescript,
    gettickers,
    loadexchangestatuses,
    getchart,
    getsequence,
    getindicators,
    runstrategy,
    runanalysis,
    saveanalysis,
    createcandles,
    autocandlemaker,
    candlecombiner,
    autocombine,
    removecandles,
    reviewverbatim,
    createverbatim,
    deleteverbatim,
    )

from jads.apis import (
    ExchangeList,
    CurrencyList,
    CandleList,
    TSI,
    ADX,
    WVF,
    CandleInfo
    )

urlpatterns = [
    path(r'', index, name='index'),

    # -------------------------------------------------------------------------
    # View Routes
    # -------------------------------------------------------------------------
    path(r'logs/', logs, name='logs'),
    path(r'about/', about, name='about'),
    path(r'exchanges/', exchanges, name='exchanges'),
    path(r'charts/', charts, name='charts'),
    path(r'indicators/', indicators, name='indicators'),
    path(r'tickers/', tickers, name='tickers'),
    path(r'loadfile/', loadfile, name='loadfile'),
    path(r'strategies/', strategies, name='strategies'),
    path(r'analysis/', analysis, name='analysis'),
    path(r'vbi/', vbi, name='vbi'),
    path(r'candlemaker/', candlemaker, name='candlemaker'),
    path(r'indexmaker/', indexmaker, name='indexmaker'),

    # -------------------------------------------------------------------------
    # AJAX Routes
    # -------------------------------------------------------------------------
    path(r'loadverbatim/', loadverbatim, name='loadverbatim'),
    path(r'loadfilescript/', loadfilescript, name='loadfilescript'),
    path(r'gettickers/', gettickers, name='gettickers'),
    path(r'loadexchangestatuses/', loadexchangestatuses,
         name='loadexchangestatuses'),
    path(r'getchart/', getchart, name='getchart'),
    path(r'getsequence/', getsequence, name='getsequence'),
    path(r'getindicators/', getindicators, name='getindicators'),
    path(r'runstrategy/', runstrategy, name='runstrategy'),
    path(r'runanalysis/', runanalysis, name='runanalysis'),
    path(r'saveanalysis/', saveanalysis, name='saveanalysis'),
    path(r'createcandles/', createcandles, name='createcandles'),
    path(r'autocandlemaker/', autocandlemaker, name='autocandlemaker'),
    path(r'candlecombiner/', candlecombiner, name='candlecombiner'),
    path(r'autocombine/', autocombine, name='autocombine'),
    path(r'removecandles/', removecandles, name='removecandles'),
    path(r'reviewverbatim/', reviewverbatim, name='reviewverbatim'),
    path(r'createverbatim/', createverbatim, name='createverbatim'),
    path(r'deleteverbatim/', deleteverbatim, name='deleteverbatim'),

    # -------------------------------------------------------------------------
    # API Routes
    # -------------------------------------------------------------------------

    # Data
    path(r'api/exchanges/', ExchangeList.as_view(), name='exchangelist'),

    path(r'api/currencies/', CurrencyList.as_view(), name='currencylist'),

    path(r'api/candles/',
         CandleList.as_view(), name='candlelist_default'),
    path(r'api/candles/<str:source>/',
         CandleList.as_view(), name='candlelist_source'),
    path(r'api/candles/<str:source>/<slug:pair>/',
         CandleList.as_view(), name='candlelist_pair'),
    path(r'api/candles/<str:source>/<slug:pair>/<int:intervals>/',
         CandleList.as_view(), name='candlelist_interval'),
    path(r'api/candles/<str:source>/<slug:pair>/<int:interval>' +
         '<int:startdate>/<int:enddate>/', CandleList.as_view(),
         name='candlelist'),
    path(r'api/candles/<str:source>/<slug:pair>/<int:interval>/' +
         '<int:startdate>/<int:enddate>/<int:limit>/', CandleList.as_view(),
         name='candlelist_limit'),

    # Indicators
    path(r'api/tsi/',
         TSI.as_view(), name='tsi_default'),
    path(r'api/tsi/<slug:pair>/',
         TSI.as_view(), name='tsi_pair'),
    path(r'api/tsi/<slug:pair>/<str:source>/',
         TSI.as_view(), name='tsi'),
    path(r'api/tsi/<slug:pair>/<str:source>/<str:intervals>',
         TSI.as_view(), name='tsi_interval'),

    path(r'api/adx/',
         ADX.as_view(), name='adx_default'),
    path(r'api/adx/<slug:pair>/',
         ADX.as_view(), name='adx_pair'),
    path(r'api/adx/<slug:pair>/<str:source>/',
         ADX.as_view(), name='adx'),
    path(r'api/adx/<slug:pair>/<str:source>/<str:intervals>',
         ADX.as_view(), name='adx_interval'),

    path(r'api/wvf/',
         WVF.as_view(), name='wvf_default'),
    path(r'api/wvf/<slug:pair>/',
         WVF.as_view(), name='wvf_pair'),
    path(r'api/wvf/<slug:pair>/<str:source>/',
         WVF.as_view(), name='wvf'),
    path(r'api/wvf/<slug:pair>/<str:source>/<str:intervals>',
         WVF.as_view(), name='wvf_interval'),

    # Candle Information
    path(r'api/candleinfo/',
         CandleInfo.as_view(), name='candleinfo'),
    path(r'api/candleinfo/<slug:pair>/',
         CandleInfo.as_view(), name='candleinfo'),
    path(r'api/candleinfo/<slug:pair>/<str:source>/',
         CandleInfo.as_view(), name='candleinfo'),
    path(r'api/candleinfo/<slug:pair>/<str:source>/<int:interval>/',
         CandleInfo.as_view(), name='candleinfo'),

    # -------------------------------------------------------------------------
    # User Authorization Routes
    # -------------------------------------------------------------------------
    path(r'accounts/login/', login, name='login'),
    path(r'accounts/auth/', auth_view, name='auth_view'),
    path(r'accounts/logout/', logout, name='logout'),
    path(r'accounts/loggedin/', loggedin, name='loggedin'),
    path(r'accounts/invalid_login/', invalid_login, name='invalid'),
]
