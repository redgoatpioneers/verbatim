import json
import time

from django.http import HttpResponse, Http404

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

from jads.models import Exchange, Currency, Candle
from jads.serializers import (
    CurrencySerializer,
    ExchangeSerializer,
    CandleSerializer)

from jads.api.tsi import gettsi
from jads.api.adx import getadx
from jads.api.wvf import getwvf
from jads.api.candleinfo import get_candle_info

NOW = time.time()
THEN = NOW - 86400
PAST = 1388534400

ACTIVE_INTERVALS = ['900',     # 15m
                    '1800',    # 30m
                    '3600',    # 1hr
                    '7200',    # 2hr
                    '14400',   # 4hr
                    '28800',   # 8hr
                    '86400',   # 1dy
                    '604800']  # 1wk


# ------------------------------------------------------------------------------
#   RESTful API View Classes
# ------------------------------------------------------------------------------


class ExchangeList(APIView):
    def get(self, request, format=None):
        exchanges = Exchange.objects.all()
        serializer = ExchangeSerializer(exchanges, many=True)
        return Response(serializer.data)


class CurrencyList(APIView):
    def get(self, request, format=None):
        currencies = Currency.objects.all()
        serializer = CurrencySerializer(currencies, many=True)
        return Response(serializer.data)


class CandleList(APIView):
    def get(self, request, source='GDAX', pair='BTC-USD', interval=300,
            startdate=THEN, enddate=NOW, limit=100, format=None):

        pair = pair.upper().split('-')
        symbol1 = pair[0]
        symbol2 = pair[1]
        print('DEBUG >>> Pair: {0} | Symbol1: {1} | Symbol2: {2}'
              .format(pair, symbol1, symbol2))

        try:
            candles = Candle.objects.filter(
                symbol1=Currency.objects.get(symbol=symbol1),
                symbol2=Currency.objects.get(symbol=symbol2),
                startdate__gte=startdate,
                startdate__lte=enddate,
                interval=interval,
                source=Exchange.objects.get(name=source))[:limit]
        except Exception as e:
            return Response(str(e), status=status.HTTP_400_BAD_REQUEST)

        if candles is not None and len(candles) > 0:
            serializer = CandleSerializer(candles, many=True)
            return Response(serializer.data)

        error_response = {
            'error': 'Error retrieving candle list!'
        }
        return Response(error_response, status=status.HTTP_400_BAD_REQUEST)


class TSI(APIView):
    def get(self, request, pair='BTC-USD', source='GDAX',
            intervals=ACTIVE_INTERVALS, format=None):

        pair = pair.upper().split('-')
        symbol1 = Currency.objects.get(symbol=pair[0]).name
        symbol2 = Currency.objects.get(symbol=pair[1]).name

        try:
            int(intervals)
            if intervals not in ACTIVE_INTERVALS:
                error_response = {'error': 'Unsupported interval: {0}'
                                           .format(intervals)}
                return Response(error_response,
                                status=status.HTTP_400_BAD_REQUEST)
            intervals = [intervals]
        except (TypeError, ValueError):
            if intervals != "score" and not isinstance(intervals, list):
                error_response = {'error': 'Invalid parameter \'interval\''}
                return Response(error_response,
                                    status=status.HTTP_400_BAD_REQUEST)

        if intervals == "score":
            tsi = gettsi(source, symbol1, symbol2,
                         get_score=True, intervals=intervals)
        else:
            tsi = gettsi(source, symbol1, symbol2, intervals=intervals)

        return HttpResponse(json.dumps(tsi),
                            content_type='application/json', status=400)


class ADX(APIView):
    def get(self, request, pair='BTC-USD', source='GDAX',
            intervals=ACTIVE_INTERVALS, format=None):

        pair = pair.upper().split('-')
        symbol1 = Currency.objects.get(symbol=pair[0]).name
        symbol2 = Currency.objects.get(symbol=pair[1]).name

        try:
            int(intervals)
            if intervals not in ACTIVE_INTERVALS:
                error_response = {'error': 'Unsupported interval: {0}'
                                           .format(intervals)}
                return Response(error_response,
                                status=status.HTTP_400_BAD_REQUEST)
            intervals = [intervals]
        except (TypeError, ValueError):
            if intervals != "score" and not isinstance(intervals, list):
                error_response = {'error': 'Invalid parameter \'interval\''}
                return Response(error_response,
                                    status=status.HTTP_400_BAD_REQUEST)

        if intervals == "score":
            adx = getadx(source, symbol1, symbol2,
                         get_score=True, intervals=ACTIVE_INTERVALS)
        else:
            adx = getadx(source, symbol1, symbol2, intervals=intervals)

        return HttpResponse(json.dumps(adx),
                            content_type='application/json', status=400)


class WVF(APIView):
    def get(self, request, pair='BTC-USD', source='GDAX',
            intervals=ACTIVE_INTERVALS, format=None):

        pair = pair.upper().split('-')
        symbol1 = Currency.objects.get(symbol=pair[0]).name
        symbol2 = Currency.objects.get(symbol=pair[1]).name

        try:
            int(intervals)
            if intervals not in ACTIVE_INTERVALS:
                error_response = {'error': 'Unsupported interval: {0}'
                                           .format(intervals)}
                return Response(error_response,
                                status=status.HTTP_400_BAD_REQUEST)
            intervals = [intervals]
        except (TypeError, ValueError):
            if not isinstance(intervals, list):
                error_response = {'error': 'Invalid parameter \'interval\''}
                return Response(error_response,
                                    status=status.HTTP_400_BAD_REQUEST)

        if intervals == "score":
            wvf = getwvf(source, symbol1, symbol2,
                         get_score=True, intervals=ACTIVE_INTERVALS)
        else:
            wvf = getwvf(source, symbol1, symbol2, intervals=intervals)

        return HttpResponse(json.dumps(wvf),
                            content_type='application/json', status=400)


class CandleInfo(APIView):
    def get(self, request, pair='BTC-USD', source='GDAX',
            interval=300, format=None):

        pair = pair.upper().split('-')
        symbol1 = Currency.objects.get(symbol=pair[0]).name
        symbol2 = Currency.objects.get(symbol=pair[1]).name

        candle_info = get_candle_info(source, symbol1, symbol2, interval)

        return HttpResponse(json.dumps(candle_info),
                            content_type='application/json', status=400)
