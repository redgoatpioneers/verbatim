import discord
from discord.ext import commands

import asyncio
import sys
import json

command_prefix = '$'
bot = commands.Bot(command_prefix)

SENTIMENTS: list = [
    "Rhino",
    "Bull",
    "Turtle",
    "Bear",
    "Gator",
]

ACTIVE_INTERVALS: list = ['900',     # 15m
                          '1800',    # 30m
                          '3600',    # 1hr
                          '7200',    # 2hr
                          '14400',   # 4hr
                          '28800',   # 8hr
                          '86400',   # 1dy
                          '604800']  # 1wk

INTERVAL_NAMES: dict = {
        '300':    ' 5 min ',
        '900':    '15 min ',
        '1800':   '30 min ',
        '3600':   ' 1 hour',
        '7200':   ' 2 hour',
        '14400':  ' 4 hour',
        '28800':  ' 8 hour',
        '86400':  ' 1 day ',
        '259200': ' 3 day ',
        '604800': ' 1 week',
    }

# -----------------------------------------------------------------------------
# MISCELLANEOUS METHODS
# -----------------------------------------------------------------------------

import requests
from datetime import datetime


def getjson(html):
    return requests.get(html).text


def prettytime(unixtimestamp):
    return (datetime
            .fromtimestamp(int(unixtimestamp))
            .strftime('%Y-%m-%d %H:%M:%S'))


# -----------------------------------------------------------------------------
# COMMANDS
# -----------------------------------------------------------------------------


@bot.command(pass_context=True)
async def info(ctx):
    await bot.say(
        "```asciidoc\n"
        "Type [$command] and I will do my best to accommodate."

        "\n\n[$healthcheck]"
        "\nI'll tell you if I'm alive (hint: I am!)."

        "\n\n[$sentiment <sentiment> <user>]"
        "\nI will set your role to"
        " <sentiment>. If you mention a <user>, I will set the "
        "role for that user instead!"

        "\n\n[$indicate]"
        "\nI will tell you the current TSI, ADX and WVF "
        "for BTC-USD on GDAX."

        "\n\n[$indicate <exchange> <currency>]"
        "\nI will tell you " +
        "the current TSI, ADX and WVF for <currency> on <exchange>."
        "\nCurrencies should be listed using the format 'XXX-YYY' "
        "where XXX is the base currency, and YYY is the quote."

        "\n\n[$card]"
        "\nI will tell you "
        "the current TSI, ADX and VIX for BTC-USD on GDAX for 1 hour candles."
        
        "\n\n[$card <base> <quote> <exchange> <interval>]"
        "\nI will tell you "
        "the current TSI, ADX and VIX for <base>-<quote> on <exchange> "
        "for <interval> candles."

        "\n\n[$info]"
        "\nI'll say all of this again."

        "\n\n= Available Roles ="
        "\nRhino, Bull, Turtle, Bear, Gator"

        "\n\n= Available Exchanges ="
        "\nGDAX, Binance"

        "\n\n= Available Currencies ="
        "\nBTC-USD, ETH-USD, ETH-BTC, LTC-USD, LTC-BTC, ADA-BTC"
        
        "\n\n= Available Intervals ="
        "\n300:    5 min"
        "\n900:   15 min"
        "\n1800:  30 min"
        "\n3600:   1 hour"
        "\n7200:   2 hour"
        "\n14400:  4 hour"
        "\n28800:  8 hour"
        "\n86400:  1 day"
        "\n604800: 1 week"
        "```")


@bot.command(pass_context=True)
async def healthcheck(ctx, member: discord.Member = None):
    if member is None:
        member = ctx.message.author

    await bot.say("Yes, I am alive {0}, thanks for asking!"
                  .format(member.mention))


@bot.command(pass_context=True)
async def sentiment(ctx, role="Turtle", member: discord.Member = None):
    if member is None:
        member = ctx.message.author

    newrole = discord.utils.get(ctx.message.server.roles, name=role)

    if newrole is None:
        await bot.say("There is no role called %s!" % role)
        print("No role found called %s!" % role)
        return

    if role[-1] == "s":
        role = role[0:len(role) - 1]

    await bot.say("{0} is now a {1}!"
                  .format(member.mention, role))

    for s in SENTIMENTS:
        role = discord.utils.get(ctx.message.server.roles, name=s)
        if role is not None:
            await bot.remove_roles(member, role)
            print("Removing role {0} from {1}.".format(role.name, member.name))
    await bot.add_roles(member, newrole)
    print("Adding role {0} to {1}.".format(newrole.name, member.name))


@bot.command(pass_context=True)
async def card(ctx, base='BTC', quote='USD', source='GDAX', interval='3600'):
    pair = "{0}-{1}".format(base, quote)
    config = "{0}.{1}".format(pair, source)
    supported_configs = ['BTC-USD.GDAX',
                         'BTC-USD.Binance',
                         'ETH-USD.GDAX',
                         'ADA-USD.Binance',
                         'ADA-BTC.Binance']
    if config not in supported_configs:
        await bot.say("Sorry, I can't do that yet... Try one of these:")
        for cfg in supported_configs:
            await bot.say(cfg)
        return

    await bot.say("Ok, you want to see what's going on for {0}, looking at the "
                  "{1} candles from {2}. Give me a moment to pull the data..."
                  .format(pair, INTERVAL_NAMES[interval], source)
                  .replace("  ", " "))

    currencies = {'BTC': ['Bitcoin', 0xf89c2e],
                  'ETH': ['Ethereum', 0x333333],
                  'ADA': ['Cardano', 0x246dd3],
                  'USD': ['USD', 0x85bb65]}
    currencyname = currencies[base][0]

    tsiurl = ('http://dev.redgoatpioneers.com/verbatim/api/tsi/{0}/{1}/{2}'
              .format(pair, source, interval))
    adxurl = ('http://dev.redgoatpioneers.com/verbatim/api/adx/{0}/{1}/{2}'
              .format(pair, source, interval))
    wvfurl = ('http://dev.redgoatpioneers.com/verbatim/api/wvf/{0}/{1}/{2}'
              .format(pair, source, interval))

    tsi_data = next((rtn_intv for rtn_intv
                     in json.loads(getjson(tsiurl))['tsi']
                     if rtn_intv['interval'] == interval), {'tsi': 'N/A'})
    adx_data = next((rtn_intv for rtn_intv
                     in json.loads(getjson(adxurl))['adx']
                     if rtn_intv['interval'] == interval), {'adx': 'N/A'})
    wvf_data = next((rtn_intv for rtn_intv
                     in json.loads(getjson(wvfurl))['wvf']
                     if rtn_intv['interval'] == interval), {'wvf': 'N/A'})

    embed = discord.Embed(title="{0} Status Report".format(currencies[base][0]),
                          description="Indicator status for {0}-{1}"
                                      .format(base, quote),
                          color=currencies[base][1])
    embed.add_field(name="Data Source",
                    value="{0}".format(source),
                    inline=True)
    embed.add_field(name="Time Period",
                    value=INTERVAL_NAMES[interval],
                    inline=True)
    embed.add_field(name="TSI",
                    value="{0}".format(tsi_data['tsi']),
                    inline=False)
    embed.add_field(name="ADX",
                    value="{0}".format(adx_data['adx']),
                    inline=False)
    embed.add_field(name="VIX",
                    value="{0}".format(wvf_data['wvf']),
                    inline=False)
    # embed.set_image(
    #     url="http://dev.redgoatpioneers.com/static/img/discord/{0}.png"
    #         .format(currencyname.lower()))
    embed.set_thumbnail(
        url="http://dev.redgoatpioneers.com/static/img/discord/{0}.png"
            .format(currencyname.lower()))

    await bot.say(embed=embed)


@bot.command(pass_context=True)
async def indicate(ctx, source='GDAX', pair='BTC-USD'):
    await bot.say("Please wait a moment while I calculate indicators...")

    tsiurl = ('http://dev.redgoatpioneers.com/verbatim/api/tsi/{0}/{1}'
              .format(pair, source))
    adxurl = ('http://dev.redgoatpioneers.com/verbatim/api/adx/{0}/{1}'
              .format(pair, source))
    wvfurl = ('http://dev.redgoatpioneers.com/verbatim/api/wvf/{0}/{1}'
              .format(pair, source))

    tsidata = json.loads(getjson(tsiurl))
    adxdata = json.loads(getjson(adxurl))
    wvfdata = json.loads(getjson(wvfurl))

    if 'error' in tsidata:
        await bot.say("Oops! There was an error calculating TSI: {0}"
                      .format(tsidata['error']))

    if 'error' in adxdata:
        await bot.say("Oops! There was an error calculating ADX: {0}"
                      .format(tsidata['error']))

    if 'error' in adxdata:
        await bot.say("Oops! There was an error calculating WVF: {0}"
                      .format(wvfdata['error']))

    if (
            'error' not in tsidata
            and 'error' not in adxdata
            and 'error' not in wvfdata):

        report_data = {
            interval: {
                'name': INTERVAL_NAMES[interval],
                'tsi': '   N/A',
                'adx': '   N/A',
                'wvf': '   N/A',
                'period': 'N/A'}
            for interval in ACTIVE_INTERVALS}

        for interval in tsidata['tsi']:
            if interval['interval'] in report_data:
                report_data[interval['interval']]['tsi'] = "{:.2f}".format(
                    interval['tsi'])
                while len(report_data[interval['interval']]['tsi']) < 6:
                    report_data[interval['interval']]['tsi'] = (" "
                        + report_data[interval['interval']]['tsi'])
                report_data[interval['interval']]['period'] = interval['period']
        for interval in adxdata['adx']:
            if interval['interval'] in report_data:
                report_data[interval['interval']]['adx'] = "{:.2f}".format(
                    interval['adx'])
                while len(report_data[interval['interval']]['adx']) < 6:
                    report_data[interval['interval']]['adx'] = (" "
                        + report_data[interval['interval']]['adx'])
        for interval in wvfdata['wvf']:
            if interval['interval'] in report_data:
                report_data[interval['interval']]['wvf'] = "{:.2f}".format(
                    interval['wvf'])
                while len(report_data[interval['interval']]['wvf']) < 6:
                    report_data[interval['interval']]['wvf'] = (" "
                        + report_data[interval['interval']]['wvf'])

        report = (
            "```Indicator Report: {0} on {1}\n\n"
            "Interval   |    TSI     ADX     WVF  Latest candle\n"
            "--------------------------------------------------------\n").format(
            pair, source)
        for interval in ACTIVE_INTERVALS:
            if interval in report_data:
                report += "{0}    | {1}  {2}  {3}  {4} UTC\n".format(
                    report_data[interval]['name'],
                    report_data[interval]['tsi'],
                    report_data[interval]['adx'],
                    report_data[interval]['wvf'],
                    report_data[interval]['period']
                )
        report += "```"
        await bot.say(report)


# -----------------------------------------------------------------------------
# EVENTS
# -----------------------------------------------------------------------------


@bot.event
async def on_message(message):
    if message.author == bot.user:
        return

    content = str(message.content)

    if "thanks" in content and "Verbatim Bot" in content:
        await bot.send_message(message.channel, "You're welcome, {0}!"
                               .format(message.author.mention))
        return

    if "Verbatim Bot" in content:
        await bot.send_message(message.channel, "I'm always listening, {0}..."
                               .format(message.author.mention))

    await bot.process_commands(message)


@bot.event
async def on_ready():
    print('------------------------------------------------------------\n'
          'Verbatim Bot - Discord - Magic Trading Room\n'
          '------------------------------------------------------------\n'
          'Logged in as:')
    print(bot.user.name)
    print(bot.user.id)
    print('------------------------------------------------------------')


bot.run('NDE3MDIzNDc2MzUyODExMDE4.DXM_iQ._VcLwXR9Ov9-seYr9RrjRgaRVa0')
