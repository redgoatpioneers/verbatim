import time

from jads.common.utilities import prettytime, verbatimtime, get_period_name
from jads.datasources.sequencer2 import sequence_ohlc
from jads.analysis.indicators2 import tsi

'''
TSI.PY
-------------------------------------------------------------------------------
Created by Jacob Ashton
Red Goat Pioneers (c) February 2018

    INSTRUCTIONS
    ===========================================================================
    Call this API using a dedicated route. Returns the TSIs and score for the
    last 1000 candles available for each interval.
'''

PAST: int = 1388534400
ACTIVE_INTERVALS: list = ['900',     # 15m
                          '1800',    # 30m
                          '3600',    # 1hr
                          '7200',    # 2hr
                          '14400',   # 4hr
                          '28800',   # 8hr
                          '86400',   # 1dy
                          '604800']  # 1wk
SCORING_INTERVALS: list = ['14400', '86400', '604800']

TSI_SHORT: int = 13
TSI_LONG: int = 25
TSI_SIGNAL: int = 13

DEVIATION_THRESHOLD: int = 0
LOW_MULTIPLIER: int = 1
MED_MULTIPLIER: int = 1
HIGH_MULTIPLIER: int = 1

INTERVAL_WEIGHT: list = {
    '300':    LOW_MULTIPLIER,
    '900':    LOW_MULTIPLIER,
    '1800':   LOW_MULTIPLIER,
    '3600':   LOW_MULTIPLIER,
    '7200':   LOW_MULTIPLIER,
    '14400':  LOW_MULTIPLIER,
    '28800':  LOW_MULTIPLIER,
    '86400':  LOW_MULTIPLIER,
    '604800': LOW_MULTIPLIER,
}


# -----------------------------------------------------------------------------
# Indicator functions
# -----------------------------------------------------------------------------


def gettsi(source, base, quote, get_score=False, enddate=time.time(),
           intervals=ACTIVE_INTERVALS):

        # Pull the last 1000 available candles for each interval
        # ---------------------------------------------------------------------
        candles = {}

        def get_candles():
            startdate = int(enddate) - (1000 * int(interval))
            if startdate < PAST:
                startdate = PAST
            recent_candles = sequence_ohlc(
                base, quote, source,
                verbatimtime(startdate), verbatimtime(enddate), interval)
            if recent_candles is not None:
                print(('INFO >>> Retrieved {0} {1} candles ' +
                       'between {2} and {3}.')
                      .format(len(recent_candles), get_period_name(interval),
                              prettytime(startdate), prettytime(enddate)))
                return recent_candles
            else:
                print(('INFO >>> No {0} candles available ' +
                       'between {1} and {2}.')
                      .format(get_period_name(interval),
                              prettytime(startdate), prettytime(enddate)))
                return None

        try:
            for interval in intervals:
                candles[interval] = get_candles()
        except Exception as e:
            print("ERROR >>> Exception getting candles for TSI: {0}".format(e))
            tsi_error = {
                'error': 'Error retrieving candles: {0}'.format(e),
            }
            return tsi_error
        
        # Calculate the TSI for each interval
        # ---------------------------------------------------------------------
        try:
            for interval in intervals:
                if candles[interval] is not None:
                    tsi_periods = tsi(candles[interval], 'close')
                    candles[interval]['tsi'] = tsi_periods['tsi']
                    candles[interval]['tsi_signal'] = tsi_periods['tsi_signal']
                    print('INFO >>> Processed TSI for {0} second candles.'
                          .format(interval))
        except Exception as e:
            tsi_error = {
                'error': 'Error calculating TSI values: {0}'.format(e),
            }
            return tsi_error
        
        # Determine the TSI score for each interval
        # ---------------------------------------------------------------------

        def calculate_tsi_score(tsi_value, tsi_signal):
            deviation_threshold = 0
            deviation = abs(tsi_value - tsi_signal)
            score = 0
            if tsi_value > 0:
                score += 2
            elif tsi_value < 0:
                score -= 2
            if tsi_value > tsi_signal and deviation > deviation_threshold:
                score += 1
            elif tsi_value < tsi_signal and deviation > deviation_threshold:
                score -= 1
            return score

        tsi_score = 'N/A'
        if get_score:
            tsi_score = 0
            try:
                for interval in SCORING_INTERVALS:
                    if candles[interval] is not None:
                        tsi_score += calculate_tsi_score(
                            candles[interval]['tsi'].iloc[-1],
                            candles[interval]['tsi_signal'].iloc[-1])
            except Exception as e:
                tsi_error = {
                    'error': 'Error calculating TSI scores: {0}'.format(e),
                }
                return tsi_error

        # Build the return JSON
        # ---------------------------------------------------------------------
        tsi_results = {
            'source': source,
            'base': base,
            'quote': quote,
            'period': 'N/A',
            'tsi': [],
            'vbi_tsiscore': tsi_score,
            'vbi_tsiscore_max': (3 * len(intervals))
        }

        try:
            for interval in intervals:
                if candles[interval] is not None:
                    tsi_results['tsi'].append({
                        'interval': interval,
                        'period': prettytime(candles[interval]['period'].iloc[-1]),
                        'tsi': candles[interval]['tsi'].iloc[-1],
                        'tsi_signal': candles[interval]['tsi_signal'].iloc[-1],
                        })
        except Exception as e:
            tsi_error = {
                'error': 'Error creating results: {0}'.format(e),
            }
            return tsi_error

        for interval in tsi_results['tsi']:
            if tsi_results['period'] == 'N/A':
                tsi_results['period'] = interval['period']
            elif interval['period'] > tsi_results['period']:
                tsi_results['period'] = interval['period']

        return tsi_results
