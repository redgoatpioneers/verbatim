import time

from jads.common.utilities import prettytime, verbatimtime, get_period_name
from jads.datasources.sequencer2 import sequence_ohlc
from jads.analysis.indicators2 import wvf

'''
WVF.PY
-------------------------------------------------------------------------------
Created by Jacob Ashton
Red Goat Pioneers (c) February 2018

    INSTRUCTIONS
    ===========================================================================
    Call this API using a dedicated route. Returns the WVFs and score for the
    last 1000 candles available for each interval.
'''

PAST: int = 1388534400
ACTIVE_INTERVALS = ['900',     # 15m
                    '1800',    # 30m
                    '3600',    # 1hr
                    '7200',    # 2hr
                    '14400',   # 4hr
                    '28800',   # 8hr
                    '86400',   # 1dy
                    '604800']  # 1wk
SCORING_INTERVALS: list = ['14400', '86400', '604800']
WVFLEN = 22


# -----------------------------------------------------------------------------
# Indicator functions
# -----------------------------------------------------------------------------


def getwvf(source, base, quote, get_score=False, enddate=time.time(),
           intervals=ACTIVE_INTERVALS):

        # Pull the last 1000 available candles for each interval
        # ---------------------------------------------------------------------
        candles = {}

        def get_candles():
            startdate = int(enddate) - (1000 * int(interval))
            if startdate < PAST:
                startdate = PAST
            recent_candles = sequence_ohlc(
                base, quote, source,
                verbatimtime(startdate), verbatimtime(enddate), interval)
            if recent_candles is not None:
                print(('INFO >>> Retrieved {0} {1} candles ' +
                       'between {2} and {3}.')
                      .format(len(recent_candles), get_period_name(interval),
                              prettytime(startdate), prettytime(enddate)))
                return recent_candles
            else:
                print(('INFO >>> No {0} candles available ' +
                       'between {1} and {2}.')
                      .format(get_period_name(interval),
                              prettytime(startdate), prettytime(enddate)))
                return None

        try:
            for interval in intervals:
                candles[interval] = get_candles()
        except Exception as e:
            print("ERROR >>> Exception getting candles for ADX: {0}".format(e))
            wvf_error = {
                'error': 'Error retrieving candles: {0}'.format(e),
            }
            return wvf_error

        # Calculate the WVF for each interval
        # ---------------------------------------------------------------------
        try:
            for interval in intervals:
                if candles[interval] is not None:
                    candles[interval]['wvf'] = wvf(candles[interval], WVFLEN)
                    print('INFO >>> Processed WVF for {0} candles.'
                          .format(get_period_name(interval)))
        except Exception as e:
            wvf_error = {
                'error': 'Error calculating WVF values: {0}'.format(e),
            }
            return wvf_error
        
        # Determine the WVF score for each interval
        # ---------------------------------------------------------------------
        
        def calculate_wvf_score(wvf_value):
            # Not yet implemented
            return 0

        wvf_score = 'N/A'
        if get_score:
            wvf_score = 0
            try:
                for interval in SCORING_INTERVALS:
                    if candles[interval] is not None:
                        wvf_score += calculate_wvf_score(
                            candles[interval]['wvf'].iloc[-1])
            except Exception as e:
                wvf_error = {
                    'error': 'Error calculating WVF scores: {0}'.format(e),
                }
                return wvf_error

        # Build the return JSON
        # ---------------------------------------------------------------------
        wvf_results = {
            'source': source,
            'base': base,
            'quote': quote,
            'period': 'N/A',
            'wvf': [],
            'vbi_wvfscore': wvf_score,
            'vbi_wvfscore_max': (0 * len(SCORING_INTERVALS))
        }

        try:
            for interval in intervals:
                if candles[interval] is not None:
                    wvf_results['wvf'].append({
                        'interval': interval,
                        'period': prettytime(candles[interval]['period'].iloc[-1]),
                        'wvf': candles[interval]['wvf'].iloc[-1],
                        })
        except Exception as e:
            wvf_error = {
                'error': 'Error creating results: {0}'.format(e),
            }
            return wvf_error

        for interval in wvf_results['wvf']:
            if wvf_results['period'] == 'N/A':
                wvf_results['period'] = interval['period']
            elif interval['period'] > wvf_results['period']:
                wvf_results['period'] = interval['period']

        return wvf_results
