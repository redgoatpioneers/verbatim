from jads.common.utilities import prettytime
from jads.models import Exchange, Currency, Candle

'''
CANDLEINFO.PY
-------------------------------------------------------------------------------
Created by Jacob Ashton
Red Goat Pioneers (c) April 2018

    INSTRUCTIONS
    ===========================================================================
    Call this API using a dedicated route. Returns candle information for
    specific exchanges, currencies and time intervals.
'''


def get_candle_info(source, base, quote, interval):
    print('INFO >>> Pulling candles from database...')
    candle_infos = Candle.objects.filter(
        source=Exchange.objects.get(name=source),
        symbol1=Currency.objects.get(name=base),
        symbol2=Currency.objects.get(name=quote),
        interval=interval,
    ).values('startdate')

    candle_infos = sorted(candle_infos,
                          key=lambda candles: candles['startdate'])

    print('INFO >>> Analyzing candles for candle information...')
    first_startdate = candle_infos[0]['startdate']
    final_startdate = candle_infos[-1]['startdate']
    candle_count = len(candle_infos)

    return {
        'source': source,
        'base': base,
        'quote': quote,
        'interval': interval,
        'first_startdate': first_startdate,
        'first_startdate_pretty': prettytime(first_startdate),
        'final_startdate': final_startdate,
        'final_startdate_pretty': prettytime(final_startdate),
        'count': candle_count,
    }
