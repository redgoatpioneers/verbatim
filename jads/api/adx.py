import time
import math

from jads.common.utilities import prettytime, verbatimtime, get_period_name
from jads.datasources.sequencer2 import sequence_ohlc
from jads.analysis.indicators2 import adx, sma, roc

'''
ADX.PY
-------------------------------------------------------------------------------
Created by Jacob Ashton
Red Goat Pioneers (c) February 2018

    INSTRUCTIONS
    ===========================================================================
    Call this API using a dedicated route. Returns the ADXs and score for the
    last 1000 candles available for each interval.
'''

PAST: int = 1388534400
ACTIVE_INTERVALS = ['900',     # 15m
                    '1800',    # 30m
                    '3600',    # 1hr
                    '7200',    # 2hr
                    '14400',   # 4hr
                    '28800',   # 8hr
                    '86400',   # 1dy
                    '604800']  # 1wk
SCORING_INTERVALS: list = ['14400', '86400', '604800']
DILEN = 14
ADXLEN = 14
VLEN = 14


# -----------------------------------------------------------------------------
# Indicator functions
# -----------------------------------------------------------------------------


def getadx(source, base, quote, get_score=False, get_beacon=False,
           enddate=time.time(), intervals=ACTIVE_INTERVALS):

        # Pull the last 1000 available candles for each interval
        # ---------------------------------------------------------------------
        candles = {}

        def get_candles(get_interval):
            startdate = int(enddate) - (1000 * int(get_interval))
            if startdate < PAST:
                startdate = PAST
            recent_candles = sequence_ohlc(
                base, quote, source,
                verbatimtime(startdate), verbatimtime(enddate), get_interval)
            if recent_candles is not None:
                print(('INFO >>> Retrieved {0} {1} candles ' +
                       'between {2} and {3}.')
                      .format(len(recent_candles), get_period_name(get_interval),
                              prettytime(startdate), prettytime(enddate)))
                return recent_candles
            else:
                print(('INFO >>> No {0} candles available ' +
                       'between {1} and {2}.')
                      .format(get_period_name(get_interval),
                              prettytime(startdate), prettytime(enddate)))
                return None

        try:
            for interval in intervals:
                candles[interval] = get_candles(interval)
        except Exception as e:
            print("ERROR >>> Exception getting candles for ADX: {0}".format(e))
            adx_error = {
                'error': 'Error retrieving candles: {0}'.format(e),
            }
            return adx_error

        # Calculate the ADX for each interval
        # ---------------------------------------------------------------------
        try:
            for interval in intervals:
                if candles[interval] is not None:
                    candles[interval]['adx'] = adx(
                        candles[interval], DILEN, ADXLEN)['adx']
                    print('INFO >>> Processed ADX for {0} candles.'
                          .format(get_period_name(interval)))
        except Exception as e:
            adx_error = {
                'error': 'Error calculating ADX values: {0}'.format(e),
            }
            return adx_error

        # Calculate the VADX for each interval (only if requested)
        # ---------------------------------------------------------------------
        if get_beacon:
            try:
                for interval in intervals:
                    if candles[interval] is not None:
                        candles[interval]['smooth_adx'] = sma(
                            candles[interval], 'adx', 14)
                        candles[interval]['vadx'] = roc(
                            candles[interval], 'smooth_adx', 14)
                        print('INFO >>> Processed VADX for {0} candles.'
                              .format(get_period_name(interval)))
            except Exception as e:
                adx_error = {
                    'error': 'Error calculating VADX values: {0}'.format(e),
                }
                return adx_error
        
        # Determine the ADX score for each interval
        # ---------------------------------------------------------------------
        
        def calculate_adx_score(adx_value):
            score = math.floor(adx_value/10)
            return score

        adx_score = 'N/A'
        if get_score:
            adx_score = 0
            try:
                for interval in SCORING_INTERVALS:
                    if candles[interval] is not None:
                        adx_score += calculate_adx_score(
                            candles[interval]['adx'].iloc[-1])
            except Exception as e:
                adx_error = {
                    'error': 'Error calculating ADX scores: {0}'.format(e),
                }
                return adx_error

        # Build the return JSON
        # ---------------------------------------------------------------------
        adx_results = {
            'source': source,
            'base': base,
            'quote': quote,
            'period': 'N/A',
            'adx': [],
            'vbi_adxscore': adx_score,
            'vbi_adxscore_max': (10 * len(SCORING_INTERVALS))
        }

        try:
            for interval in intervals:
                if candles[interval] is not None:
                    adx_results['adx'].append({
                        'interval': interval,
                        'period': prettytime(candles[interval]['period'].iloc[-1]),
                        'adx': candles[interval]['adx'].iloc[-1],
                        'vadx': candles[interval]['vadx'].iloc[-1] if get_beacon else "N/A",
                        })
        except Exception as e:
            adx_error = {
                'error': 'Error creating results: {0}'.format(e),
            }
            return adx_error

        for interval in adx_results['adx']:
            if adx_results['period'] == 'N/A':
                adx_results['period'] = interval['period']
            elif interval['period'] > adx_results['period']:
                adx_results['period'] = interval['period']

        return adx_results
